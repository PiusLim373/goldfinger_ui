import React, { Component } from "react";
import {
  Alert,
  Col,
  Button,
  Collapse,
  Card,
  Row,
  Figure,
  Image,
  Spinner,
} from "react-bootstrap";
import axios from "axios";

class MainOperation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      current_stage: "loading", // loading, confirmation, inspecting and finished
      tool_selected: "",
      hashmap: {
        tcj: "Towel Clip Jones",
        spr: "Spear Redivac",
        oth: "Tools that are NOT Towel Clip Jones or Spear Redivac",
      },
      is_defective: false,
    };
    this.get_state();
  }

  get_state =() =>{
    axios
      .get(
        "http://localhost:5000/api/non_ring/")
      .then((res) => {
        this.setState({current_stage: res.data.ui_state})
      });
  }

  reset = () => {
    this.setState({
      tool_selected: "",
      current_stage: "loading",
    });
    //send action call to non ring ib to change the width of the belt
  };

  selectTool = (e) => {
    this.setState({
      current_stage: "confirmation",
    });
    console.log("http://localhost:5000/api/non_ring/belt_width/" + e.target.id)
    axios
      .get(
        "http://localhost:5000/api/non_ring/belt_width/" + e.target.id
      )
      .then((res) => {
        if (res.data.success) {
          this.setState({ tool_selected: e.target.id, });
        } else {
          this.setState({ refill_state: "error" });
        }
      });
  };

  startInspection = () => {
    this.setState({
      current_stage: "inspecting",
    });
    axios
      .get(
        "http://localhost:5000/api/non_ring/start_inspection")
      .then((res) => {
        if (res.data.success) {
          this.setState({ current_stage: "finished",})
        } else {
          this.setState({ refill_state: "error" });
        }
      });
  };

  render() {
    return (
      <React.Fragment>
        <Col md={12}>
          <h1>Main operation</h1>
        </Col>
        <br />
        <Col md={12}>
          <Card>
            <Card.Header>
              <h2>Select Instrument</h2>
            </Card.Header>
            <Collapse
              in={this.state.current_stage === "loading" ? true : false}
            >
              <Card.Body>
                <h4>
                  Select one of the 3 types of instrument to be loaded in:
                </h4>
                <Row>
                  <Col md={4}>
                    <Figure className="d-block mx-auto">
                      <Figure.Image
                        className="d-block mx-auto"
                        fluid={true}
                        src="https://via.placeholder.com/500x500"
                        onClick={this.selectTool}
                        id="tcj"
                      />
                      <Figure.Caption style={{ textAlign: "center" }}>
                        <h5>Towel Clip Jones</h5>
                      </Figure.Caption>
                    </Figure>
                  </Col>
                  <Col md={4}>
                    <Figure className="d-block mx-auto">
                      <Figure.Image
                        className="d-block mx-auto"
                        fluid={true}
                        src="https://via.placeholder.com/500x5s00"
                        onClick={this.selectTool}
                        id="spr"
                      />
                      <Figure.Caption style={{ textAlign: "center" }}>
                        <h5>Spear Redivac</h5>
                      </Figure.Caption>
                    </Figure>
                  </Col>
                  <Col md={4}>
                    <Figure className="d-block mx-auto">
                      <Figure.Image
                        className="d-block mx-auto"
                        fluid={true}
                        src="https://via.placeholder.com/500x500"
                        onClick={this.selectTool}
                        id="oth"
                      />
                      <Figure.Caption style={{ textAlign: "center" }}>
                        <h5>Others</h5>
                      </Figure.Caption>
                    </Figure>
                  </Col>
                </Row>
              </Card.Body>
            </Collapse>
          </Card>
          <br />
          <Card>
            <Card.Header>
              <h2>Confirmation</h2>
            </Card.Header>
            <Collapse
              in={this.state.current_stage === "confirmation" ? true : false}
            >
              <Card.Body>
                <Row>
                  <Col md={12}>
                    <h4>You have selected to load in:</h4>
                    <h4 style={{ color: "red" }}>
                      {this.state.hashmap[this.state.tool_selected]}
                    </h4>
                    <h4>
                      Please load in the tool as shown and press "Start
                      Inspection"
                    </h4>
                    <Image
                      className="d-block mx-auto"
                      fluid={true}
                      src="https://via.placeholder.com/500x500"
                    />
                  </Col>
                </Row>
                <br />
                <Row>
                  <Col md={6}>
                    <Button
                      variant="warning"
                      style={{ width: "100%" }}
                      onClick={this.reset}
                    >
                      Change Instrument
                    </Button>
                  </Col>
                  <Col md={6}>
                    <Button
                      variant="success"
                      style={{ width: "100%" }}
                      onClick={this.startInspection}
                    >
                      Start Inspection
                    </Button>
                  </Col>
                </Row>
              </Card.Body>
            </Collapse>
          </Card>
          <br />
          <Card>
            <Card.Header>
              <h2>
                Inspection in Progress...
                <Spinner
                  hidden={this.state.current_stage === "inspecting" ? false: true}
                  animation="border"
                  className="float-end"
                  role="status"
                >
                  <span className="visually-hidden">Loading...</span>
                </Spinner>{" "}
              </h2>
            </Card.Header>
            <Collapse
              in={this.state.current_stage === "inspecting" || this.state.current_stage === "finished" ? true : false}
            >
              <Card.Body>
                <h4 hidden={this.state.current_stage === "inspecting" ? false: true}>
                  Inspection in progress, please wait patiently...
                </h4>
                <Row hidden={this.state.current_stage === "finished" ? false: true}>
                  <Col md={12}>
                    <h4>
                      Inspection has completed, item is{" "}
                      {this.state.is_defective ? "defective" : "non-defective"}{" "}
                      and have been transferred to the subsequent station
                    </h4>
                  </Col>
                  <Col md={6}>
                    <Button
                      variant="warning"
                      style={{ width: "100%" }}
                      onClick={this.endSession}
                    >
                      End Session
                    </Button>
                  </Col>
                  <Col md={6}>
                    <Button
                      variant="success"
                      style={{ width: "100%" }}
                      onClick={this.reset}
                    >
                      Load next item
                    </Button>
                  </Col>
                </Row>
              </Card.Body>
            </Collapse>
          </Card>
        </Col>
      </React.Fragment>
    );
  }
}

export default MainOperation;
