import React, { Component } from "react";
import { Route, Routes, Link } from "react-router-dom";
import { Navbar, Nav, Container } from "react-bootstrap";

class UINavbar extends React.Component {
  render() {
    return (
      <React.Fragment>
      {/* <Link to="/"> Main Operation </Link>
      <Link to="/system_control">System Control</Link> */}
      <Navbar variant="dark" bg="dark" expand="lg" text="white">
      <Container>
        <Navbar.Brand href="#home">Non Ring Inspection Bay</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="me-auto">
            <Link className="nav-item nav-link" to="/"> Main Operation </Link>
            <Link className="nav-item nav-link" to="/system_control">System Control</Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
    </React.Fragment>
      // <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
      //   <a className="navbar-brand m-2" href="#">
      //     Non Ring Inspection Bay UI
      //   </a>
      //   <button
      //     className="navbar-toggler"
      //     type="button"
      //     data-toggle="collapse"
      //     data-target="#navbarNavAltMarkup"
      //     aria-controls="navbarNavAltMarkup"
      //     aria-expanded="false"
      //     aria-label="Toggle navigation"
      //   >
      //     <span className="navbar-toggler-icon"></span>
      //   </button>
      //   <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
      //     <div className="navbar-nav">
      //       <Link to="/" className="nav-item nav-link">
      //         Main Operation
      //       </Link>
      //       <Link to="/system_control" className="nav-item nav-link">
      //         System Control
      //       </Link>
      //       <Link to="/instrument" className="nav-item nav-link">
      //         Processed Instruments
      //       </Link>
      //     </div>
      //   </div>
      // </nav>
    );
  }
}

export default UINavbar;
