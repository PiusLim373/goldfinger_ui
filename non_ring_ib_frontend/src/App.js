import React, { Component } from "react";
import { BrowserRouter , Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

import UINavbar from "./components/navbar";
import MainOperation from "./components/main_operation";
import SystemControl from "./components/system_control";
// import ProcessedInstruments from "./components/processed_instruments";
// import InstrumentDetail from "./components/instrument_detail";
// import Consumables from "./components/consumables";
// import InstrumentInspectionProof from "./components/instrument_inspection_proof";
// import Spares from "./components/spares";

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      // model_state: false,
      // model_data: 0,
    };
  }

  // showModel(e) {
  //   console.log(e);
  //   this.setState({
  //     model_state: true,
  //     model_data: e,
  //   });
  // }

  // hideModel = () => {
  //   this.setState({
  //     model_state: false,
  //   });
  // };

  render() {
    return (
      <BrowserRouter>
        <div className="container">
          <UINavbar />
          <br />
          
          <Route  path="/" exact component={MainOperation} />
          <Route  path="/system_control" component={SystemControl} />
          
          {/* <Route path="/instrument" exact component={ProcessedInstruments} />
          <Route path="/consumables" exact component={Consumables} />
          <Route path="/spares" exact component={Spares} />
          <Route
            path="/instrument/:box_id"
            render={(props) => (
              <InstrumentDetail
                onShow={(e) => this.showModel(e, "value")}
                {...props}
              />
            )}
          />
          <InstrumentInspectionProof
            model_state={this.state.model_state}
            model_data={this.state.model_data}
            onHide={this.hideModel}
          /> */}
        </div>
        </BrowserRouter>
    );
  }
}

export default App;
