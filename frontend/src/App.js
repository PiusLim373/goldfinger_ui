import React, { Component } from "react";
import axios from "axios";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.css";

import Header from "./components/navbar";
import MainOperation from "./components/main_operation";
import SystemControl from "./components/system_control";
import ProcessedInstruments from "./components/processed_instruments";
import InstrumentDetail from "./components/instrument_detail";
import Consumables from "./components/consumables";
import InstrumentInspectionProof from "./components/instrument_inspection_proof";
import Spares from "./components/spares";
import SimplifiedUI from "./components/simplified_ui";
import Home from "./components/home";
import SimplifiedSetting from "./components/simplified_setting";
import BootupShutdownModal from "./components/shutdown";

const ip = "localhost";
var internal_spawn = false;
class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model_state: false,
      model_data: 0,
      ///
      bootup_shutdown_modal_state: false,
      bootup_shutdown_modal_text:
        "System booting up, this will take up to 1 minute...",
      ///
    };
    this.check_bootup_shutdown();
  }

  showModel(e) {
    this.setState({
      model_state: true,
      model_data: e,
    });
  }

  hideModel = () => {
    this.setState({
      model_state: false,
    });
  };

  toggle_bootup_shutdown_modal_state = (e) => {
    this.setState({
      bootup_shutdown_modal_state: !this.state.bootup_shutdown_modal_state,
      bootup_shutdown_modal_text: e,
    });
  };

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  check_bootup_shutdown = () => {
    axios
      .get("http://" + ip + ":5000/api/system_control/bootup_shutdown_checker")
      .then((res) => {
        console.log(res.data);
        if (res.data.shutdown_in_progress)
          this.setState({
            bootup_shutdown_modal_state: true,
            bootup_shutdown_modal_text: "System shutting down...",
          });
        else if (res.data.bootup_in_progress) {
          this.setState({
            bootup_shutdown_modal_state: true,
            bootup_shutdown_modal_text:
              "System booting up, this will take up to 1 minute...",
          });
          if (!internal_spawn) {
            this.interval = setInterval(() => {
              this.check_bootup_shutdown();
            }, 1000);
            internal_spawn = true;
          }
        } else {
          this.setState({
            bootup_shutdown_modal_state: false,
            bootup_shutdown_modal_text: "",
          });
          clearInterval(this.interval);
        }
      });
  };

  render() {
    return (
      <Router>
        <div className="container">
          <Header />
          <br />

          <Route path="/" exact component={SimplifiedUI} />
          {/* <Route path="/setting" exact component={SimplifiedSetting} /> */}
          <Route
            path="/setting"
            render={(props) => (
              <SimplifiedSetting
                toggle_bootup_shutdown_modal_state={
                  this.toggle_bootup_shutdown_modal_state
                }
                {...props}
              />
            )}
          />
          <Route path="/home" exact component={Home} />
          <Route path="/instrument" exact component={ProcessedInstruments} />
          <Route path="/consumables" exact component={Consumables} />
          <Route path="/spares" exact component={Spares} />
          <Route path="/old_main_operation" exact component={MainOperation} />
          <Route path="/system_control" exact component={SystemControl} />

          <Route
            path="/instrument/:box_id"
            render={(props) => (
              <InstrumentDetail
                onShow={(e) => this.showModel(e, "value")}
                {...props}
              />
            )}
          />

          <InstrumentInspectionProof
            model_state={this.state.model_state}
            instrument_type={this.state.model_data.instrument_type}
            instrument_data={this.state.model_data.instrument_data}
            onHide={this.hideModel}
          />
          <BootupShutdownModal
            bootup_shutdown_modal_state={this.state.bootup_shutdown_modal_state}
            bootup_shutdown_modal_text={this.state.bootup_shutdown_modal_text}
          />
        </div>
      </Router>
    );
  }
}

export default App;
