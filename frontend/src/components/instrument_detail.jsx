import React, { Component, useState } from "react";
import axios from "axios";
import { Tabs, Tab, Table, Nav, Col, Row, Button } from "react-bootstrap";
import IndividualInstrument from "./individual_instrument";

const ip = "192.168.10.100";

class InstrumentDetail extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      box_id: parseInt(this.props.match.params.box_id),

      // box_id: 5,
      tab_key: 1,
      instrument_detail: {},
      nri_list: [],
      nri_namelist: {
        TCJ: "Towel Clip Jones",
        FOR1: "Forceps Mcindoe",
        FOR2: "Forceps Gillies",
        BAP: "Bard Parkers",
        SPR: "Spear Redivac",
        RUL: "Ruler Multipurposes",
        RTL1: "Retractors Langenback - Adult",
        RTL2: "Retractors Langenback - Baby",
      },
      ri_list: [],
      ri_namelist: {
        FAA: "Forcep Artery Adson, Curved, 180mm",
        FAC: "Forcep Artery Crile, Curved, 140mm",
        FSR: "Forcep Sponge-Holding Rampley, Straight, 250mm",
        FTL: "Forcep Tissue Littlewood, 191mm",
        FTB: "Forcep Tissue Babcock 6', 150mm",
        FTS: "Forcep Tissues Stilles",
        HNC: "Holder Needle Crilewood, Tungstun Carbide, 150mm",
        HNM: "Holder Needle Mayo Hegar, Tungstun Carbide, 150mm, Jaw 19x3mm",
        SDM: "Scissor Dissecting Metzenbaum, curved, 180mm",
        SDN: "Scissor Dressing Nurses, Blunt, Sharp, Straight, 165mm",
        SCM: "Scissor Mayo, rounded, straight, 170mm",
      },
    };
    axios
      .get(`http://${ip}:5000/api/instrument_set/${this.state.box_id}`)
      .then((res) => {
        if (res.data) {
          this.setState({
            instrument_detail: res.data,
            nri_list: Object.keys(res.data.content.non_ring_instrument),
            ri_list: Object.keys(res.data.content.ring_instrument),
          });
        }
        // console.log(res.data.status);
      });
  }

  componentWillMount() {
    console.log(this.props);
  }

  handleSelect = (key) => {
    this.setState({ tab_key: key });
  };

  render_datetime(arg) {
    const datetime = new Date(arg);
    const datetime_str =
      datetime.getFullYear() +
      "-" +
      (datetime.getMonth() + 1) +
      "-" +
      datetime.getDate() +
      "  " +
      datetime.getHours() +
      ":" +
      datetime.getMinutes() +
      ":" +
      datetime.getSeconds();
    return datetime_str;
  }

  count_defective(arg) {
    var defective_counter = 0;
    for (var x of arg) {
      if (x["inspection_result"] === "fail") defective_counter++;
    }
    return defective_counter;
  }

  render_nri() {
    if (this.state.nri_list.length) {
      return (
        <React.Fragment>
          {this.state.nri_list.map((nri) => (
            <Tab.Pane eventKey={nri} key={nri}>
              <h2>Overview</h2>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Property</th>
                    <th>Data</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Total Processed Instrument Count</td>
                    <td>
                      {
                        this.state.instrument_detail.content
                          .non_ring_instrument[nri].length
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>Total Defective Instrument</td>
                    <td>
                      {this.count_defective(
                        this.state.instrument_detail.content
                          .non_ring_instrument[nri]
                      )}
                    </td>
                  </tr>
                </tbody>
              </Table>

              {this.state.instrument_detail.content.non_ring_instrument[nri]
                .length != 0 ? (
                <IndividualInstrument
                  instrument_data={
                    this.state.instrument_detail.content.non_ring_instrument[
                      nri
                    ]
                  }
                  instrument_type="non_ring"
                  box_id={this.state.instrument_detail.box_id}
                  showModel={(e) => this.props.onShow(e, "value")}
                />
              ) : null}
            </Tab.Pane>
          ))}
        </React.Fragment>
      );
    }
  }

  render_ri() {
    if (this.state.ri_list.length) {
      return (
        <React.Fragment>
          {this.state.ri_list.map((ri) => (
            <Tab.Pane eventKey={ri} key={ri}>
              <h2>Overview</h2>
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Property</th>
                    <th>Data</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Total Processed Instrument Count</td>
                    <td>
                      {
                        this.state.instrument_detail.content.ring_instrument[ri]
                          .length
                      }
                    </td>
                  </tr>
                  <tr>
                    <td>Total Defective Instrument</td>
                    <td>
                      {this.count_defective(
                        this.state.instrument_detail.content.ring_instrument[ri]
                      )}
                    </td>
                  </tr>
                </tbody>
              </Table>

              {this.state.instrument_detail.content.ring_instrument[ri]
                .length != 0 ? (
                <IndividualInstrument
                  instrument_data={
                    this.state.instrument_detail.content.ring_instrument[ri]
                  }
                  instrument_type="ring"
                  box_id={this.state.instrument_detail.box_id}
                  showModel={(e) => this.props.onShow(e, "value")}
                />
              ) : null}
            </Tab.Pane>
          ))}
        </React.Fragment>
      );
    }
  }

  //############################################################ Debug
  debug() {}
  //############################################################ Debug Ends
  render() {
    const { instrument_detail } = this.state;
    //############################################################ Debug
    this.debug();

    //############################################################ Debug Ends
    return (
      <React.Fragment>
        <div>
          <h1>Instrument Set #{this.state.box_id}</h1>
        </div>
        <Tabs
          id="controlled-tab-example"
          activeKey={this.state.key}
          onSelect={this.handleSelect}
          className="mb-3"
        >
          <Tab eventKey={1} title="Overview">
            <Table striped bordered hover style={{ verticalAlign: "middle" }}>
              <thead>
                <tr>
                  <th>Property</th>
                  <th>Data</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>Box ID</td>
                  <td>{instrument_detail.box_id}</td>
                </tr>
                <tr>
                  <td>Box Model</td>
                  <td>{instrument_detail.model}</td>
                </tr>
                <tr>
                  <td>Input DateTime</td>
                  <td>
                    {instrument_detail.input_time === 0
                      ? "N/A"
                      : this.render_datetime(instrument_detail.input_time)}
                  </td>
                </tr>
                <tr>
                  <td>Output DateTime</td>
                  <td>
                    {instrument_detail.output_time === 0
                      ? "Processing..."
                      : this.render_datetime(instrument_detail.output_time)}
                  </td>
                </tr>
                {/* <tr>
                  <td>Sticker Tag</td>
                  <td></td>
                </tr> */}
                {/* <tr>
                  <td>Log Sheet</td>
                  <td>
                    <Button
                      variant="outline-primary"
                      disabled={
                        this.state.instrument_detail.status === "completed"
                          ? false
                          : true
                      }
                      href={`/database/logsheet/logsheet_${this.state.box_id}.pdf`}
                      target="_blank"
                    >
                      Open PDF
                    </Button>
                  </td>
                </tr> */}
              </tbody>
            </Table>
          </Tab>
          <Tab eventKey={2} title="Non Ring Instrument">
            <Tab.Container id="left-tabs-example" defaultActiveKey="BAP">
              <Row>
                <Col sm={3}>
                  <Nav variant="pills" className="flex-column">
                    {this.state.nri_list.map((nri) => (
                      <Nav.Item>
                        <Nav.Link key={nri} eventKey={nri}>
                          {this.state.nri_namelist[nri]}
                        </Nav.Link>
                      </Nav.Item>
                    ))}
                  </Nav>
                </Col>
                <Col sm={9}>
                  <Tab.Content>{this.render_nri()}</Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
          </Tab>
          <Tab eventKey={3} title="Ring Instrument">
            <Tab.Container id="left-tabs-example" defaultActiveKey="FAA">
              <Row>
                <Col sm={4}>
                  <Nav variant="pills" className="flex-column">
                    {this.state.ri_list.map((ri) => (
                      <Nav.Item>
                        <Nav.Link key={ri} eventKey={ri}>
                          {this.state.ri_namelist[ri]}
                        </Nav.Link>
                      </Nav.Item>
                    ))}
                  </Nav>
                </Col>
                <Col sm={8}>
                  <Tab.Content>{this.render_ri()}</Tab.Content>
                </Col>
              </Row>
            </Tab.Container>
          </Tab>
          {/* <Tab eventKey={4} title="Others">
            <p>123444</p>
          </Tab> */}
        </Tabs>
      </React.Fragment>
    );
  }
}

export default InstrumentDetail;
