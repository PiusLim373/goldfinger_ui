import React, { Component } from "react";
import axios from "axios";
import { Link } from "react-router-dom";

const ip = "192.168.10.100";

class ProcessedInstruments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      instrument_list: [],
      date: "2021-10-30T20:09:54.530Z",
    };
  }

  componentDidMount() {
    axios.get("http://" + ip + ":5000/api/instrument_set").then((res) => {
      if (res.data.length > 0) {
        this.setState({
          instrument_list: res.data,
        });
      }
    });
  }

  render_date(arg) {
    const date = new Date(arg);
    const date_str =
      date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + date.getDate();
    return date_str;
  }

  render_time(arg) {
    const time = new Date(arg);
    const time_str =
      time.getHours() + ":" + time.getMinutes() + ":" + time.getSeconds();
    return time_str;
  }

  render() {
    return (
      <div>
        <h1>Processed Instrument</h1>
        <table class="table">
          <thead>
            <tr>
              <th scope="col">Box ID</th>
              <th scope="col">Model</th>
              <th scope="col">Input Date</th>
              <th scope="col">Input Time</th>
              <th scope="col">Output Date</th>
              <th scope="col">Output Time</th>
              <th scope="col">Status</th>
            </tr>
          </thead>
          <tbody>
            {this.state.instrument_list
              .sort((a, b) => b.input_time - a.input_time)
              .map((set) => (
                <tr key={set.box_id}>
                  <th scope="row">{set.box_id}</th>
                  <td>{set.model}</td>
                  <td>{this.render_date(set.input_time)}</td>
                  <td>{this.render_time(set.input_time)}</td>

                  <td>
                    {set.output_time === 0
                      ? "Processing..."
                      : this.render_date(set.output_time)}
                  </td>
                  <td>
                    {set.output_time === 0
                      ? "Processing..."
                      : this.render_time(set.output_time)}
                  </td>
                  <td>
                    {
                      (set.status =
                        set.status[0].toUpperCase() + set.status.slice(1))
                    }
                  </td>
                  <td>
                    <Link to={`/instrument/${set.box_id}`}>
                      <span>More Detail</span>
                    </Link>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ProcessedInstruments;
