import React, { Component } from "react";
import { Modal, Row, Col, Image } from "react-bootstrap";
class InstrumentInspectionProof extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      model_state: this.props.model_state,
      instrument_type: this.props.instrument_type,
      instrument_data: this.props.instrument_data,
      top: [],
      front: [],
      side: [],
      slant: [],
      back: [],
      bottom: [],
    };
  }

  static getDerivedStateFromProps(nextProps, state) {
    return {
      model_state: nextProps.model_state,
      instrument_type: nextProps.instrument_type,
      instrument_data: nextProps.instrument_data,
      top: [],
      front: [],
      side: [],
      slant: [],
      back: [],
      bottom: [],
    };
  }

  group_img(data) {
    for (const img_path of data) {
      let individual_path = img_path.split("_");
      console.log(individual_path);
      console.log(
        `/database/images/${this.state.instrument_type}/${individual_path[0]}/${individual_path[2]}/${individual_path[3]}/${img_path}`
      );
      if (individual_path[4].includes("top"))
        this.state.top.push(
          `/database/images/${this.state.instrument_type}/${individual_path[0]}/${individual_path[2]}/${individual_path[3]}/${img_path}`
        );
      else if (individual_path[4].includes("side"))
        this.state.side.push(
          `/database/images/${this.state.instrument_type}/${individual_path[0]}/${individual_path[2]}/${individual_path[3]}/${img_path}`
        );
      else if (individual_path[4].includes("front"))
        this.state.front.push(
          `/database/images/${this.state.instrument_type}/${individual_path[0]}/${individual_path[2]}/${individual_path[3]}/${img_path}`
        );
      else if (individual_path[4].includes("slant"))
        this.state.slant.push(
          `/database/images/${this.state.instrument_type}/${individual_path[0]}/${individual_path[2]}/${individual_path[3]}/${img_path}`
        );
      else if (individual_path[4].includes("back"))
        this.state.back.push(
          `/database/images/${this.state.instrument_type}/${individual_path[0]}/${individual_path[2]}/${individual_path[3]}/${img_path}`
        );
      else if (individual_path[4].includes("bottom"))
        this.state.bottom.push(
          `/database/images/${this.state.instrument_type}/${individual_path[0]}/${individual_path[2]}/${individual_path[3]}/${img_path}`
        );
    }
  }

  render() {
    if (this.state.instrument_data)
      this.group_img(this.state.instrument_data.split(","));
    // try {

    // } catch (error) {
    //   console.log(error);
    // }
    return (
      <React.Fragment>
        <Modal
          size="xl"
          show={this.state.model_state}
          onHide={this.props.onHide}
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title>Inspection Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Row>
              <h5>Top Camera</h5>
              {this.state.top != 0 ? (
                this.state.top.map((img) => (
                  <Col md={6}>
                    <Image
                      className="d-block mx-auto"
                      src={img}
                      key={img}
                      style={{ marginBottom: "10px" }}
                      fluid
                    />
                  </Col>
                ))
              ) : (
                <p>no image found</p>
              )}
            </Row>
            <br />
            <Row>
              <h5>Front Camera</h5>
              {this.state.front != 0 ? (
                this.state.front.map((img) => (
                  <Col md={6}>
                    <Image
                      className="d-block mx-auto"
                      src={img}
                      key={img}
                      style={{ marginBottom: "10px" }}
                      fluid
                    />
                  </Col>
                ))
              ) : (
                <p>no image found</p>
              )}
            </Row>
            <br />
            <Row>
              <h5>Side Camera</h5>
              {this.state.side != 0 ? (
                this.state.side.map((img) => (
                  <Col md={6}>
                    <Image
                      className="d-block mx-auto"
                      src={img}
                      key={img}
                      style={{ marginBottom: "10px" }}
                      fluid
                    />
                  </Col>
                ))
              ) : (
                <p>no image found</p>
              )}
            </Row>
            <br />
            <Row>
              <h5>Slant Camera</h5>
              {this.state.slant != 0 ? (
                this.state.slant.map((img) => (
                  <Col md={6}>
                    <Image
                      className="d-block mx-auto"
                      src={img}
                      key={img}
                      style={{ marginBottom: "10px" }}
                      fluid
                    />
                  </Col>
                ))
              ) : (
                <p>no image found</p>
              )}
            </Row>
            <br />
            <Row>
              <h5>Back Camera</h5>
              {this.state.back != 0 ? (
                this.state.back.map((img) => (
                  <Col md={6}>
                    <Image
                      className="d-block mx-auto"
                      src={img}
                      key={img}
                      style={{ marginBottom: "10px" }}
                      fluid
                    />
                  </Col>
                ))
              ) : (
                <p>no image found</p>
              )}
            </Row>
            <br />
            <Row>
              <h5>Bottom Camera</h5>
              {this.state.bottom != 0 ? (
                this.state.bottom.map((img) => (
                  <Col md={6}>
                    <Image
                      className="d-block mx-auto"
                      src={img}
                      key={img}
                      style={{ marginBottom: "10px" }}
                      fluid
                    />
                  </Col>
                ))
              ) : (
                <p>no image found</p>
              )}
            </Row>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

export default InstrumentInspectionProof;
