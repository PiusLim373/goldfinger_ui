import React from "react";
import {
  Row,
  Col,
  Spinner,
  Alert,
  Button,
  Card,
  Modal,
  Table,
  ListGroup,
} from "react-bootstrap";
import axios from "axios";

import CustomAlertBox from "./simplified_ui_alert_box";
const ip = "192.168.10.100";

class SimplifiedUI extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loading: true,
      ///
      program_stage: "preprocess_checking",
      ///
      pre_process_error: false,
      pre_process_error_stage: "",
      pre_process_error_text_1: "",
      pre_process_error_text_2: [],
      pre_process_error_text_3: "",
      pre_process_button_text: "",
      pre_process_button_value: "",
      ///
      wetbay_input: [0, 0, 0, 0, 0, 0],
      drybay_input: [0, 0, 0, 0, 0, 0],
      pigeonhole_to_process: [],
      ///
      start_process_modal_state: false,
      ///
      main_program_message: "",
      main_program_error: false,
      ///
      main_program_ended: false,
      ///
      error_rectification_modal: false,
      error_message: "",
    };

    this.check_main_program_running();
  }

  check_main_program_running() {
    axios
      .get("http://" + ip + ":5000/api/system_control/main_program_status")
      .then((res) => {
        if (res.data) {
          console.log(res.data);
          if (res.data.main_program_status === true) {
            this.setState({ program_stage: "start" });
            this.interval = setInterval(() => {
              this.update_program_text();
            }, 1000);
          } else {
            this.rosnode_check();
          }
        }
      });
  }

  rosnode_check() {
    axios
      .get(
        "http://" +
          ip +
          ":5000/api/system_control/main_operation_page_quick_check"
      )
      .then((res) => {
        if (!res.data.success) {
          //some nodes aren't online or not activated
          this.setState({
            pre_process_error_text_1:
              "The following modules aren't online or not powered:",
            pre_process_error_text_2: res.data.problematic_module,
            pre_process_error_text_3: "Check the setting page for more detail.",
            pre_process_button_text: "Go to Setting Page",
            pre_process_button_value: "go_to_setting_page",
            pre_process_error: true,
            pre_process_error_stage: "rosnode_check",
            is_loading: false,
          });
        } else {
          this.setState({
            pre_process_error: false,
          });
          this.consumables_check();
        }
      });
  }

  consumables_check() {
    console.log("consuables");
    axios.get("http://" + ip + ":5000/api/consumables/").then((res) => {
      if (res.data) {
        var is_enough = true;
        for (const con of res.data.consumables) {
          if (con.remaining <= 10) {
            is_enough = false;
            this.setState({
              is_loading: false,
              pre_process_error: true,
              pre_process_error_text_1:
                "One of more consumables is insufficient",
              pre_process_error_text_2: [],
              pre_process_error_text_3: "",
              pre_process_button_text: "Refill Consumables",
              pre_process_button_value: "go_to_consumables_page",
              pre_process_error_stage: "consumables",
            });
            break;
          }
        }
        if (is_enough === true) this.spares_check();
      }
    });
  }

  spares_check() {
    axios.get("http://" + ip + ":5000/api/spares/").then((res) => {
      if (res.data) {
        var is_enough = true;
        for (const nri of res.data.non_ring_spares) {
          if (nri.remaining / nri.capacity <= 0.2) {
            is_enough = false;
            this.setState({
              is_loading: false,
              pre_process_error: true,
              pre_process_error_text_1:
                "Non-Ring Spare Instruments is insufficient",
              pre_process_error_text_2: [],
              pre_process_error_text_3: "",
              pre_process_button_text: "Refill Spares",
              pre_process_button_value: "go_to_spares_page",
              pre_process_error_stage: "spares",
            });
            break;
          }
        }
        if (is_enough) {
          for (const ri of res.data.ring_spares) {
            if (ri.remaining / ri.capacity <= 0.2) {
              is_enough = false;
              this.setState({
                is_loading: false,
                pre_process_error_text_1:
                  "Ring Spare Instruments is insufficient",
                pre_process_error_text_2: [],
                pre_process_error_text_3: "",
                pre_process_button_text: "Refill Spares",
                pre_process_button_value: "go_to_spares_page",
                pre_process_error: true,
                pre_process_error_stage: "spares",
              });
              break;
            }
          }
        }
        if (is_enough === true) {
          this.setState({ program_stage: "input" }, () => this.input_check());
        }
      }
    });
  }

  input_check = () => {
    axios.get("http://" + ip + ":5000/api/input_condition/").then((res) => {
      if (res.data.success === true) {
        var pigeonhole_to_process = [];
        for (var i = 0; i < 6; i++) {
          if (res.data.wetbay_input[i] === 1 && res.data.drybay_input[i] === 11)
            pigeonhole_to_process.push(i);
        }
        console.log(pigeonhole_to_process);
        this.setState(
          {
            wetbay_input: res.data.wetbay_input,
            drybay_input: res.data.drybay_input,
            pigeonhole_to_process: pigeonhole_to_process,
          },
          () => {
            this.setState({
              is_loading: false,
            });
          }
        );
        // this.setState({ wetbay_input: res.sdata.wetbay_input, drybay_input: res.data.drybay_input,  program_stage: "start" }, () => { this.setState({ is_loading: false }); });
      } else {
        this.setState({
          // is_loading: false,
          pre_process_error_text_1: "Error during pigeonhole checking",
          pre_process_error_text_2: [],
          pre_process_error_text_3: "",
          pre_process_button_text: "Reload",
          pre_process_error: true,
          pre_process_error_stage: "input",
        });
      }
    });
  };

  reset_led = () => {
    axios
      .get("http://" + ip + ":5000/api/input_condition/reset_led")
      .then((res) => {
        if (res.data.success === true) {
          this.input_check();
        }
      });
  };

  exclude_process = (e) => {
    this.setState({
      pigeonhole_to_process: this.state.pigeonhole_to_process.filter(
        (element) => element !== parseInt(e.target.value)
      ),
    });
  };

  update_program_text = () => {
    axios
      .get("http://" + ip + ":5000/api/system_control/main_program_message")
      .then((res) => {
        if (res.data) {
          if (res.data.main_program_status === false)
            this.setState({
              main_program_ended: true,
            });
          this.setState({
            main_program_message: res.data.main_program_message,
            main_program_error: res.data.main_program_error_message,
            is_loading: false,
          });
        }
      });
    axios.get("http://" + ip + ":5000/api/error_rectification/").then((res) => {
      // if (res.data.error === true) {
      if (res.data) {
        this.setState({
          error_rectification_modal: res.data.error,
          error_message: res.data.error_message,
        });
      }
    });
  };

  componentWillUnmount() {
    clearInterval(this.interval); //remove the pooling
  }

  render_pg_selection() {
    if (this.state.pigeonhole_to_process.length) {
      return (
        <React.Fragment>
          <h2>The following pigeonholes are ready to be processed:</h2>
          <ListGroup variant="flush" style={{ fontSize: "20px" }}>
            {this.state.pigeonhole_to_process.map((pg) => (
              <ListGroup.Item key={pg}>
                {`Pigeonhole #${pg + 1}`}
                <Button
                  variant="danger"
                  value={pg}
                  onClick={this.exclude_process}
                  style={{
                    float: "right",
                  }}
                >
                  Exclude process
                </Button>
              </ListGroup.Item>
            ))}
          </ListGroup>
        </React.Fragment>
      );
    } else {
      return (
        <h2 style={{ color: "red" }}>
          There is no available pigeonhole for processing.
        </h2>
      );
    }
  }

  toggle_start_process_modal_state = () => {
    this.setState({
      start_process_modal_state: !this.state.start_process_modal_state,
    });
  };

  launch_program_manager = () => {
    this.setState({
      program_stage: "start",
    });
    this.toggle_start_process_modal_state();
    //start message pooling timer also
    axios
      .post("http://" + ip + ":5000/api/system_control/program_manager", {
        pigeonhole_to_process: this.state.pigeonhole_to_process,
      })
      .then((res) => {
        this.setState({ program_stage: "start" }, () =>
          this.check_main_program_running()
        );
      });
  };

  reset = () => {
    clearInterval(this.interval);
    this.setState(
      {
        program_stage: "preprocess_checking",
        is_loading: true,
        ///
        pre_process_error: false,
        pre_process_error_stage: "",
        pre_process_error_text_1: "",
        pre_process_error_text_2: [],
        pre_process_error_text_3: "",
        pre_process_button_text: "",
        ///
        wetbay_input: [0, 0, 0, 0, 0, 0],
        drybay_input: [0, 0, 0, 0, 0, 0],
        pigeonhole_to_process: [],
        ///
        start_process_modal_state: false,
        ///
        main_program_message: "",
        main_program_error: false,
        ///
        main_program_ended: false,
        ///
        error_rectification_modal: false,
        error_message: "",
      },
      () => {
        this.check_main_program_running();
      }
    );
  };

  main_program_error_resolved = () => {
    axios.get("http://" + ip + ":5000/api/error_rectification/error_solved");
  };

  render() {
    console.log(this.tooltip_ref);
    if (this.state.is_loading === true) {
      return (
        <div
          style={{
            display: "flex",
            justifyContent: "center",
          }}
        >
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      );
    }
    return (
      <React.Fragment>
        <Row>
          <Col md={12}>
            <h1>Main Operation</h1>
            <br />
          </Col>
        </Row>
        <Row>
          <CustomAlertBox
            alert_error={this.state.pre_process_error}
            alert_text_1={this.state.pre_process_error_text_1}
            alert_text_2={this.state.pre_process_error_text_2}
            alert_text_3={this.state.pre_process_error_text_3}
            alert_button_text={this.state.pre_process_button_text}
            alert_button_value={this.state.pre_process_button_value}
          />
        </Row>
        <Row
          hidden={this.state.program_stage !== "input" ? true : false}
          className="justify-content-md-center"
        >
          <Col md={9}>{this.render_pg_selection()}</Col>
        </Row>
        <br />
        <Row hidden={this.state.program_stage !== "input" ? true : false}>
          <Col md={8}>
            <Button
              onClick={this.toggle_start_process_modal_state}
              variant={
                this.state.pigeonhole_to_process.length ? "success" : "danger"
              }
              disabled={this.state.pigeonhole_to_process.length ? false : true}
              style={{
                width: "100%",
                height: "100px",
                fontSize: "20px",
              }}
            >
              {this.state.program_stage === "start"
                ? "Program started, please wait"
                : this.state.pigeonhole_to_process.length !== 0
                ? "Process all available"
                : "None to process"}
            </Button>
          </Col>
          <Col md={2}>
            <Button
              onClick={this.input_check}
              variant="warning"
              style={{
                width: "100%",
                height: "100px",
                fontSize: "20px",
              }}
            >
              Refresh
            </Button>
          </Col>
          <Col md={2}>
            <Button
              onClick={this.reset_led}
              variant="warning"
              style={{
                width: "100%",
                height: "100px",
                fontSize: "20px",
              }}
            >
              Reset LED
            </Button>
          </Col>
        </Row>
        <Row hidden={this.state.program_stage === "start" ? false : true}>
          <Col md={12}>
            <h2>
              Program running...
              <Spinner
                animation="border"
                role="status"
                style={{
                  float: "right",
                }}
              >
                <span className="visually-hidden">Loading...</span>
              </Spinner>
            </h2>
            <br />
            <Alert
              className="mb-3"
              hidden={this.state.main_program_message === "" ? true : false}
              variant={
                this.state.main_program_error === true ? "danger" : "success"
              }
            >
              <h4>Feedback from program:</h4>
              {this.state.main_program_message}
            </Alert>
          </Col>
          {/* <Col md={12}>
            <Button
              variant="warning"
              style={{ width: "100%", height: "100px", fontSize: "20px" }}
            >
              Pause System
            </Button>
          </Col> */}
        </Row>

        <Modal
          size="xl"
          show={this.state.start_process_modal_state}
          onHide={this.toggle_start_process_modal_state}
        >
          <Modal.Header closeButton>
            <Modal.Title>Start Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>You are about start the BigBox instrument process for:</h5>
            <h3>
              {this.state.pigeonhole_to_process.map((pigeonhole, i) =>
                i !== this.state.pigeonhole_to_process.length - 1
                  ? "Pigeonhole #" + (pigeonhole + 1) + ", "
                  : this.state.pigeonhole_to_process.length === 1
                  ? "Pigeonhole #" + (pigeonhole + 1)
                  : " and Pigeonhole #" + (pigeonhole + 1)
              )}
            </h3>
            {/* <h3>{this.state.consumable_hashmap[this.state.modal_refill]}</h3> */}
            <h5 style={{ color: "red" }}>
              This action cannot be undone, continue?
            </h5>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="danger"
              value="input"
              onClick={this.toggle_start_process_modal_state}
            >
              Cancel and Review
            </Button>
            <Button variant="success" onClick={this.launch_program_manager}>
              Start Process
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={this.state.main_program_ended}>
          <Modal.Header>
            <Modal.Title>Process Completed</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>
              Main program finished successfully. Please proceed to unload from
              DryBay.
            </h5>
            <br />
            <Button
              style={{ width: "100%" }}
              variant="success"
              value="input"
              onClick={this.reset}
            >
              OK
            </Button>
          </Modal.Body>
        </Modal>
        <Modal size="lg" show={this.state.error_rectification_modal}>
          <Modal.Header>
            <Modal.Title style={{ color: "red" }}>
              Requesting Human Intervention
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>
              An error has occured and the system can't solve it internally,
              error details:
            </h5>
            <h5 style={{ color: "red" }}>{this.state.error_message}</h5>
            <h5>Latest message logged by program manager:</h5>
            <h5 style={{ color: "red" }}>
              {this.state.main_program_message === ""
                ? "None"
                : this.state.main_program_message}
            </h5>
            <h5>Press the button below after resolving to continue</h5>
            <br />
            <Button
              style={{ width: "100%" }}
              variant="warning"
              onClick={this.main_program_error_resolved}
            >
              Resolved and Continue
            </Button>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

export default SimplifiedUI;
