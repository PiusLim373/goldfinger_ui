import React, { Component, useState } from "react";
import { Alert, Button } from "react-bootstrap";
import { withRouter } from "react-router";

class CustomAlertBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert_error: this.props.alert_error,
      alert_text_1: this.props.alert_text_1,
      alert_text_2: this.props.alert_text_2,
      alert_text_3: this.props.alert_text_3,
      alert_button_text: this.props.alert_button_text,
      alert_button_value: this.props.alert_button_value,
    };
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        alert_error: this.props.alert_error,
        alert_text_1: this.props.alert_text_1,
        alert_text_2: this.props.alert_text_2,
        alert_text_3: this.props.alert_text_3,
        alert_button_text: this.props.alert_button_text,
        alert_button_value: this.props.alert_button_value,
      });
    }
  }

  alert_btn_function = (e) => {
    console.log(e.target.value);
    if (e.target.value === "go_to_main_operation_page") {
      this.props.history.push("/");
    } else if (e.target.value === "go_to_setting_page") {
      this.props.history.push("/setting");
    } else if (e.target.value === "go_to_consumables_page") {
      this.props.history.push("/consumables");
    } else if (e.target.value === "go_to_spares_page") {
      this.props.history.push("/spares");
    }
  };

  render() {
    return (
      <React.Fragment>
        <Alert
          id="rosnode_check_alert"
          className="mb-3"
          hidden={this.state.alert_text_1 ? false : true}
          variant={this.state.alert_error === true ? "danger" : "success"}
        >
          <h3>BigBox feedback messages: </h3>
          <h5>{this.state.alert_text_1}</h5>
          <ul>
            {this.state.alert_text_2.map((item) => (
              <li key={item}>{item}</li>
            ))}
          </ul>
          <h5>{this.state.alert_text_3}</h5>
          <Button
            hidden={this.state.alert_button_text ? false : true}
            style={{ width: "100%" }}
            variant="warning"
            value={this.state.alert_button_value}
            onClick={this.alert_btn_function}
          >
            {this.state.alert_button_text}
          </Button>
        </Alert>
      </React.Fragment>
    );
  }
}

export default withRouter(CustomAlertBox);
