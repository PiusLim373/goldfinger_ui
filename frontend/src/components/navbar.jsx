import React, { Component } from "react";
import { withRouter, Link } from "react-router-dom";
import { HouseFill, Gear } from "react-bootstrap-icons";
import { Navbar, Container, Nav } from "react-bootstrap";

class Header extends React.Component {
  constructor(props) {
    super(props);
  }

  route_home = () => {
    this.props.history.push("/home");
  };
  route_setting = () => {
    this.props.history.push("/setting");
  };

  render() {
    return (
      <React.Fragment>
        <Navbar variant="dark" bg="dark" expand="lg" text="white">
          <Container>
            <Navbar.Brand
              style={{ cursor: "pointer", fontSize: "30px" }}
              onClick={this.route_home}
            >
              <HouseFill></HouseFill>
            </Navbar.Brand>
            <Navbar.Brand
              style={{ cursor: "pointer", fontSize: "30px" }}
              onClick={this.route_setting}
            >
              <Gear></Gear>
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
              {/* <Nav className="me-auto">
                <Link to="/" className="nav-item nav-link">
                  Main Operation
                </Link>
                <Link to="/instrument" className="nav-item nav-link">
                  Processed Instruments
                </Link>
                <Link to="/consumables" className="nav-item nav-link">
                  Consumables
                </Link>
                <Link to="/spares" className="nav-item nav-link">
                  Spares Instruments
                </Link>
                <Link to="/old_main_operation" className="nav-item nav-link">
                  Old Main Operation
                </Link>
                <Link to="/system_control" className="nav-item nav-link">
                  System Control
                </Link>
              </Nav> */}
            </Navbar.Collapse>
          </Container>
        </Navbar>
      </React.Fragment>
    );
  }
}

export default withRouter(Header);
