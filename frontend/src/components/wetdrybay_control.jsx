import React from "react";
import axios from "axios";
import { Col, Row, Table, Button, Modal } from "react-bootstrap";

const ip = "192.168.10.100";
class WetDryBayController extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      wetbay_pneumatic: [],
      drybay_pneumatic: [],
      disable_drybay_3: false,
      disable_drybay_6: false,
      disable_wetbay_1: false,
      disable_wetbay_4: false,
    };
  }

  componentDidMount() {
    this.update_pg_state();
  }

  update_pg_state = () => {
    axios
      .get("http://" + ip + ":5000/api/wetbay_drybay_pneumatic")
      .then((res) => {
        if (res.data) {
          this.setState(
            {
              wetbay_pneumatic: res.data.wetbay_pneumatic,
              drybay_pneumatic: res.data.drybay_pneumatic,
            },
            () => {
              if (this.state.wetbay_pneumatic[0])
                this.setState({ disable_drybay_3: true });
              else this.setState({ disable_drybay_3: false });
              if (this.state.wetbay_pneumatic[3])
                this.setState({ disable_drybay_6: true });
              else this.setState({ disable_drybay_6: false });
              if (this.state.drybay_pneumatic[2])
                this.setState({ disable_wetbay_1: true });
              else this.setState({ disable_wetbay_1: false });
              if (this.state.drybay_pneumatic[5])
                this.setState({ disable_wetbay_4: true });
              else this.setState({ disable_wetbay_4: false });
            }
          );
        }
      });
  };

  // toggle_wetbay_pneumatic = (e) => {
  //   console.log("wetbay" + e.target.value);
  //   axios
  //     .post("http://" + ip + ":5000/api/wetbay_drybay_pneumatic/wetbay", {
  //       data: e.target.value,
  //     })
  //     .then((res) => {
  //       if (res.data) {
  //         this.update_pg_state();
  //       }
  //     });
  // };

  toggle_pneumatic = (e) => {
    console.log("toggle pneumatic" + e.target.value);
    axios
      .post("http://" + ip + ":5000/api/wetbay_drybay_pneumatic", {
        data: e.target.value,
      })
      .then((res) => {
        if (res.data) {
          if (res.status === 200) {
            console.log(res.data);
            if (res.data.success === true) {
              console.log("toggle successful");
              this.update_pg_state();
            } else {
              console.log("toggle unsuccessful");
              alert(res.data.message);
            }
          }
        }
      });
  };

  render() {
    return (
      <Row>
        <h2>Wetbay & Drybay Pneumatic Controller</h2>
        <Col md={6}>
          <Row>
            <h4>Wetbay</h4>
          </Row>
          <Row>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={0}
                onClick={this.toggle_pneumatic}
                disabled={this.state.disable_wetbay_1}
                variant={
                  this.state.disable_wetbay_1
                    ? "danger"
                    : this.state.wetbay_pneumatic[0]
                    ? "success"
                    : "outline-secondary"
                }
              >
                {this.state.disable_wetbay_1
                  ? "Disabled for safety"
                  : "Pigeonhole 1"}
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={1}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.wetbay_pneumatic[1]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 2
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={2}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.wetbay_pneumatic[2]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 3
              </Button>
            </Col>
          </Row>
          <Row className="mt-3">
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={3}
                onClick={this.toggle_pneumatic}
                disabled={this.state.disable_wetbay_4}
                variant={
                  this.state.disable_wetbay_4
                    ? "danger"
                    : this.state.wetbay_pneumatic[3]
                    ? "success"
                    : "outline-secondary"
                }
              >
                {this.state.disable_wetbay_4
                  ? "Disabled for safety"
                  : "Pigeonhole 4"}
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={4}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.wetbay_pneumatic[4]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 5
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={5}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.wetbay_pneumatic[5]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 6
              </Button>
            </Col>
          </Row>
        </Col>
        <Col md={6}>
          <Row>
            <h4>Drybay</h4>
          </Row>
          <Row>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={6}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.drybay_pneumatic[0]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 1
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={7}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.drybay_pneumatic[1]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 2
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={8}
                onClick={this.toggle_pneumatic}
                disabled={this.state.disable_drybay_3}
                variant={
                  this.state.disable_drybay_3
                    ? "danger"
                    : this.state.drybay_pneumatic[2]
                    ? "success"
                    : "outline-secondary"
                }
              >
                {this.state.disable_drybay_3
                  ? "Disabled for safety"
                  : "Pigeonhole 3"}
              </Button>
            </Col>
          </Row>
          <Row className="mt-3">
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={9}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.drybay_pneumatic[3]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 4
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={10}
                onClick={this.toggle_pneumatic}
                variant={
                  this.state.drybay_pneumatic[4]
                    ? "success"
                    : "outline-secondary"
                }
              >
                Pigeonhole 5
              </Button>
            </Col>
            <Col md={4}>
              <Button
                style={{ width: "100%" }}
                value={11}
                onClick={this.toggle_pneumatic}
                disabled={this.state.disable_drybay_6}
                variant={
                  this.state.disable_drybay_6
                    ? "danger"
                    : this.state.drybay_pneumatic[5]
                    ? "success"
                    : "outline-secondary"
                }
              >
                {this.state.disable_drybay_6
                  ? "Disabled for safety"
                  : "Pigeonhole 6"}
              </Button>
            </Col>
          </Row>
        </Col>
      </Row>
    );
  }
}

export default WetDryBayController;
