import ReactTimeAgo from "react-time-ago";
import TimeAgo from "javascript-time-ago";

import en from "javascript-time-ago/locale/en.json";
import React, { Component } from "react";
import { Table, Col, Button, Modal, Image, Spinner } from "react-bootstrap";
import axios from "axios";

TimeAgo.addDefaultLocale(en);

const ip = "192.168.10.100";

class Spares extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      main_program_status: false,
      non_ring_spares: [],
      ring_spares: [],
      modal_state: false,
      modal_refill: "",
      refill_state: "",
      spares_hashmap: {},
      is_loading: true,
    };
    this.get_spares();
    this.get_main_program_status();
  }

  get_spares() {
    axios.get("http://" + ip + ":5000/api/spares/").then((res) => {
      if (res.data) {
        this.setState(
          {
            non_ring_spares: res.data.non_ring_spares,
            ring_spares: res.data.ring_spares,
            refill_state: res.data.spares_ui_state.refill_state,
            modal_refill: res.data.spares_ui_state.which_spares,
            spares_hashmap: res.data.spares_hashmap,
          },
          () => {
            console.log(this.state);
            if (this.state.refill_state !== "prompting") {
              this.setState({ modal_state: true });
            }
          }
        );
      }
    });
  }

  get_main_program_status() {
    axios
      .get("http://" + ip + ":5000/api/system_control/main_program_status")
      .then((res) => {
        if (res.data) {
          this.setState({
            main_program_status: res.data.main_program_status,
            is_loading: false,
          });
        }
      });
  }

  refill = (e) => {
    this.setState({ modal_refill: e.target.value });
    this.modalHandler();
  };

  modalHandler = () => {
    if (
      this.state.refill_state === "refilling" ||
      this.state.refill_state === "door_error"
    ) {
      console.log("you cant dismiss me");
    } else {
      console.log("setting modal state");
      this.setState({ modal_state: !this.state.modal_state }, () => {
        console.log(this.state.modal_state);
      });
    }
  };

  unlockModuleDoor = () => {
    axios
      .get(
        "http://" +
          ip +
          ":5000/api/spares/unlock_door/" +
          this.state.modal_refill
      )
      .then((res) => {
        if (res.data.success) {
          this.setState({ refill_state: "refilling" });
        } else {
          this.setState({ refill_state: "error" });
        }
      });
  };

  completeRefill = () => {
    axios
      .get(
        "http://" +
          ip +
          ":5000/api/spares/check_door/" +
          this.state.modal_refill
      )
      .then((res) => {
        if (res.data.success) {
          axios
            .get(
              "http://" +
                ip +
                ":5000/api/spares/refill/" +
                this.state.modal_refill
            )
            .then((res) => {
              console.log(res.data);

              this.setState({ refill_state: "prompting" }, () => {
                this.get_spares();
                this.modalHandler();
              });
            });
        } else {
          //module door not close properly
          console.log("door error");
          this.setState({ refill_state: "door_error" });
        }
      });
  };

  render() {
    if (this.state.is_loading === true) {
      return (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      );
    }
    return (
      <React.Fragment>
        <Col md={12}>
          <h1>Spare Instruments Refill</h1>
          <br />
        </Col>
        <Col
          md={12}
          hidden={
            !this.state.main_program_status || this.state.is_loading === true
          }
        >
          <h2 style={{ color: "red" }}>Not Allowed while program is running</h2>
        </Col>
        <Col
          md={12}
          hidden={
            this.state.main_program_status || this.state.is_loading === true
          }
        >
          <h3>Non Ring Instruments</h3>
          <Table bordered hover style={{ verticalAlign: "middle" }}>
            <thead>
              <tr>
                <th>Spares Instruments</th>
                <th>Last Loaded</th>
                <th>Remaining</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.non_ring_spares.map((non_ring_spares) => (
                <tr
                  className={
                    non_ring_spares.remaining / non_ring_spares.capacity <= 0.2
                      ? "table-danger"
                      : non_ring_spares.remaining / non_ring_spares.capacity >=
                          0.2 &&
                        non_ring_spares.remaining / non_ring_spares.capacity <=
                          0.5
                      ? "table-warning"
                      : "table-success"
                  }
                  key={non_ring_spares.id}
                >
                  <td>{non_ring_spares.name}</td>
                  <td>
                    <ReactTimeAgo
                      date={non_ring_spares.time_last_refill}
                      locale="en-US"
                    />
                  </td>
                  <td>
                    {non_ring_spares.remaining +
                      " out of " +
                      non_ring_spares.capacity}
                  </td>
                  <td>
                    <Button
                      onClick={this.refill}
                      value={non_ring_spares.codename}
                    >
                      Refill
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>

        <Col
          md={12}
          hidden={
            this.state.main_program_status || this.state.is_loading === true
          }
        >
          <h3>Ring Instruments</h3>
          <Table bordered hover style={{ verticalAlign: "middle" }}>
            <thead>
              <tr>
                <th>Spares</th>
                <th>Last Loaded</th>
                <th>Remaining Level</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.ring_spares.map((ring_spares) => (
                <tr
                  className={
                    ring_spares.remaining / ring_spares.capacity <= 0.2
                      ? "table-danger"
                      : ring_spares.remaining / ring_spares.capacity >= 0.2 &&
                        ring_spares.remaining / ring_spares.capacity <= 0.5
                      ? "table-warning"
                      : "table-success"
                  }
                  key={ring_spares.id}
                >
                  <td>{ring_spares.name}</td>
                  <td>
                    <ReactTimeAgo
                      date={ring_spares.time_last_refill}
                      locale="en-US"
                    />
                  </td>
                  <td>
                    {ring_spares.remaining + " out of " + ring_spares.capacity}
                  </td>
                  <td>
                    <Button onClick={this.refill} value={ring_spares.codename}>
                      Refill
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
        <Col md={12}>
          <Button
            style={{ width: "100%", height: "100px", fontSize: "30px" }}
            variant="success"
            onClick={(e) => {
              this.props.history.push("/");
            }}
          >
            Go to Main Operation Page
          </Button>
        </Col>
        <br />
        <Modal
          size="xl"
          show={this.state.modal_state}
          onHide={this.modalHandler}
        >
          <Modal.Header closeButton>
            <Modal.Title>Refill Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>You are about to refill: </h5>
            <h3>{this.state.spares_hashmap[this.state.modal_refill]}</h3>
            <h5
              hidden={this.state.refill_state !== "prompting" ? true : false}
              style={{ color: "red" }}
            >
              This action cannot be undone, continue?
            </h5>
            <Button
              hidden={this.state.refill_state !== "prompting" ? true : false}
              variant="primary"
              onClick={this.unlockModuleDoor}
            >
              Unlock Consumables Door and Refill
            </Button>
            <div
              hidden={
                this.state.refill_state === "refilling" ||
                this.state.refill_state === "door_error"
                  ? false
                  : true
              }
            >
              <h5>Please follow the following guide to refill: </h5>
              <Image
                className="d-block mx-auto"
                src="https://via.placeholder.com/1920x1080"
                fluid
              />
              <br />
            </div>
            <h5
              hidden={this.state.refill_state === "door_error" ? false : true}
              style={{ color: "red" }}
            >
              Module's door not closed properly, please check and press the
              button again
            </h5>
          </Modal.Body>
          <Modal.Footer>
            <Button
              hidden={this.state.refill_state !== "prompting" ? true : false}
              variant="secondary"
              onClick={this.modalHandler}
            >
              Cancel
            </Button>
            <Button
              hidden={
                this.state.refill_state === "refilling" ||
                this.state.refill_state === "door_error"
                  ? false
                  : true
              }
              variant="primary"
              onClick={this.completeRefill}
            >
              Complete Refill
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Spares;
