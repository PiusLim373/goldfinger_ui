import React, { Component } from "react";
import axios from "axios";
import {
  Table,
  Tab,
  Nav,
  Col,
  Row,
  Card,
  ButtonGroup,
  Button,
  Spinner,
  Badge,
  Alert,
} from "react-bootstrap";
import { ArrowClockwise } from "react-bootstrap-icons";
import { Joystick } from "react-joystick-component";

class SystemControl extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert_danger: false,
      alert_text: "",
      ///
      plc_status: false,
      wetbay_camera_status: false,
      drybay_camera_status: false,
      pt1_camera_status: false,
      pt2_camera_status: false,
      ai_status: false,
      pcl_status: false,
      ur10_status: false,
      ur5_status: false,
      nrib_status: false,
      ///
      plcBootBtn: false,
      plcBootBtnLoading: true,
      ///
      cameraBootBtn: false,
      cameraBootBtnLoading: true,
      ur10BootBtn: false,
      ur10BootBtnLoading: true,
      ur5BootBtn: false,
      ur5BootBtnLoading: true,
      nribBootBtn: false,
      nribBootBtnLoading: true,
      ///
      ur10_robot_status: "Requesting...",
      ur10_program_status: "Requesting...",
      joystick_direction: "STOP",
    };
    this.update_state();
  }

  update_state() {
    axios.get("http://192.168.10.100:5000/api/system_control/").then((res) => {
      if (res.data) {
        this.setState({
          plc_status: res.data.plc_status,
          wetbay_camera_status: res.data.wetbay_camera_status,
          drybay_camera_status: res.data.drybay_camera_status,
          pt1_camera_status: res.data.pt1_camera_status,
          pt2_camera_status: res.data.pt2_camera_status,
          ai_status: res.data.ai_status,
          pcl_status: res.data.pcl_status,
          ur10_status: res.data.ur10_status,
          ur5_status: res.data.ur5_status,
          nrib_status: res.data.nrib_status,
          ur10_robot_status: res.data.ur10_robot_status,
          ur10_program_status: res.data.ur10_program_status,
        });
        if (res.data.plc_status) this.setState({ plcBootBtn: true });
        else this.setState({ plcBootBtn: false });

        if (
          res.data.wetbay_camera_status ||
          res.data.drybay_camera_status ||
          res.data.pt1_camera_status ||
          res.data.pt2_camera_status ||
          res.data.ai_status ||
          res.data.pcl_status
        )
          this.setState({
            cameraBootBtn: true,
          });
        else
          this.setState({
            cameraBootBtn: false,
          });
        if (res.data.ur10_status) this.setState({ ur10BootBtn: true });
        else this.setState({ ur10BootBtn: false });
        if (res.data.ur5_status) this.setState({ ur5BootBtn: true });
        else this.setState({ ur5BootBtn: false });
        if (res.data.nrib_status) this.setState({ nribBootBtn: true });
        else this.setState({ nribBootBtn: false });

        this.setState({
          plcBootBtnLoading: false,
          cameraBootBtnLoading: false,
          ur10BootBtnLoading: false,
          ur5BootBtnLoading: false,
          nribBootBtnLoading: false,
        });
      }
    });
  }

  componentDidMount() {
    // this.interval = setInterval(() => this.update_state(), 5000);
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  module_action = (e) => {
    const module = e.target.id;
    const boot_or_kill = e.target.value;
    if (module === "camera") {
      this.setState({ cameraBootBtnLoading: true });
    } else if (module === "ur10") {
      this.setState({ ur10BootBtnLoading: true });
    } else if (module === "ur5") {
      this.setState({ ur5BootBtnLoading: true });
    } else if (module === "ib1") {
      this.setState({ nribBootBtnLoading: true });
    } else if (module === "plc") {
      this.setState({ plcBootBtnLoading: true });
    }
    axios
      .get(
        "http://192.168.10.100:5000/api/system_control/" +
          boot_or_kill +
          "/" +
          module
      )
      .then((res) => {
        console.log(res);
        this.setState({
          alert_danger: !res.data.success,
          alert_text: res.data.message,
        });
        // if (module === "camera") this.setState({ cameraBootBtnLoading: false });
        // else if (module === "ur10")
        //   this.setState({ ur10BootBtnLoading: false });
        // else if (module === "ur5") this.setState({ ur5BootBtnLoading: false });
        this.update_state();
      });
  };

  handleMove = (e) => {
    if (e.direction !== this.state.joystick_direction) {
      this.setState({ joystick_direction: e.direction });
    }
  };

  handleStop = (e) => {
    this.setState({ joystick_direction: "STOP" });
  };

  handleButtonHold = (e) => {
    this.setState({ joystick_direction: e.target.value });
  };

  handleButtonRelease = (e) => {
    this.setState({ joystick_direction: "STOP" });
  };

  render() {
    return (
      <div>
        <h1>System Control</h1>
        <Alert
          className="mb-2"
          hidden={this.state.alert_text === "" ? true : false}
          variant={this.state.alert_danger === true ? "danger" : "success"}
        >
          {this.state.alert_text}
        </Alert>

        <Row className="align-items-center md-2">
          <Col md={1}>
            <Button
              className="float-end"
              size="lg"
              style={{ height: "90px" }}
              variant="primary"
              onClick={() => this.update_state()}
            >
              <ArrowClockwise></ArrowClockwise>
            </Button>
          </Col>
          <Col>
            <Card bg="light" text="dark" className="mb-2">
              <Card.Body>
                <Card.Title>Modules Status</Card.Title>
                <Badge
                  bg={this.state.plc_status === true ? "success" : "danger"}
                  className="m-1"
                >
                  PLC
                </Badge>
                <Badge
                  bg={
                    this.state.wetbay_camera_status === true
                      ? "success"
                      : "danger"
                  }
                  className="m-1"
                >
                  Wetbay Camera
                </Badge>
                <Badge
                  bg={
                    this.state.drybay_camera_status === true
                      ? "success"
                      : "danger"
                  }
                  className="m-1"
                >
                  Drybay Camera
                </Badge>
                <Badge
                  bg={
                    this.state.pt1_camera_status === true ? "success" : "danger"
                  }
                  className="m-1"
                >
                  PT1 Camera
                </Badge>
                <Badge
                  bg={
                    this.state.pt2_camera_status === true ? "success" : "danger"
                  }
                  className="m-1"
                >
                  PT2 Camera
                </Badge>
                <Badge
                  bg={this.state.ai_status === true ? "success" : "danger"}
                  className="m-1"
                >
                  AI
                </Badge>
                <Badge
                  bg={this.state.pcl_status === true ? "success" : "danger"}
                  className="m-1"
                >
                  PCL
                </Badge>
                <Badge
                  bg={this.state.ur10_status === true ? "success" : "danger"}
                  className="m-1"
                >
                  UR10
                </Badge>
                <Badge
                  bg={this.state.ur5_status === true ? "success" : "danger"}
                  className="m-1"
                >
                  UR5
                </Badge>
                <Badge
                  bg={this.state.nrib_status === true ? "success" : "danger"}
                  className="m-1"
                >
                  NRIB
                </Badge>
              </Card.Body>
            </Card>
          </Col>
        </Row>
        <Tab.Container defaultActiveKey="bootup">
          <Row>
            <Col sm={3}>
              <Nav variant="pills" className="flex-column">
                <Nav.Item>
                  <Nav.Link eventKey="bootup">System Boot Up</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="ur10_control">UR10 Control</Nav.Link>
                </Nav.Item>
                <Nav.Item>
                  <Nav.Link eventKey="ur5_control" disabled="true">
                    UR5 Control
                  </Nav.Link>
                </Nav.Item>
              </Nav>
            </Col>
            <Col sm={9}>
              <Tab.Content>
                <Tab.Pane eventKey="bootup">
                  <Card>
                    <Card.Body>
                      <Card.Title>PLC Software</Card.Title>
                      <Card.Text>
                        <p>
                          Boot up PLC for Wetbay, Drybay and other PLC related
                          modules control
                          <br />
                          <span style={{ color: "red" }}>
                            UR10 and UR5 can't be turned on without this
                          </span>
                        </p>
                      </Card.Text>
                      <Button
                        variant={
                          this.state.plcBootBtn !== false ? "danger" : "success"
                        }
                        onClick={this.module_action}
                        id="plc"
                        value={
                          this.state.plcBootBtn !== false ? "kill" : "boot"
                        }
                        disabled={
                          this.state.plcBootBtnLoading !== false ? true : false
                        }
                      >
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                          hidden={
                            this.state.plcBootBtnLoading !== false
                              ? false
                              : true
                          }
                        />
                        <span className="visually-hidden">Loading...</span>
                        {this.state.plcBootBtn !== false
                          ? "  Turn Off"
                          : "  Turn On"}
                      </Button>
                    </Card.Body>
                  </Card>
                  <br />
                  <Card>
                    <Card.Body>
                      <Card.Title>Camera Software</Card.Title>
                      <Card.Text>
                        Boot up cameras, ai, pointcloud library software
                      </Card.Text>
                      <Button
                        variant={
                          this.state.cameraBootBtn !== false
                            ? "danger"
                            : "success"
                        }
                        onClick={this.module_action}
                        id="camera"
                        value={
                          this.state.cameraBootBtn !== false ? "kill" : "boot"
                        }
                        disabled={
                          this.state.cameraBootBtnLoading !== false
                            ? true
                            : false
                        }
                      >
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                          hidden={
                            this.state.cameraBootBtnLoading !== false
                              ? false
                              : true
                          }
                        />
                        <span className="visually-hidden">Loading...</span>
                        {this.state.cameraBootBtn !== false
                          ? "  Turn Off"
                          : "  Turn On"}
                      </Button>
                    </Card.Body>
                  </Card>
                  <br />
                  <Card>
                    <Card.Body>
                      <Card.Title>UR10 Software</Card.Title>
                      <Card.Text>
                        Boot up UR10 driver, moveit motion planner, motion
                        server etc
                      </Card.Text>
                      <Button
                        variant={
                          this.state.ur10BootBtn !== false ||
                          this.state.plc_status === false
                            ? "danger"
                            : "success"
                        }
                        onClick={this.module_action}
                        id="ur10"
                        value={
                          this.state.ur10BootBtn !== false ? "kill" : "boot"
                        }
                        disabled={
                          this.state.ur10BootBtnLoading !== false ||
                          this.state.plc_status === false
                            ? true
                            : false
                        }
                      >
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                          hidden={
                            this.state.ur10BootBtnLoading !== false
                              ? false
                              : true
                          }
                        />
                        <span className="visually-hidden">Loading...</span>
                        {this.state.plc_status === false
                          ? "Please turn on PLC first"
                          : this.state.ur10BootBtn !== false
                          ? "  Turn Off"
                          : "  Turn On"}
                      </Button>
                    </Card.Body>
                  </Card>
                  <br />
                  <Card>
                    <Card.Body>
                      <Card.Title>UR5 Software</Card.Title>
                      <Card.Text>
                        Boot up UR5 driver, moveit motion planner, motion server
                        etc
                      </Card.Text>
                      <Button
                        variant={
                          this.state.ur5BootBtn !== false ||
                          this.state.plc_status === false
                            ? "danger"
                            : "success"
                        }
                        onClick={this.module_action}
                        id="ur5"
                        value={
                          this.state.ur5BootBtn !== false ? "kill" : "boot"
                        }
                        disabled={
                          this.state.ur5BootBtnLoading !== false ||
                          this.state.plc_status === false
                            ? true
                            : false
                        }
                      >
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                          hidden={
                            this.state.ur5BootBtnLoading !== false
                              ? false
                              : true
                          }
                        />
                        <span className="visually-hidden">Loading...</span>
                        {this.state.plc_status === false
                          ? "Please turn on PLC first"
                          : this.state.ur5BootBtn !== false
                          ? "  Turn Off"
                          : "  Turn On"}
                      </Button>
                    </Card.Body>
                  </Card>
                  <br />
                  <Card>
                    <Card.Body>
                      <Card.Title>Non-Ring Inspection Bay Software</Card.Title>
                      <Card.Text>
                        Boot up Non-Ring Inspection Bay Software, including ai,
                        belt control etc
                      </Card.Text>
                      <Button
                        variant={
                          this.state.nribBootBtn !== false
                            ? "danger"
                            : "success"
                        }
                        onClick={this.module_action}
                        id="ib1"
                        value={
                          this.state.nribBootBtn !== false ? "kill" : "boot"
                        }
                        disabled={
                          this.state.nribBootBtnLoading !== false ? true : false
                        }
                      >
                        <Spinner
                          as="span"
                          animation="border"
                          size="sm"
                          role="status"
                          aria-hidden="true"
                          hidden={
                            this.state.nribBootBtnLoading !== false
                              ? false
                              : true
                          }
                        />
                        <span className="visually-hidden">Loading...</span>
                        {this.state.nribBootBtn !== false
                          ? "  Turn Off"
                          : "  Turn On"}
                      </Button>
                    </Card.Body>
                  </Card>
                </Tab.Pane>
                <Tab.Pane eventKey="ur10_control">
                  <Row>
                    <h2>
                      Robot Status
                      <Badge
                        className="float-end"
                        pill
                        bg={
                          this.state.ur10_robot_status === "RUNNING"
                            ? "success"
                            : "warning"
                        }
                      >
                        {" "}
                        {this.state.ur10_robot_status}
                      </Badge>
                    </h2>
                    <h2>
                      Program Status
                      <Badge
                        className="float-end"
                        pill
                        bg={
                          this.state.ur10_program_status === "PLAYING"
                            ? "success"
                            : "warning"
                        }
                      >
                        {this.state.ur10_program_status}
                      </Badge>
                    </h2>
                  </Row>

                  <Row className="align-items-center">
                    <h2>
                      Position Control
                      <Badge className="float-end" pill bg="info">
                        {this.state.joystick_direction}
                      </Badge>
                    </h2>
                    <Row className="mt-3">
                      <Col md={3}>
                        <ButtonGroup vertical className="float-end">
                          <Button
                            variant="outline-primary"
                            style={{ height: "140px" }}
                            onTouchStart={this.handleButtonHold}
                            onTouchEnd={this.handleButtonRelease}
                            onMouseDown={this.handleButtonHold}
                            onMouseUp={this.handleButtonRelease}
                            value="UP"
                          >
                            Up
                          </Button>
                          <Button
                            style={{ height: "140px" }}
                            onTouchStart={this.handleButtonHold}
                            onTouchEnd={this.handleButtonRelease}
                            onMouseDown={this.handleButtonHold}
                            onMouseUp={this.handleButtonRelease}
                            value="DOWN"
                          >
                            Down
                          </Button>
                        </ButtonGroup>
                      </Col>
                      <Col md={9}>
                        <Joystick
                          size={300}
                          baseColor="grey"
                          stickColor="black"
                          move={this.handleMove}
                          stop={this.handleStop}
                        ></Joystick>
                      </Col>
                    </Row>
                  </Row>
                </Tab.Pane>
                <Tab.Pane eventKey="ur5_control">
                  <p>ur5 control</p>
                </Tab.Pane>
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    );
  }
}

export default SystemControl;
