import ReactTimeAgo from "react-time-ago";
import TimeAgo from "javascript-time-ago";

import en from "javascript-time-ago/locale/en.json";
import React, { useEffect, useState } from "react";
import { Table, Col, Button, Modal, Image, Spinner } from "react-bootstrap";
import axios from "axios";

const ip = "192.168.10.100";

TimeAgo.addDefaultLocale(en);

class Consumables extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      main_program_status: false,
      consumables: [],
      modal_state: false,
      modal_refill: "",
      refill_state: "",
      consumable_hashmap: {},
      is_loading: true,
    };
    this.get_consumables();
    this.get_main_program_status();
  }

  get_consumables() {
    axios.get("http://" + ip + ":5000/api/consumables/").then((res) => {
      if (res.data) {
        this.setState(
          {
            consumables: res.data.consumables,
            refill_state: res.data.consumables_ui_state.refill_state,
            modal_refill: res.data.consumables_ui_state.which_consumables,
            consumable_hashmap: res.data.consumable_hashmap,
          },
          () => {
            console.log(this.state);
            if (this.state.refill_state !== "prompting") {
              this.setState({ modal_state: true });
            }
          }
        );
      }
    });
  }

  get_main_program_status() {
    axios
      .get("http://" + ip + ":5000/api/system_control/main_program_status")
      .then((res) => {
        if (res.data) {
          this.setState({
            main_program_status: res.data.main_program_status,
            is_loading: false,
          });
        }
      });
  }

  refill = (e) => {
    this.setState({ modal_refill: e.target.value });
    this.modalHandler();
  };

  modalHandler = () => {
    if (
      this.state.refill_state === "refilling" ||
      this.state.refill_state === "door_error"
    ) {
      console.log("you cant dismiss me");
    } else {
      console.log("setting modal state");
      this.setState({ modal_state: !this.state.modal_state }, () => {
        console.log(this.state.modal_state);
      });
    }
  };

  unlockModuleDoor = () => {
    axios
      .get(
        "http://" +
          ip +
          ":5000/api/consumables/unlock_door/" +
          this.state.modal_refill
      )
      .then((res) => {
        if (res.data.success) {
          this.setState({ refill_state: "refilling" });
        } else {
          this.setState({ refill_state: "error" });
        }
      });
  };

  completeRefill = () => {
    axios
      .get(
        "http://" +
          ip +
          ":5000/api/consumables/check_door/" +
          this.state.modal_refill
      )
      .then((res) => {
        if (res.data.success) {
          axios
            .get(
              "http://" +
                ip +
                ":5000/api/consumables/refill/" +
                this.state.modal_refill
            )
            .then((res) => {
              console.log(res.data);

              this.setState({ refill_state: "prompting" }, () => {
                this.get_consumables();
                this.modalHandler();
              });
            });
        } else {
          //module door not close properly
          console.log("door error");
          this.setState({ refill_state: "door_error" });
        }
      });
  };

  render() {
    if (this.state.is_loading === true) {
      return (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      );
    }
    return (
      <React.Fragment>
        <Col md={12}>
          <h1>Consumables Refill</h1>
          <br />
        </Col>
        <Col
          md={12}
          hidden={
            !this.state.main_program_status || this.state.is_loading === true
          }
        >
          <h2 style={{ color: "red" }}>Not Allowed while program is running</h2>
        </Col>
        <Col
          md={12}
          hidden={
            this.state.main_program_status || this.state.is_loading === true
          }
        >
          <Table bordered hover style={{ verticalAlign: "middle" }}>
            <thead>
              <tr>
                <th>Consumables</th>
                <th>Which Side</th>
                <th>Last Loaded</th>
                <th>Remaining Level</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              {this.state.consumables.map((consumables) => (
                <tr
                  className={
                    consumables.remaining <= 10
                      ? "table-danger"
                      : consumables.remaining >= 11 &&
                        consumables.remaining <= 30
                      ? "table-warning"
                      : "table-success"
                  }
                  key={consumables.id}
                >
                  <td>{consumables.name}</td>
                  <td>{consumables.location}</td>
                  <td>
                    <ReactTimeAgo
                      date={consumables.time_last_refill}
                      locale="en-US"
                    />
                  </td>
                  <td>{consumables.remaining + "%"}</td>
                  <td>
                    <Button onClick={this.refill} value={consumables.codename}>
                      Refill
                    </Button>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </Col>
        <Col md={12}>
          <Button
            style={{ width: "100%", height: "100px", fontSize: "30px" }}
            variant="success"
            onClick={(e) => {
              this.props.history.push("/");
            }}
          >
            Go to Main Operation Page
          </Button>
        </Col>
        <Modal
          size="xl"
          show={this.state.modal_state}
          onHide={this.modalHandler}
        >
          <Modal.Header closeButton>
            <Modal.Title>Refill Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>You are about to refill: </h5>
            <h3>{this.state.consumable_hashmap[this.state.modal_refill]}</h3>
            <h5
              hidden={this.state.refill_state !== "prompting" ? true : false}
              style={{ color: "red" }}
            >
              This action cannot be undone, continue?
            </h5>
            <Button
              hidden={this.state.refill_state !== "prompting" ? true : false}
              variant="primary"
              onClick={this.unlockModuleDoor}
            >
              Unlock Consumables Door and Refill
            </Button>
            <div
              hidden={
                this.state.refill_state === "refilling" ||
                this.state.refill_state === "door_error"
                  ? false
                  : true
              }
            >
              <h5>Please follow the following guide to refill: </h5>
              <Image
                className="d-block mx-auto"
                src="https://via.placeholder.com/1920x1080"
                fluid
              />
              <br />
            </div>
            <h5
              hidden={this.state.refill_state === "door_error" ? false : true}
              style={{ color: "red" }}
            >
              Module's door not closed properly, please check and press the
              button again
            </h5>
          </Modal.Body>
          <Modal.Footer>
            <Button
              hidden={this.state.refill_state !== "prompting" ? true : false}
              variant="secondary"
              onClick={this.modalHandler}
            >
              Cancel
            </Button>
            <Button
              hidden={
                this.state.refill_state === "refilling" ||
                this.state.refill_state === "door_error"
                  ? false
                  : true
              }
              variant="primary"
              onClick={this.completeRefill}
            >
              Complete Refill
            </Button>
          </Modal.Footer>
        </Modal>
      </React.Fragment>
    );
  }
}

export default Consumables;
