import React, { Component } from "react";
import { Modal, Spinner } from "react-bootstrap";
class BootupShutdownModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bootup_shutdown_modal_state: this.props.bootup_shutdown_modal_state,
      bootup_shutdown_modal_text: this.props.bootup_shutdown_modal_text,
    };
  }
  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.setState({
        bootup_shutdown_modal_state: this.props.bootup_shutdown_modal_state,
        bootup_shutdown_modal_text: this.props.bootup_shutdown_modal_text,
      });
    }
  }
  render() {
    return (
      <React.Fragment>
        <Modal size="xl" show={this.state.bootup_shutdown_modal_state} centered>
          <Modal.Body>
            <h1>
              <Spinner
                as="span"
                animation="border"
                size="lg"
                role="status"
                aria-hidden="true"
              />
              <span className="visually-hidden">Loading... </span>
              {`  ${this.state.bootup_shutdown_modal_text}`}
            </h1>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

export default BootupShutdownModal;
