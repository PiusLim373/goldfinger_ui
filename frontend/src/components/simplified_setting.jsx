import React, { Component } from "react";
import axios from "axios";
import { Col, Row, Button, Spinner, Modal } from "react-bootstrap";
import { ArrowClockwise } from "react-bootstrap-icons";
import CustomAlertBox from "./simplified_ui_alert_box";
import WetDryBayController from "./wetdrybay_control";
const ip = "192.168.10.100";

class SimplifiedSetting extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      alert_error: false,
      alert_text_1: "",
      alert_text_2: [],
      alert_text_3: "",
      alert_button_text: "",
      alert_button_value: "",
      ///
      button_loading: true,
      ///
      shutdown_modal_state: false,
      ///
      main_program_status: false,
      ///
      kill_program_manager_modal_state: false,
    };
    this.check_state();
  }

  check_state() {
    axios
      .get("http://" + ip + ":5000/api/system_control/bootup_shutdown_checker")
      .then((res) => {
        if (!res.data.bootup_in_progress) {
          this.setState({
            button_loading: false,
          });
        } else {
          this.interval = setInterval(() => {
            this.check_bootup_has_finish();
          }, 1000);
          this.setState({
            alert_error: true,
            alert_text_1:
              "Page refreshed unexpectedly, press the green button below for troubleshooting.",
          });
        }
      });
    axios
      .get("http://" + ip + ":5000/api/system_control/main_program_status")
      .then((res) => {
        if (res.data) {
          this.setState({
            main_program_status: res.data.main_program_status,
          });
        }
      });
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }

  componentWillMount() {
    // console.log(this.props);
  }

  check_bootup_has_finish = () => {
    axios
      .get("http://" + ip + ":5000/api/system_control/bootup_shutdown_checker")
      .then((res) => {
        if (!res.data.bootup_in_progress) {
          this.setState(
            {
              button_loading: false,
            },
            () => clearInterval(this.interval)
          );
        }
      });
  };

  bootupOrFix = () => {
    this.setState({ button_loading: true });
    this.props.toggle_bootup_shutdown_modal_state(
      "System booting up, this will take up to 1 minute..."
    );
    axios
      .get("http://" + ip + ":5000/api/system_control/bootup_or_fix/")
      .then((res) => {
        console.log(res.data);
        if (!res.data.success) {
          if (res.data.message === "not_powered") {
            this.setState(
              {
                alert_error: true,
                alert_text_1:
                  "The following module(s) isn't / aren't powered, please check: ",
                alert_text_2: res.data.problematic_module,
                alert_text_3:
                  "Please check if the above module(s) is / are powered, and press the green button again",
              },
              () => {
                this.setState({ button_loading: false });
                this.props.toggle_bootup_shutdown_modal_state("");
              }
            );
          }
        } else {
          this.setState(
            {
              alert_error: false,
              alert_text_1:
                "Software successfully launched, proceed to main page for processing",
              alert_text_2: [],
              alert_text_3: "",
              alert_button_text: "Go to Main Operation page",
              alert_button_value: "go_to_main_operation_page",
            },
            () => {
              this.setState({ button_loading: false });
              this.props.toggle_bootup_shutdown_modal_state("");
            }
          );
        }
      });
  };

  toggle_shutdown_modal_state = () => {
    this.setState({
      shutdown_modal_state: !this.state.shutdown_modal_state,
    });
  };

  shutdown_system = () => {
    this.toggle_shutdown_modal_state();
    this.props.toggle_bootup_shutdown_modal_state("System shutting down...");
    axios
      .get("http://" + ip + ":5000/api/system_control/shutdown")
      .then((res) => {
        console.log("pinged for shutdown");
      });
  };

  toggle_kill_program_manager_modal_state = () => {
    this.setState({
      kill_program_manager_modal_state:
        !this.state.kill_program_manager_modal_state,
    });
  };

  kill_program_manager = () => {
    this.toggle_kill_program_manager_modal_state();
    axios
      .get("http://" + ip + ":5000/api/system_control/kill/program_manager")
      .then((res) => {
        console.log("pinged for program_manager termination");
        this.check_state();
      });
  };

  render() {
    return (
      <div>
        <h1>System Control</h1>
        <br />
        <Row>
          <CustomAlertBox
            alert_error={this.state.alert_error}
            alert_text_1={this.state.alert_text_1}
            alert_text_2={this.state.alert_text_2}
            alert_text_3={this.state.alert_text_3}
            alert_button_text={this.state.alert_button_text}
            alert_button_value={this.state.alert_button_value}
          />
        </Row>
        <br />
        <Row>
          <Col md={6}>
            <Button
              onClick={this.bootupOrFix}
              style={{ width: "100%", height: "300px", fontSize: "40px" }}
              variant="success"
              disabled={this.state.button_loading ? true : false}
            >
              <Spinner
                as="span"
                animation="border"
                size="lg"
                role="status"
                aria-hidden="true"
                hidden={this.state.button_loading ? false : true}
              />
              <span className="visually-hidden">Loading... </span>
              {this.state.button_loading
                ? "  Working in progress"
                : "  Boot up System / Quick Fix"}
            </Button>
          </Col>
          <Col md={6} className="">
            <Button
              onClick={
                this.state.main_program_status
                  ? this.toggle_kill_program_manager_modal_state
                  : this.toggle_shutdown_modal_state
              }
              style={{ width: "100%", height: "300px", fontSize: "40px" }}
              variant="danger"
              disabled={this.state.button_loading ? true : false}
            >
              <Spinner
                as="span"
                animation="border"
                size="lg"
                role="status"
                aria-hidden="true"
                hidden={this.state.button_loading ? false : true}
              />
              <span className="visually-hidden">Loading... </span>
              {this.state.button_loading
                ? "  Working in progress"
                : this.state.main_program_status
                ? "  Terminate Program"
                : "  Shutdown system"}
            </Button>
          </Col>
        </Row>
        <br />
        <br />
        <WetDryBayController />
        <Modal
          size="xl"
          show={this.state.shutdown_modal_state}
          onHide={this.toggle_shutdown_modal_state}
        >
          <Modal.Header closeButton>
            <Modal.Title>Shutdown Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: "red" }}>
              You are about to shutdown the whole BigBox system. This action
              cannot be undone, continue?
            </h5>
            <h5 style={{ color: "red" }}>
              Flip the main switch off and on again will reboot the system for
              the next run.
            </h5>
            <h5>See ya!</h5>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="success"
              value="input"
              onClick={this.toggle_shutdown_modal_state}
            >
              Cancel
            </Button>
            <Button
              variant="danger"
              // onClick={this.props.show_shutting_down_modal}
              onClick={this.shutdown_system}
            >
              Confirm Shutdown
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal
          size="xl"
          show={this.state.kill_program_manager_modal_state}
          onHide={this.toggle_kill_program_manager_modal_state}
        >
          <Modal.Header closeButton>
            <Modal.Title>Terminate Program Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5 style={{ color: "red" }}>
              You are about to terminate the current program BigBox system is
              running. This action cannot be undone, continue?
            </h5>
            <h5 style={{ color: "red" }}>
              This will stop all processes. Items position will need to be
              manually resetted. Only terminate if the system is not responding.
            </h5>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="success"
              value="input"
              onClick={this.toggle_kill_program_manager_modal_state}
            >
              Cancel
            </Button>
            <Button variant="danger" onClick={this.kill_program_manager}>
              Confirm Program Termination
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default SimplifiedSetting;
