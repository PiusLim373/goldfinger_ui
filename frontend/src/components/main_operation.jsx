import React, { Component } from "react";
import {
  Alert,
  Table,
  Col,
  Row,
  Button,
  Badge,
  Collapse,
  Card,
  Modal,
  Spinner,
} from "react-bootstrap";
import axios from "axios";
import { Link } from "react-router-dom";
import ReactTimeAgo from "react-time-ago";
import TimeAgo from "javascript-time-ago";

import en from "javascript-time-ago/locale/en.json";

const ip = "192.168.10.100";

TimeAgo.addDefaultLocale(en);
class MainOperation extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      is_loading: true,
      ///
      alert_text: "",
      alert_danger: false,
      ///
      requisite_list: ["consumables", "spare", "input", "control"],
      ///
      currently_checking: "consumables",
      checked_list: [],
      ///
      consumables: [],
      consumables_enough: false,
      ///
      non_ring_spares: [],
      ring_spares: [],
      spares_enough: false,
      ///
      wetbay_input: [0, 0, 0, 0, 0, 0],
      drybay_input: [0, 0, 0, 0, 0, 0],
      pigeonhole_to_process: [],
      ///
      ur10_robot_status: "Requesting...",
      ur10_program_status: "Requesting...",
      pause_play_btn_status: "Pause",
      main_program_status: false,
      ///
      modal_state: false,
      ///
      main_program_ended: false,
      ///
      error_rectification_modal: false,
      error_message: "",
    };
    this.update_state();
    this.get_consumables();
    this.get_spares();
  }

  componentWillUnmount() {
    clearInterval(this.interval); //remove the pooling
  }

  update_state() {
    axios
      .get("http://" + ip + ":5000/api/system_control/")
      .then((res) => {
        if (res.data) {
          this.setState(
            {
              ur10_robot_status: res.data.ur10_robot_status,
              ur10_program_status: res.data.ur10_program_status,
              main_program_status: res.data.main_program_status,
              is_loading: false,
            },
            () => {
              //patch for empty robot status and program status
              if (this.state.ur10_program_status === "")
                this.setState({
                  ur10_robot_status: "POWERED OFF",
                  ur10_program_status: "STOPPED",
                });
              if (this.state.main_program_status === true) {
                this.setState({ currently_checking: "control" });
                this.interval = setInterval(() => {
                  this.update_alert_text();
                }, 1000);
              }
              if (
                this.state.ur10_robot_status === "RUNNING" &&
                this.state.ur10_program_status === "PLAYING" &&
                this.state.main_program_status === false
              )
                this.setState({ pause_play_btn_status: "Start Program" });
              else if (
                this.state.ur10_robot_status === "RUNNING" &&
                this.state.ur10_program_status === "PLAYING" &&
                this.state.main_program_status === true
              )
                this.setState({ pause_play_btn_status: "Pause Program" });
              else if (
                this.state.ur10_robot_status === "RUNNING" &&
                this.state.ur10_program_status === "PAUSE"
              )
                this.setState({ pause_play_btn_status: "Continue Program" });
              else
                this.setState({
                  pause_play_btn_status: "Robot not ready, not allowed",
                });
            }
          );
        }
      })
      .catch((err) => {
        console.log("update robot state return with code: " + err);
      });
  }

  update_alert_text = () => {
    axios
      .get("http://" + ip + ":5000/api/system_control/main_program_message")
      .then((res) => {
        if (res.data) {
          if (res.data.main_program_status === false)
            this.setState({
              main_program_ended: true,
            });
          this.setState({
            alert_text: res.data.main_program_message,
            alert_danger: res.data.main_program_error_message,
          });
        }
      });
    axios.get("http://" + ip + ":5000/api/error_rectification/").then((res) => {
      // if (res.data.error === true) {
      if (res.data) {
        this.setState({
          error_rectification_modal: res.data.error,
          error_message: res.data.error_message,
        });
      }
    });
  };

  get_consumables() {
    axios.get("http://" + ip + ":5000/api/consumables/").then((res) => {
      if (res.data) {
        this.setState({ consumables: res.data.consumables }, () => {
          var is_enough = true;
          for (const con of this.state.consumables) {
            if (con.remaining <= 10) {
              is_enough = false;
              this.setState({ consumables_enough: false });
              break;
            }
          }
          if (is_enough) this.setState({ consumables_enough: true });
        });
      }
    });
  }

  get_spares() {
    axios.get("http://" + ip + ":5000/api/spares/").then((res) => {
      if (res.data) {
        this.setState(
          {
            non_ring_spares: res.data.non_ring_spares,
            ring_spares: res.data.ring_spares,
          },
          () => {
            var is_enough = true;
            for (const nri of this.state.non_ring_spares) {
              if (nri.remaining / nri.capacity <= 0.2) {
                is_enough = false;
                this.setState({ spares_enough: false });
                break;
              }
            }
            if (is_enough) {
              for (const ri of this.state.ring_spares) {
                if (ri.remaining / ri.capacity <= 0.2) {
                  is_enough = false;
                  this.setState({ spares_enough: false });
                  break;
                }
              }
            }
            if (is_enough) this.setState({ spares_enough: true });
          }
        );
      }
    });
  }

  check_requisite = (e) => {
    const element = this.state.requisite_list[parseInt(e.target.value) + 1];
    if (element === "input") this.get_input_condition();
    const checked_element = this.state.checked_list.concat(
      this.state.requisite_list[parseInt(e.target.value)]
    );
    if (
      !this.state.checked_list.includes(
        this.state.requisite_list[parseInt(e.target.value)]
      )
    )
      this.setState({ checked_list: checked_element });
    this.setState({ currently_checking: element });
  };

  change_card = (e) => {
    const check = e.target.value;
    this.setState({ currently_checking: check });
  };

  get_input_condition = () => {
    axios.get("http://" + ip + ":5000/api/input_condition/").then((res) => {
      this.setState(
        {
          alert_danger: !res.data.success,
          alert_text: res.data.message,
          wetbay_input: res.data.wetbay_input,
          drybay_input: res.data.drybay_input,
        },
        () => {
          var pigeonhole_to_process = [];
          for (var i = 0; i < 6; i++) {
            if (
              this.state.wetbay_input[i] === 1 &&
              this.state.drybay_input[i] === 11
            )
              pigeonhole_to_process.push(i);
          }
          this.setState({ pigeonhole_to_process: pigeonhole_to_process });
        }
      );
    });
  };

  exclude_process = (e) => {
    this.setState({
      pigeonhole_to_process: this.state.pigeonhole_to_process.filter(
        (element) => element !== parseInt(e.target.value)
      ),
    });
  };

  play_pause = (e) => {
    var action = e.target.value;
    if (action === "start_program") this.modal_handler();
    else {
      axios
        .get("http://" + ip + ":5000/api/system_control/" + action)
        .then((res) => {
          this.setState({
            alert_danger: !res.data.success,
            alert_text: res.data.message,
          });
          this.update_state();
        });
    }
  };

  launch_program_manager = () => {
    this.setState({
      pause_play_btn_status: "Starting program, please wait...",
    });
    var action = "start_program";
    this.modal_handler();
    //start message pooling timer also
    axios
      .post("http://" + ip + ":5000/api/system_control/program_manager", {
        pigeonhole_to_process: this.state.pigeonhole_to_process,
      })
      .then((res) => {
        this.setState({
          alert_danger: !res.data.success,
          alert_text: res.data.message,
          main_program_status: true,
        });
        this.update_state();
      });
  };

  modal_handler = () => {
    this.setState({
      modal_state: !this.state.modal_state,
    });
  };

  main_program_modal_handler = () => {
    this.setState({
      main_program_ended: false,
    });
  };

  review_loading = (e) => {
    this.modal_handler();
    this.change_card(e);
  };

  error_resolved = () => {
    axios.get("http://" + ip + ":5000/api/error_rectification/error_solved");
  };

  reset = () => {
    clearInterval(this.interval);
    this.setState(
      {
        alert_text: "",
        alert_danger: false,
        ///
        currently_checking: "consumables",
        checked_list: [],
        ///
        consumables: [],
        consumables_enough: false,
        ///
        non_ring_spares: [],
        ring_spares: [],
        spares_enough: false,
        ///
        wetbay_input: [0, 0, 0, 0, 0, 0],
        drybay_input: [0, 0, 0, 0, 0, 0],
        pigeonhole_to_process: [],
        ///
        ur10_robot_status: "Requesting...",
        ur10_program_status: "Requesting...",
        pause_play_btn_status: "Pause",
        main_program_status: false,
        ///
        modal_state: false,
        ///
        main_program_ended: false,
      },
      () => {
        this.update_state();
        this.get_consumables();
        this.get_spares();
        console.log(this.state);
      }
    );
  };
  render() {
    if (this.state.is_loading === true) {
      return (
        <div style={{ display: "flex", justifyContent: "center" }}>
          <Spinner animation="border" role="status">
            <span className="visually-hidden">Loading...</span>
          </Spinner>
        </div>
      );
    }
    return (
      <React.Fragment>
        <Col md={12}>
          <h1>Main Operation</h1>
        </Col>
        <Alert
          className="mb-3"
          hidden={this.state.alert_text === "" ? true : false}
          variant={this.state.alert_danger === true ? "danger" : "success"}
        >
          {this.state.alert_text}
        </Alert>
        <Card hidden={this.state.main_program_status}>
          <Card.Header>
            <h2>
              Consumables
              <Button
                hidden={
                  this.state.currently_checking === "consumables" ||
                  !this.state.checked_list.includes("consumables")
                    ? true
                    : false
                }
                variant="success"
                className="float-end"
                onClick={this.change_card}
                value="consumables"
              >
                Review{" "}
              </Button>
            </h2>
          </Card.Header>
          <Collapse
            in={this.state.currently_checking === "consumables" ? true : false}
          >
            <Card.Body>
              <Row>
                <Col md={12}>
                  <Table bordered hover>
                    <thead>
                      <tr>
                        <th>Comsumables</th>
                        <th>Which Sides</th>
                        <th>Last loaded</th>
                        <th>Remaining Level</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.consumables.map((con) => (
                        <React.Fragment>
                          <tr
                            className={
                              con.remaining <= 10
                                ? "table-danger"
                                : con.remaining >= 11 && con.remaining <= 30
                                ? "table-warning"
                                : "table-success"
                            }
                            key={con.id}
                          >
                            <td>{con.name}</td>
                            <td>{con.location}</td>
                            <td>
                              <ReactTimeAgo
                                date={con.time_last_refill}
                                locale="en-US"
                              />
                            </td>

                            <td>{con.remaining + "%"}</td>
                          </tr>
                        </React.Fragment>
                      ))}
                    </tbody>
                  </Table>
                </Col>
                <Col md={6}>
                  <Link to="/consumables">
                    <Button
                      style={{ width: "100%" }}
                      size="lg"
                      variant="warning"
                    >
                      Refill Consumables
                    </Button>
                  </Link>
                </Col>
                <Col md={6}>
                  <Button
                    style={{ width: "100%" }}
                    size="lg"
                    variant={
                      this.state.consumables_enough === true
                        ? "success"
                        : "danger"
                    }
                    onClick={this.check_requisite}
                    value="0"
                    disabled={
                      this.state.consumables_enough === true ? false : true
                    }
                  >
                    {this.state.consumables_enough === true
                      ? "Checked and Proceed"
                      : "Insufficient Consumables, Please Refill"}
                  </Button>
                </Col>
              </Row>
            </Card.Body>
          </Collapse>
        </Card>
        <br />
        <Card hidden={this.state.main_program_status}>
          <Card.Header>
            <h2>
              Spare Instruments
              <Button
                hidden={
                  this.state.currently_checking === "spare" ||
                  !this.state.checked_list.includes("spare")
                    ? true
                    : false
                }
                variant="success"
                className="float-end"
                onClick={this.change_card}
                value="spare"
              >
                Review
              </Button>
            </h2>
          </Card.Header>
          <Collapse
            in={this.state.currently_checking === "spare" ? true : false}
          >
            <Card.Body>
              <Row>
                <Col md={12}>
                  <Table bordered hover>
                    <thead>
                      <tr>
                        <th>Non-Ring Spare Instruments</th>
                        <th>Last loaded</th>
                        <th>Remaining Instruments</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.non_ring_spares.map((nri) => (
                        <React.Fragment>
                          <tr
                            className={
                              nri.remaining / nri.capacity <= 0.2
                                ? "table-danger"
                                : nri.remaining / nri.capacity >= 0.2 &&
                                  nri.remaining / nri.capacity <= 0.5
                                ? "table-warning"
                                : "table-success"
                            }
                            key={nri.id}
                          >
                            <td>{nri.name}</td>
                            <td>
                              <ReactTimeAgo
                                date={nri.time_last_refill}
                                locale="en-US"
                              />
                            </td>

                            <td>{nri.remaining + " out of " + nri.capacity}</td>
                          </tr>
                        </React.Fragment>
                      ))}
                    </tbody>
                  </Table>
                  <Table bordered hover>
                    <thead>
                      <tr>
                        <th>Ring Spare Instruments</th>
                        <th>Last loaded</th>
                        <th>Remaining Instruments</th>
                      </tr>
                    </thead>
                    <tbody>
                      {this.state.ring_spares.map((ri) => (
                        <React.Fragment>
                          <tr
                            className={
                              ri.remaining / ri.capacity <= 0.2
                                ? "table-danger"
                                : ri.remaining / ri.capacity >= 0.2 &&
                                  ri.remaining / ri.capacity <= 0.5
                                ? "table-warning"
                                : "table-success"
                            }
                            key={ri.id}
                          >
                            <td>{ri.name}</td>
                            <td>
                              <ReactTimeAgo
                                date={ri.time_last_refill}
                                locale="en-US"
                              />
                            </td>

                            <td>{ri.remaining + " out of " + ri.capacity}</td>
                          </tr>
                        </React.Fragment>
                      ))}
                    </tbody>
                  </Table>
                </Col>
                <Col md={6}>
                  <Link to="/spares">
                    <Button
                      style={{ width: "100%" }}
                      size="lg"
                      variant="warning"
                    >
                      Refill Spares
                    </Button>
                  </Link>
                </Col>
                <Col md={6}>
                  <Button
                    style={{ width: "100%" }}
                    size="lg"
                    variant="success"
                    value="1"
                    variant={
                      this.state.spares_enough === true ? "success" : "danger"
                    }
                    onClick={this.check_requisite}
                    disabled={this.state.spares_enough === true ? false : true}
                  >
                    {this.state.spares_enough === true
                      ? "Checked and Proceed"
                      : "Insufficient Spare Instruments, Please Refill"}
                  </Button>
                </Col>
              </Row>
            </Card.Body>
          </Collapse>
        </Card>
        <br />
        <Card hidden={this.state.main_program_status}>
          <Card.Header>
            <h2>
              Input Conditions
              <Button
                hidden={
                  this.state.currently_checking === "input" ||
                  !this.state.checked_list.includes("input")
                    ? true
                    : false
                }
                variant="success"
                className="float-end"
                onClick={this.change_card}
                value="input"
              >
                Review
              </Button>
            </h2>
          </Card.Header>
          <Collapse
            in={this.state.currently_checking === "input" ? true : false}
          >
            <Card.Body>
              <Row>
                <Col md={6}>
                  <h4>Wetbay Pigeonhole Loading Condition:</h4>
                  <Table bordered hover style={{ verticalAlign: "middle" }}>
                    <tbody>
                      <tr>
                        {this.state.wetbay_input
                          .slice(0, 3)
                          .map((pigeonhole, i) => (
                            <th
                              key={i}
                              style={{
                                background:
                                  pigeonhole === 0 ? "#f8d7da" : "#d1e7dd",
                              }}
                            >
                              {pigeonhole === 0
                                ? `#${i + 1}: Not Loaded`
                                : `#${i + 1}: Tray Loaded`}
                            </th>
                          ))}
                      </tr>
                      <tr>
                        {this.state.wetbay_input
                          .slice(3, 6)
                          .map((pigeonhole, i) => (
                            <th
                              key={i}
                              style={{
                                background:
                                  pigeonhole === 0 ? "#f8d7da" : "#d1e7dd",
                              }}
                            >
                              {pigeonhole === 0
                                ? `#${i + 4}: Not Loaded`
                                : `#${i + 4}: Tray Loaded`}
                            </th>
                          ))}
                      </tr>
                    </tbody>
                  </Table>
                </Col>
                <Col md={6}>
                  <h4>Drybay Pigeonhole Loading Condition:</h4>
                  <Table bordered hover style={{ verticalAlign: "middle" }}>
                    <tbody>
                      <tr>
                        {this.state.drybay_input
                          .slice(0, 3)
                          .map((pigeonhole, i) => (
                            <th
                              key={i}
                              style={{
                                background:
                                  pigeonhole !== 11 ? "#f8d7da" : "#d1e7dd",
                              }}
                            >
                              {pigeonhole === 0
                                ? `#${i + 1}: Not Loaded`
                                : pigeonhole === 1
                                ? `#${i + 1}: Cover Not Loaded`
                                : pigeonhole === 10
                                ? `#${i + 1}: Container Not Loaded`
                                : `#${i + 1}: Loaded`}
                            </th>
                          ))}
                      </tr>
                      <tr>
                        {this.state.drybay_input
                          .slice(3, 6)
                          .map((pigeonhole, i) => (
                            <th
                              key={i}
                              style={{
                                background:
                                  pigeonhole !== 11 ? "#f8d7da" : "#d1e7dd",
                              }}
                            >
                              {pigeonhole === 0
                                ? `#${i + 4}: Not Loaded`
                                : pigeonhole === 1
                                ? `#${i + 4}: Cover Not Loaded`
                                : pigeonhole === 10
                                ? `#${i + 4}: Container Not Loaded`
                                : `#${i + 4}: Loaded`}
                            </th>
                          ))}
                      </tr>
                    </tbody>
                  </Table>
                </Col>
                <Col md={12}>
                  <h3>Pigeonhole to be processed:</h3>
                  <Table bordered hover style={{ verticalAlign: "middle" }}>
                    <thead>
                      <tr>
                        <th> Pigeonhole</th>
                        <th> To be processed?</th>
                      </tr>
                    </thead>
                    <thead>
                      {this.state.pigeonhole_to_process.map((pigeonhole) => (
                        <tr key={pigeonhole}>
                          <td>{`Pigeonhole #${pigeonhole + 1}`}</td>
                          <td>
                            <Button
                              variant="danger"
                              value={pigeonhole}
                              onClick={this.exclude_process}
                            >
                              Press to exclude
                            </Button>
                          </td>
                        </tr>
                      ))}
                    </thead>
                  </Table>
                </Col>
              </Row>
              <Row>
                <Col md={6}>
                  <Button
                    style={{ width: "100%" }}
                    size="lg"
                    variant="warning"
                    onClick={this.get_input_condition}
                  >
                    Refresh
                  </Button>
                </Col>
                <Col md={6}>
                  <Button
                    style={{ width: "100%" }}
                    size="lg"
                    variant={
                      this.state.pigeonhole_to_process.length === 0
                        ? "danger"
                        : "success"
                    }
                    disabled={
                      this.state.pigeonhole_to_process.length === 0
                        ? true
                        : false
                    }
                    value="2"
                    onClick={this.check_requisite}
                  >
                    {this.state.pigeonhole_to_process.length === 0
                      ? "There is not pigeonhole to process, please load and refresh"
                      : "Check and Proceed"}
                  </Button>
                </Col>
              </Row>
            </Card.Body>
          </Collapse>
        </Card>
        <br />
        <Card>
          <Card.Header>
            <h2>Control</h2>
          </Card.Header>
          <Collapse
            in={this.state.currently_checking === "control" ? true : false}
          >
            <Card.Body>
              <Row>
                <Col md={6}>
                  <h2>
                    Robot Status
                    <Badge
                      className="float-end"
                      pill
                      bg={
                        this.state.ur10_robot_status === "RUNNING"
                          ? "success"
                          : "warning"
                      }
                    >
                      {" "}
                      {this.state.ur10_robot_status}
                    </Badge>
                  </h2>
                </Col>
                <Col md={6}>
                  <h2>
                    Program Status
                    <Badge
                      className="float-end"
                      pill
                      bg={
                        this.state.ur10_program_status === "PLAYING"
                          ? "success"
                          : "warning"
                      }
                    >
                      {this.state.ur10_program_status}
                    </Badge>
                  </h2>
                </Col>
                <br />
                <Col hidden={!this.state.main_program_status}>
                  <h3>
                    Main program is running, program log will be constanty
                    updates at the top
                  </h3>
                  <h3>You can also pause the program with the button below:</h3>
                </Col>
                <br />
                <Col md={12} className="mt-3">
                  <Button
                    variant={
                      this.state.pause_play_btn_status === "Continue Program" ||
                      this.state.pause_play_btn_status === "Start Program"
                        ? "success"
                        : "warning"
                    }
                    style={{ width: "100%" }}
                    disabled={
                      this.state.pause_play_btn_status !== "Continue Program" &&
                      this.state.pause_play_btn_status !== "Pause Program" &&
                      this.state.pause_play_btn_status !== "Start Program"
                        ? true
                        : false
                    }
                    onClick={this.play_pause}
                    value={this.state.pause_play_btn_status
                      .toLowerCase()
                      .split(" ")
                      .join("_")}
                  >
                    {this.state.pause_play_btn_status}
                  </Button>
                </Col>
              </Row>
            </Card.Body>
          </Collapse>
        </Card>
        <Modal
          size="xl"
          show={this.state.modal_state}
          onHide={this.modal_handler}
        >
          <Modal.Header closeButton>
            <Modal.Title>Start Alert</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>You are about start the BigBox instrument process for:</h5>
            <h3>
              {this.state.pigeonhole_to_process.map((pigeonhole, i) =>
                i !== this.state.pigeonhole_to_process.length - 1
                  ? "Pigeonhole #" + (pigeonhole + 1) + ", "
                  : this.state.pigeonhole_to_process.length === 1
                  ? "Pigeonhole #" + (pigeonhole + 1)
                  : " and Pigeonhole #" + (pigeonhole + 1)
              )}
            </h3>
            {/* <h3>{this.state.consumable_hashmap[this.state.modal_refill]}</h3> */}
            <h5 style={{ color: "red" }}>
              This action cannot be undone, continue?
            </h5>
          </Modal.Body>
          <Modal.Footer>
            <Button
              variant="danger"
              value="input"
              onClick={this.review_loading}
            >
              Cancel and Review
            </Button>
            <Button variant="success" onClick={this.launch_program_manager}>
              Start Process
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={this.state.main_program_ended}>
          <Modal.Header>
            <Modal.Title>Process Completed</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>
              Main program finished successfully. Please proceed to unload from
              DryBay.
            </h5>
            <br />
            <Button
              style={{ width: "100%" }}
              variant="success"
              value="input"
              onClick={this.reset}
            >
              OK
            </Button>
          </Modal.Body>
        </Modal>
        <Modal size="lg" show={this.state.error_rectification_modal}>
          <Modal.Header>
            <Modal.Title style={{ color: "red" }}>
              Requesting Human Intervention
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <h5>
              An error has occured and the system can't solve it internally,
              error details:
            </h5>
            <h5 style={{ color: "red" }}>{this.state.error_message}</h5>
            <h5>Latest message logged by program manager:</h5>
            <h5 style={{ color: "red" }}>
              {this.state.alert_text === "" ? "None" : this.state.alert_text}
            </h5>
            <h5>Press the button below after resolving to continue</h5>
            <br />
            <Button
              style={{ width: "100%" }}
              variant="warning"
              onClick={this.error_resolved}
            >
              Resolved and Continue
            </Button>
          </Modal.Body>
        </Modal>
      </React.Fragment>
    );
  }
}

export default MainOperation;
