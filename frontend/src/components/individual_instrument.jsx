import React, { Component, useState } from "react";
import { Table, Button, Modal } from "react-bootstrap";
import { Link } from "react-router-dom";
class IndividualInstrument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      instrument_data: this.props.instrument_data,
      instrument_type: this.props.instrument_type,
      box_id: this.props.box_id,
      model_state: false,
    };
  }

  modeldataHandler(e) {
    var data = {
      instrument_type: this.state.instrument_type,
      instrument_data: e.target.value,
    };
    this.props.showModel(data);
  }

  //############################################################ Debug
  debug() {
    // console.log(this.state.instrument_data);
  }
  //############################################################ Debug Ends

  render() {
    //############################################################ Debug
    this.debug();

    //############################################################ Debug Ends
    const { instrument_data } = this.state;
    return (
      <React.Fragment>
        <h2>Details</h2>
        <Table bordered hover>
          <thead>
            <tr>
              <th>Instrument</th>
              <th>Property</th>
              <th>Data</th>
            </tr>
          </thead>
          <tbody>
            {instrument_data.map((instrument) => (
              <React.Fragment>
                <tr>
                  <td rowSpan="4">{instrument.name}</td>
                </tr>
                <tr>
                  <td>Item ID</td>
                  <td>{instrument.item_id}</td>
                </tr>
                <tr
                  className={
                    instrument.inspection_result == "pass"
                      ? "table-success"
                      : instrument.inspection_result == "fail" ||
                        instrument.inspection_result == "missing"
                      ? "table-danger"
                      : "table-warning"
                  }
                >
                  <td>Inspection Result</td>
                  <td>
                    {instrument.inspection_result[0].toUpperCase() +
                      instrument.inspection_result.substring(1)}
                  </td>
                </tr>
                <tr>
                  <td>Inspection Proof</td>
                  <td>
                    <Button
                      variant="outline-primary"
                      onClick={(e) => this.modeldataHandler(e, "value")}
                      value={instrument.ai_output}
                    >
                      Inspection Proof
                    </Button>
                  </td>
                </tr>
              </React.Fragment>
            ))}
          </tbody>
        </Table>
      </React.Fragment>
    );
  }
}

export default IndividualInstrument;
