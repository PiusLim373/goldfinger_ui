import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Row, Col, Button } from "react-bootstrap";
class Header extends React.Component {
  redirect(e) {
    console.log(e.target.value);
    this.props.history.push("/" + e.target.value);
  }

  render() {
    const home_btn = {
      width: "100%",
      height: "250px",
      fontSize: "40px",
    };
    return (
      <React.Fragment>
        <Row>
          <Col md={12}>
            <Button
              variant="success"
              style={home_btn}
              onClick={(e) => this.redirect(e, "value")}
              value=""
            >
              Main Operation
            </Button>
          </Col>
        </Row>
        <Row className="mt-4">
          <Col md={4}>
            <Button
              variant="outline-success"
              style={home_btn}
              onClick={(e) => this.redirect(e, "value")}
              value="consumables"
            >
              Consumables Refill
            </Button>
          </Col>
          <br />
          <Col md={4}>
            <Button
              variant="outline-success"
              style={home_btn}
              onClick={(e) => this.redirect(e, "value")}
              value="spares"
            >
              Spares Refill
            </Button>
          </Col>
          <br />
          <Col md={4}>
            <Button
              variant="outline-success"
              style={home_btn}
              onClick={(e) => this.redirect(e, "value")}
              value="instrument"
            >
              Processed Instruments
            </Button>
          </Col>
        </Row>
      </React.Fragment>
    );
  }
}

export default Header;
