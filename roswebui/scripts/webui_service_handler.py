#!/usr/bin/env python
from distutils.util import execute
from sre_constants import SUCCESS
import rospy
import actionlib
from roswebui.msg import *

import subprocess
from time import sleep
import os
import signal
import pexpect
import select
import time
from ur10_pap.msg import *
from plc_server.srv import *

DEBUG = False
PTPD_LAUNCHED = False
CAMERA_LAUCHED = False
CAMERA_KILLED = False
REQUIRED_MODULE_DICT ={
                'plc':{'ip':"192.168.10.103", 'node':"/plc_server", "ping_alive": False, "node_alive" : False},\
                'ur10':{'ip':"192.168.10.101", 'node':"/motion_server", "ping_alive": False, "node_alive" : False},\
                #'ur5':{'ip':"192.168.10.196", "ping_alive": False},\
                'wetbay_camera':{'ip':"192.168.11.200", 'node':"/wetbay/rc_visard_driver", "ping_alive": False, "node_alive" : False},\
                'drybay_camera':{'ip':"192.168.12.200", 'node':"/drybay/rc_visard_driver", "ping_alive": False, "node_alive" : False},\
                'pt1_camera':{'ip':"192.168.13.200", 'node':"/pt1/rc_visard_driver", "ping_alive": False, "node_alive" : False},\
                # 'pt2_camera':{'ip':"192.168.10.132", "ping_alive": False},\
                'ai':{'node':"/ai_server" , "node_alive" : False},\
                'pcl':{'node':"/pcl_server", "node_alive" : False},\
                # 'ib1':{'ip':"192.168.10.100", 'node':"/ib1_server", "ping_alive": False, "node_alive" : False},\
                'ib2':{'ip':"192.168.10.100", "ping_alive": False},\
                # 'drybay_cover':{'usb':"drybay_cover" , "ping_alive": False},\
                # 'printer':{'usb':"/dev/printer" , "ping_alive": False},\
                # 'indicator_dispenser':{'usb':"/dev/indicator_dispenser" , "ping_alive": False},\
                }

# REQUIRED_MODULE_DICT ={
#                 'plc':{'ip':"192.168.10.100", 'node':"/rosout", "ping_alive": False, "node_alive" : False},\
#                 'ur10':{'ip':"192.168.10.100", 'node':"/rosout", "ping_alive": False, "node_alive" : False},\
#                 'ur5':{'ip':"192.168.10.100", "ping_alive": False},\
#                 'wetbay_camera':{'ip':"192.168.10.100", 'node':"/rosout", "ping_alive": False, "node_alive" : False},\
#                 'drybay_camera':{'ip':"192.168.10.100", 'node':"/rosout", "ping_alive": False, "node_alive" : False},\
#                 'pt1_camera':{'ip':"192.168.10.100", 'node':"/rosout", "ping_alive": False, "node_alive" : False},\
#                 'pt2_camera':{'ip':"192.168.10.100", "ping_alive": False},\
#                 'ai':{'node':"/rosout" , "node_alive" : False},\
#                 'pcl':{'node':"/rosout", "node_alive" : False},\
#                 'ib1':{'ip':"192.168.10.100", 'node':"/rosout", "ping_alive": False, "node_alive" : False},\
#                 'ib2':{'ip':"192.168.10.100", "ping_alive": False},\
#                 # 'drybay_cover':{'usb':"/dev/drybay_cover" , "ping_alive": False},\
#                 # 'printer':{'usb':"/dev/printer" , "ping_alive": False},\
#                 # 'indicator_dispenser':{'usb':"/dev/indicator_dispenser" , "ping_alive": False},\
#                 }

def check_active_module(modules):
    feedback = os.popen("rosnode list").readlines()
    print(feedback)
    module_found = 0
    for x in modules:
        for i in range(len(feedback)):
            if feedback[i] == x+'\n':
                print("module found")
                module_found += 1
                break
    if module_found == len(modules):
        print("all modules is active")
        return True
    else:
        print("number of module given: " + str(len(modules))+", but found only: " + str(module_found))
        return False

def kill_list_of_modules(modules):
    print("killing a series of modules")
    module_killed = 0
    for x in modules:
        kill_str = "rosnode kill " + x
        kill_task = subprocess.Popen([kill_str], shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        
        poll_obj = select.poll()
        poll_obj.register(kill_task.stdout, select.POLLIN)
        kill_success = False
        timeout = False
        start_time = time.time()
        while(not kill_success and not timeout):
            if (time.time() - start_time) > 1:
                timeout = True
            poll_result = poll_obj.poll(0)
            if poll_result:
                stdout = kill_task.stdout.readlines()
                try:
                    if stdout[-1] == "killed\n":
                        print("successfully killed")
                        kill_success = True
                        module_killed += 1
                except:
                    print("modules doesnt exist")
                    sleep(1)
    
    if module_killed == len(modules):
        print("all modules successfully kill")
        return True
    else:
        print("module killed: " + str(module_killed) + ", but modules request: " + str(len(modules)))
        return False

def check_module_online(module):
    ip_online = False
    module_server_online = False
    if module == "ur10":
        hostname = "192.168.10.101" #example
        response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

        if response == 0:
            ip_online = True
            motion_client = actionlib.SimpleActionClient('motion_server', RobotControlAction)
            if motion_client.wait_for_server(timeout = rospy.Duration(1.0)):
                module_server_online = motion_client
            else:
                print("motion action not found ")
        
    elif module == "plc":
        hostname = "192.168.10.103" #example
        response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

        if response == 0:
            ip_online = True
            plc_client = rospy.ServiceProxy('plc_server', PLCService)
            try:
                plc_client.wait_for_service(timeout = rospy.Duration(1.0))
                module_server_online = plc_client
            except:
                print("plc service not found ")

    return (ip_online, module_server_online)

def get_active_modules():
    feedback = os.popen("rosnode list").readlines()
    active_module_list = []
    for i in range(len(feedback)):
        active_module_list.append(feedback[i].strip())
    
    ur10_robot_status, ur10_program_status = "", ""
    ip_online, motion_client = check_module_online("ur10")
    if ip_online:
        if motion_client:
            robot_goal = RobotControlGoal()
            robot_goal.option = robot_goal.DASHBOARD_SERVER_QUERY
            motion_client.send_goal(robot_goal)
            motion_client.wait_for_result()
            robot_result = RobotControlResult()
            robot_result = motion_client.get_result()
            if robot_result.success:
                ur10_robot_status = robot_result.dashboard_robot_status
                ur10_program_status = robot_result.dashboard_program_status
                success = True
            else:
                ur10_robot_status = "UNKNOWN"
                ur10_program_status = "UNKNOWN"
        else:
            ur10_robot_status = "UNKNOWN"
            ur10_program_status = "UNKNOWN"
    else:
        ur10_robot_status = "POWER OFF"
        ur10_program_status = "STOPPED"

    return active_module_list, ur10_robot_status, ur10_program_status

def rosnode_ping_check():
    global REQUIRED_MODULE_DICT
    active_module_list, ur10_robot_status, ur10_program_status = get_active_modules()
    launch_list, kill_list, not_powered_list, good_list = [], [], [], []
    
    #checking and populate data
    for x in REQUIRED_MODULE_DICT:
        if 'ip' in REQUIRED_MODULE_DICT[x]:
            response = os.system("timeout 0.2 ping -c 1 " + REQUIRED_MODULE_DICT[x]['ip'])
            if response != 0:
                REQUIRED_MODULE_DICT[x]['ping_alive'] = False
            else:
                REQUIRED_MODULE_DICT[x]['ping_alive'] = True
        elif 'usb' in REQUIRED_MODULE_DICT[x]:
            dev_list = os.popen("ls /dev").readlines()
            for i in range(len(dev_list)):
                if dev_list[i] == REQUIRED_MODULE_DICT[x]['usb'] + '\n':
                    REQUIRED_MODULE_DICT[x]['ping_alive'] = True
                    break
        if 'node' in REQUIRED_MODULE_DICT[x]:
            if REQUIRED_MODULE_DICT[x]['node'] in active_module_list:
                REQUIRED_MODULE_DICT[x]['node_alive'] = True
            else:
                REQUIRED_MODULE_DICT[x]['node_alive'] = False
        
    #creating solution
    for x in REQUIRED_MODULE_DICT:
        if 'ping_alive' in REQUIRED_MODULE_DICT[x] and 'node_alive' in REQUIRED_MODULE_DICT[x]:
            if not REQUIRED_MODULE_DICT[x]['ping_alive'] and not REQUIRED_MODULE_DICT[x]['node_alive']:
                not_powered_list.append(x)
            elif not REQUIRED_MODULE_DICT[x]['ping_alive'] and REQUIRED_MODULE_DICT[x]['node_alive']:
                not_powered_list.append(x)
                kill_list.append(x)
            elif REQUIRED_MODULE_DICT[x]['ping_alive'] and not REQUIRED_MODULE_DICT[x]['node_alive']:  
                launch_list.append(x)
            elif REQUIRED_MODULE_DICT[x]['ping_alive'] and REQUIRED_MODULE_DICT[x]['node_alive']:  
                good_list.append(x)
            else:
                print('i shouldnt reach here, please debug me')
        elif 'ping_alive' in REQUIRED_MODULE_DICT[x]:
            if not REQUIRED_MODULE_DICT[x]['ping_alive']:
                not_powered_list.append(x)
            else:
                good_list.append(x)
        elif 'node_alive' in REQUIRED_MODULE_DICT[x]:
            if not REQUIRED_MODULE_DICT[x]['node_alive']:
                launch_list.append(x)
            else:
                good_list.append(x)
        
    print("good_list: ", good_list)
    print("launch_list: ", launch_list)
    print("kill_list: ", kill_list)
    print("not_powered_list: ", not_powered_list)

    return good_list, launch_list,kill_list, not_powered_list 


########################################################################### launching function
def launch_camera():
    global PTPD_LAUNCHED

    if not PTPD_LAUNCHED:
        print("launching ptpd background task")
        print("launching 1")
        launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd1.sh']
        subprocess.Popen(launch_str)
        print("launching 2")
        launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd2.sh']
        subprocess.Popen(launch_str)
        print("launching 3")
        launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd3.sh']
        subprocess.Popen(launch_str)

        PTPD_LAUNCHED = True

    print("launching camera node and pcl")
    if DEBUG:
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch ai_server roboception.launch;']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        subprocess.Popen("roslaunch ai_server roboception.launch", shell= True)

    roboception_reconfigure = pexpect.spawn('../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/roboception_configure.sh')
    print(roboception_reconfigure.read())
    roboception_reconfigure.expect(pexpect.EOF)
    roboception_reconfigure.close()
    if not roboception_reconfigure.exitstatus:
        print("cameras configured")

    #check if nodes are successfully activated then return True
    module_list = ['/wetbay/rc_visard_driver', '/drybay/rc_visard_driver', '/pt1/rc_visard_driver', '/pcl_server']
    if check_active_module(module_list):
        return True
    else:
        return False

def launch_ai():
    print("launching ai nodes")
    if DEBUG:
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'source ~/anaconda3/etc/profile.d/conda.sh; conda activate tf; roslaunch ai_server ai.launch;']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        launch_str = ['. ../anaconda3/etc/profile.d/conda.sh; conda activate tf; roslaunch ai_server ai.launch;']
        subprocess.Popen(launch_str, shell= True)
    return True

def launch_ur10():
    print("pinging ur10 ip to check if it is powered on")
    hostname = "192.168.10.101" #example
    response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

    #and then check the response...
    if response == 0:
        print('ping success, launching software')
        if DEBUG:
            launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch ur10_pap ur10_pap_bringup.launch;']
            subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        else:
            subprocess.Popen('roslaunch ur10_pap ur10_pap_bringup.launch', shell=True)

        #check if nodes are successfully activated then return True
        sleep(35)
        module_list = ['/motion_server']
        if check_active_module(module_list):
            # result.success = True
            # result.message = "UR10 software launched successfully"
            return True
        else:
            # result.success = False
            # result.message = "UR10 software launch is unsuccesfully"
            return False
    else:
        rospy.logerr("UR10 Ping unsuccessful, is it powered on?")
        # result.success = False
        # result.message = "UR10 Ping unsuccessful, is it powered on?"
        return False

def launch_plc():
    print("pinging plc ip to check if it is powered on")
    hostname = "192.168.10.103" #example
    response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

    #and then check the response...
    if response == 0:
        print("launching plc node")
        if DEBUG:
            launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch plc_server plc.launch']
            subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        else:
            subprocess.Popen('roslaunch plc_server plc.launch', shell=True)
        sleep(4)
        module_list = ['/plc_server']
        if check_active_module(module_list):
            return True
        else:
            return False
    else:
        rospy.logerr("PLC Ping unsuccessful, is it powered on?")
        return False

def launch_ib1():
    return True
    if DEBUG:
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'rosrun ib1_server ib1_hardware_test.py']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        subprocess.Popen('rosrun ib1_server ib1_hardware_test.py', shell=True)
    sleep(4)
    module_list = ['/ib1_server']
    if check_active_module(module_list):
        # result.success = True
        # result.message = "Inspection Bay 1 software launched succesfully"
        return True
    else:
        # result.success = False
        # result.message = "Error during Inspection Bay 1 software launching"
        return False
########################################################################### launching function ends

class WebUIServerHandler(object):
    def __init__(self):
        self.server = actionlib.SimpleActionServer('webui_handler', WebUIAction, self.execute, False)
        self.server.start()
    
    def execute(self, goal):
        global PTPD_LAUNCHED
        result = WebUIResult()
        
        if goal.task == goal.GET_ACTIVE_MODULE: #GET_ACTIVE_MODULE = 1
            print("get active module query")
            result.active_modules, result.ur10_robot_status, result.ur10_program_status = get_active_modules()
            print(result)

        elif goal.task == goal.CHECK_MODULE_STATUS: #CHECK_MODULE_STATUS = 2
            # print("checking specific module status")
            feedback = os.popen("rosnode list").readlines()
            print(feedback)
            has_found = False
            for i in range(len(feedback)):
                if feedback[i] == goal.module+'\n':
                    print("module found")
                    has_found = True
                    break
            if has_found:
                result.success = True
            else:
                print("module not found")
                result.success = False

        elif goal.task == goal.LAUNCH:
            if goal.module == "program_manager":
                pigeonhole_to_process = ""
                for x in goal.arg2:
                    pigeonhole_to_process += str(x) + " "
                print("launching program manager with pigeonhole: " + pigeonhole_to_process)
                if DEBUG:
                    launch_str = ['gnome-terminal', '--', 'bash', '-c', 'rosrun program_manager combined_controller.py '+  pigeonhole_to_process]
                    print(launch_str)
                    subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                else:
                    subprocess.Popen('rosrun program_manager combined_controller.py '+  pigeonhole_to_process, shell=True)
                sleep(3)
                module_list = ['/program_manager']
                if check_active_module(module_list):
                    result.success = True
                    result.message = "program manager launched succesfully, pigeonhole to process: " + pigeonhole_to_process
                else:
                    result.success = False
                    result.message = "error during program manager launching"
            elif goal.module == "pt2_camera":
                subprocess.Popen('roslaunch ai_server pt2_individual.launch', shell=True)
                sleep(9)
                module_list = ['/pt2/rc_visard_driver']
                if check_active_module(module_list):
                    result.success = True
                    result.message = "pt2 camera driver launched succesfully, pigeonhole to process: "
                else:
                    result.success = False
                    result.message = "error during pt2 camera driver launching"
                

            print(result)
        elif goal.task == goal.MAIN_OPERATION_PAGE_QUICK_CHECK:
            good_list, launch_list,kill_list, not_powered_list = rosnode_ping_check()
            if len(launch_list) or len(kill_list) or len(not_powered_list):
                #some nodes are wrong
                result.success = False
                result.problematic_modules = launch_list + kill_list + not_powered_list
            else:
                result.success = True

        elif goal.task == goal.BOOTUP_OR_FIX:
            '''a function to bootup or relaunch some dead nodes. First will check if ip is alive, then will try to boot'''
            global CAMERA_LAUCHED, CAMERA_KILLED
            good_list, launch_list,kill_list, not_powered_list = rosnode_ping_check()

            if len(not_powered_list):
                #some modules aren't powered on, less likely if all are connected properly to main switch
                print("The following module(s) isn't powered, please check.")
                print(not_powered_list)
                result.problematic_modules = not_powered_list
                result.message = "not_powered"
                result.success = False

            elif len(kill_list):
                #some nodes to be killed
                for x in kill_list:
                    if x == "pt1_camera" or x == "wetbay_camera" or x == "drybay_camera" or x == "ai" or x == "pcl":
                        if not CAMERA_KILLED:
                            print("killing camera")
                            modules_to_kill = ['/wetbay/rc_visard_driver', '/drybay/rc_visard_driver', '/pt1/rc_visard_driver', '/pcl_server', '/ai_server']
                            if kill_list_of_modules(modules_to_kill):
                                CAMERA_KILLED = True
                    elif x == "pt2_camera":
                        modules_to_kill = ['/pt2/rc_visard_driver']
                        kill_list_of_modules(modules_to_kill)
                    elif x == "ur10":
                        modules_to_kill = ['/motion_server']
                        kill_list_of_modules(modules_to_kill)
                    elif x == "plc":
                        modules_to_kill = ['/plc_server']
                        kill_list_of_modules(modules_to_kill)
                    elif x == "ib1":
                        modules_to_kill = ['/ib1_server']
                        kill_list_of_modules(modules_to_kill)

            elif len(launch_list):
                launch_list.sort()
                #all modules are powered, launching or killing software
                for x in launch_list:
                    if x == "pt1_camera" or x == "wetbay_camera" or x == "drybay_camera" or x == "pcl":
                        #launch camera
                        if not CAMERA_LAUCHED:
                            print("launching camera")
                            CAMERA_LAUCHED = True
                            if launch_camera():
                                good_list.append(x)
                                CAMERA_LAUCHED = True
                        else:
                            good_list.append(x)
                    else:
                        print("launching "+x)
                        if eval("launch_" + x +"()"):
                            good_list.append(x)

                CAMERA_LAUCHED = False

                for x in good_list:
                    if x in launch_list:
                        launch_list.remove(x)

                if len(launch_list):
                    print("some nodes not launched successfully: ", launch_list)
                    result.problematic_modules = launch_list
                    result.message = "nodes_not_fully_launched"
                    result.success = False
                else:
                    print("all done")
                    result.success = True

            print(result)    
                

        elif goal.task == goal.KILL:
            if goal.module == "program_manager":
                modules_to_kill = ['/program_manager']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "Program manager terminated successfully "
                else:
                    result.success = False
                    result.message = "Program manager terminated is not successful"
            if goal.module == "pt2_camera":
                modules_to_kill = ['/pt2/rc_visard_driver']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "pt2 camera driver terminated successfully "
                else:
                    result.success = False
                    result.message = "pt2 camera driver terminated is not successful"

            print(result)
        
        elif goal.task == goal.SHUTDOWN:
            rospy.loginfo("signalling shutdown")
            # signal_shutdown to ur5 and ib2
            #
            #
            #
            modules_to_kill = ['/pcl_server', '/ai_server', '/motion_server', '/plc_server', '/roswebui']                
            kill_list_of_modules(modules_to_kill)
            # os.system("systemctl poweroff")

            print(result)

        elif goal.task == goal.ROUTER:
            if goal.module == "ur10":
                ip_online, motion_client = check_module_online("ur10")
                if ip_online:
                    if motion_client:
                        robot_goal = RobotControlGoal()
                        robot_goal.option = robot_goal.DASHBOARD_SERVER_ACTION
                        if goal.arg1 == "continue_program":
                            robot_goal.extra_argument = "play"
                        elif goal.arg1 == "pause_program":
                            robot_goal.extra_argument = "pause"
                        motion_client.send_goal(robot_goal)
                        motion_client.wait_for_result()
                        robot_result = RobotControlResult()
                        robot_result = motion_client.get_result()
                        if robot_result.success:
                            print("request to " + str(goal.arg1) + " is successful")
                            result.success = True
                        else:
                            result.success = False
                            result.message = "Motion server returned False"
                    else:
                        result.success = False
                        result.message = "UR10 motion server not found, please try relaunch"
                else:
                    result.success = False
                    result.message = "UR10 IP not found, is it powered on?"
                
            elif goal.module == "plc":
                ip_online, plc_client = check_module_online("plc")
                if ip_online:
                    if plc_client:
                        plc_request = PLCServiceRequest()
                        if "toggle" in goal.arg1:
                            plc_request.task = plc_request.WRITE_FROM_UI
                            plc_request.custom_pin = int(goal.arg1.split("_")[-1])
                        else:
                            plc_request.task = plc_request.READ_BULK
                            plc_request.bulk_read_module = goal.arg1
                        plc_response = plc_client(plc_request)
                        if plc_response.success:
                            # print("request is successful")
                            # print(plc_response.bulk_read_return_value)
                            result.success = True
                            result.input_condition = plc_response.bulk_read_return_value
                        else:
                            result.success = False
                            result.message = plc_response.message
                    else:
                        result.success = False
                        result.message = "PLC service not found, please try relaunch"
                else:
                    result.success = False
                    result.message = "PLC IP not found, is it powered on?"
                
            
            print(result)
        
        self.server.set_succeeded(result)

def kill_all():
    os.killpg(os.getpgid(react_process.pid), signal.SIGTERM)
    os.killpg(os.getpgid(node_process.pid), signal.SIGTERM)

if __name__ == "__main__":
    rospy.init_node('webui_handler', anonymous= True)
    server = WebUIServerHandler()
    launch_str = ['sudo systemctl restart mongod.service']
    subprocess.Popen(launch_str, shell= True)
    if DEBUG:
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'cd ../all_ws/ur_ws/src/goldfinger_ui/backend && npm start']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        sleep(1)
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'cd ../all_ws/ur_ws/src/goldfinger_ui/frontend && npm start']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        launch_str = ['cd ../all_ws/ur_ws/src/goldfinger_ui/backend && npm start']
        node_process = subprocess.Popen(launch_str, shell= True, preexec_fn=os.setsid)
        sleep(1)
        launch_str = ['cd ../all_ws/ur_ws/src/goldfinger_ui/frontend && npm start']
        react_process = subprocess.Popen(launch_str, shell= True, preexec_fn=os.setsid)
        rospy.on_shutdown(kill_all)
    