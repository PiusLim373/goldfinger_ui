#!/usr/bin/env python
from pickle import FALSE
import signal
import subprocess
from time import sleep
from ur10_pap.msg import *
import os
import actionlib
import rospy
from roswebui.msg import *
from ai_server.msg import *
import sys
import pexpect
import select
import time

DEBUG= False
PTPD_LAUNCHED = False

def check_active_module(modules):
    feedback = os.popen("rosnode list").readlines()
    print(feedback)
    module_found = 0
    for x in modules:
        for i in range(len(feedback)):
            if feedback[i] == x+'\n':
                print("module found")
                module_found += 1
                break
    if module_found == len(modules):
        print("all modules is active")
        return True
    else:
        print("number of module given: " + str(len(modules))+", but found only: " + str(module_found))
        return False

def launch_ai():
    launch_str = ['. ../anaconda3/etc/profile.d/conda.sh; conda activate tf; roslaunch ai_server ai.launch;']
    subprocess.Popen(launch_str, shell= True)

def launch_camera():
    global PTPD_LAUNCHED
    if not PTPD_LAUNCHED:
        print("launching ptpd background task")
        print("launching 1")
        launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd1.sh']
        subprocess.Popen(launch_str)
        print("launching 2")
        launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd2.sh']
        subprocess.Popen(launch_str)
        print("launching 3")
        launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd3.sh']
        subprocess.Popen(launch_str)

        PTPD_LAUNCHED = True


    print("launching camera node and pcl")
    if DEBUG:
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch ai_server roboception.launch;']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        subprocess.Popen("roslaunch ai_server roboception.launch", shell= True)

    print("launching ai nodes")
    if DEBUG:
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'source ~/anaconda3/etc/profile.d/conda.sh; conda activate tf; roslaunch ai_server ai.launch;']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        launch_str = ['. ./anaconda3/etc/profile.d/conda.sh; conda activate tf; roslaunch ai_server ai.launch;']
        subprocess.Popen(launch_str, shell= True)

    roboception_reconfigure = pexpect.spawn('../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/roboception_configure.sh')
    print(roboception_reconfigure.read())
    roboception_reconfigure.expect(pexpect.EOF)
    roboception_reconfigure.close()
    if not roboception_reconfigure.exitstatus:
        print("cameras configured")

    #check if nodes are successfully activated then return True
    module_list = ['/wetbay/rc_visard_driver', '/drybay/rc_visard_driver', '/pt1/rc_visard_driver', '/pcl_server','/ai_server']
    if check_active_module(module_list):
        # result.success = True
        # result.message = "cameras software launched succesfully"
        return True
    else:
        # result.success = False
        # result.message = "cameras software launch is unsuccesfully"
        return False

def launch_ur10():
    start = time.time()
    print("pinging ur10 ip to check if it is powered on")
    hostname = "192.168.10.101" #example
    response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

    #and then check the response...
    if response == 0:
        print('ping success, launching software')
        if DEBUG:
            launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch ur10_pap ur10_pap_bringup.launch;']
            subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        else:
            subprocess.Popen('roslaunch ur10_pap ur10_pap_bringup.launch', shell=True)

        #check if nodes are successfully activated then return True
        sleep(30)
        module_list = ['/motion_server']
        if check_active_module(module_list):
            print(time.time() - start)
            return True
        else:
            return False
    else:
        rospy.logerr("UR10 Ping unsuccessful, is it powered on?")
        # result.success = False
        # result.message = "UR10 Ping unsuccessful, is it powered on?"
        return False

def launch_plc():
    print("pinging plc ip to check if it is powered on")
    hostname = "192.168.10.103" #example
    response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

    #and then check the response...
    if response == 0:
        print("launching plc node")
        if DEBUG:
            launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch plc_server plc.launch']
            subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        else:
            subprocess.Popen('roslaunch plc_server plc.launch', shell=True)
        sleep(4)
        module_list = ['/plc_server']
        if check_active_module(module_list):
            # result.success = True
            # result.message = "PLC software launched successfully"
            return True
        else:
            # result.success = False
            # result.message = "PLC software launch is unsuccesful"
            return False
    else:
        rospy.logerr("PLC Ping unsuccessful, is it powered on?")
        # result.success = False
        # result.message = "PLC Ping unsuccessful, is it powered on?"
        return False

def kill_list_of_modules(modules):
    print("killing a series of modules")
    module_killed = 0
    for x in modules:
        kill_str = "rosnode kill " + x
        kill_task = subprocess.Popen([kill_str], shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        
        poll_obj = select.poll()
        poll_obj.register(kill_task.stdout, select.POLLIN)
        kill_success = False
        timeout = False
        start_time = time.time()
        while(not kill_success and not timeout):
            if (time.time() - start_time) > 1:
                timeout = True
            poll_result = poll_obj.poll(0)
            if poll_result:
                stdout = kill_task.stdout.readlines()
                try:
                    if stdout[-1] == "killed\n":
                        print("successfully killed")
                        kill_success = True
                        module_killed += 1
                except:
                    print("modules doesnt exist")
                    sleep(1)
    
    if module_killed == len(modules):
        print("all modules successfully kill")
        return True
    else:
        print("module killed: " + str(module_killed) + ", but modules request: " + str(len(modules)))
        return False

REQUIRED_MODULE_DICT ={
                'drybay_cover':{'usb':"drybay_cover" , "ping_alive": False},\
                # 'printer':{'usb':"/dev/printer" , "ping_alive": False},\
                # 'indicator_dispenser':{'usb':"/dev/indicator_dispenser" , "ping_alive": False},\
                }

def rosnode_ping_check():
    global REQUIRED_MODULE_DICT
    
    #checking and populate data
    for x in REQUIRED_MODULE_DICT:
        if 'ip' in REQUIRED_MODULE_DICT[x]:
            response = os.system("timeout 0.2 ping -c 1 " + REQUIRED_MODULE_DICT[x]['ip'])
            if response != 0:
                REQUIRED_MODULE_DICT[x]['ping_alive'] = False
            else:
                REQUIRED_MODULE_DICT[x]['ping_alive'] = True
        elif 'usb' in REQUIRED_MODULE_DICT[x]:
            dev_list = os.popen("ls /dev").readlines()
            for i in range(len(dev_list)):
                if dev_list[i] == REQUIRED_MODULE_DICT[x]['usb'] + '\n':
                    REQUIRED_MODULE_DICT[x]['ping_alive'] = True
                    break
   
def true_func():
    return True

if __name__ == "__main__":
    rospy.init_node("launcher")
    # launch_str = ['. ./anaconda3/etc/profile.d/conda.sh; conda activate tf; roslaunch ai_server ai_playground.launch;']
    # subprocess.Popen(launch_str, shell= True)
    # print(launch_plc())
    # print(launch_camera())
    # print(launch_ur10())
    # print(os.getcwd())
    # print(launch_ur10())
    # print("ended")
    x = "WETBAY_DRAWER_11111"
    print(x.split("_")[-1])


