#!/usr/bin/env python
from distutils.util import execute
import rospy
import actionlib
from roswebui.msg import *

import subprocess
from time import sleep
import os
import signal
import pexpect
import select
import time
from ur10_pap.msg import *
from plc_server.srv import *

DEBUG = False
PTPD_LAUNCHED = False
# IP_MODULE_DICT = {\
#                 'ur10': '192.168.10.105',\
#                 'ur5': '192.168.10.105', \
#                 'ur5_pc': '192.168.10.105', \
#                 'plc': '192.168.10.105',\
#                 'ib1_pc': '192.168.10.105',\
#                 'ib1_rasp': '192.168.10.105',\
#                 'ib2_pc': '192.168.10.105',\
#                 'wb_camera': '192.168.10.105',\
#                 'db_camera': '192.168.10.105',\
#                 'pt1_camera': '192.168.10.105',\
#                 'pt2_camera': '192.168.10.105',\
#                 'plc': '192.168.10.105',\
#                 }
# USB_MODULE_DICT = {
#                 'db_cover': '/dev/drybay_cover',\
#                 'printer': '/dev/printer',\
#                 'indicator_dispenser': '/dev/indicator_dispenser',\
#                 }
REQUIRED_MODULE_DICT ={
                'plc':{'ip':"192.168.10.103", 'node':"/plc_server", "ping_alive": False, "node_alive" : False},\
                'ur10':{'ip':"192.168.10.101", 'node':"/motion_server", "ping_alive": False, "node_alive" : False},\
                'ur5':{'ip':"192.168.10.105", "ping_alive": False},\
                'wetbay_camera':{'ip':"192.168.10.105", 'node':"/wetbay/rc_visard_driver", "ping_alive": False, "node_alive" : False},\
                'drybay_camera':{'ip':"192.168.10.105", 'node':"/drybay/rc_visard_driver", "ping_alive": False, "node_alive" : False},\
                'pt1_camera':{'ip':"192.168.10.105", 'node':"/pt1/rc_visard_driver", "ping_alive": False, "node_alive" : False},\
                'pt2_camera':{'ip':"192.168.10.105", "ping_alive": False},\
                'ai':{'node':"/ai_server" , "node_alive" : False},\
                'pcl':{'node':"/pcl_server", "node_alive" : False},\
                'ib1':{'ip':"192.168.10.105", 'node':"/ib1_server", "ping_alive": False, "node_alive" : False},\
                'ib2':{'ip':"192.168.10.105", "ping_alive": False},\
                # 'drybay_cover':{'usb':"/dev/drybay_cover" , "ping_alive": False},\
                # 'printer':{'usb':"/dev/printer" , "ping_alive": False},\
                # 'indicator_dispenser':{'usb':"/dev/indicator_dispenser" , "ping_alive": False},\
                }

def check_active_module(modules):
    feedback = os.popen("rosnode list").readlines()
    print(feedback)
    module_found = 0
    for x in modules:
        for i in range(len(feedback)):
            if feedback[i] == x+'\n':
                print("module found")
                module_found += 1
                break
    if module_found == len(modules):
        print("all modules is active")
        return True
    else:
        print("number of module given: " + str(len(modules))+", but found only: " + str(module_found))
        return False

def kill_list_of_modules(modules):
    print("killing a series of modules")
    module_killed = 0
    for x in modules:
        kill_str = "rosnode kill " + x
        kill_task = subprocess.Popen([kill_str], shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        
        poll_obj = select.poll()
        poll_obj.register(kill_task.stdout, select.POLLIN)
        kill_success = False
        timeout = False
        start_time = time.time()
        while(not kill_success and not timeout):
            if (time.time() - start_time) > 1:
                timeout = True
            poll_result = poll_obj.poll(0)
            if poll_result:
                stdout = kill_task.stdout.readlines()
                try:
                    if stdout[-1] == "killed\n":
                        print("successfully killed")
                        kill_success = True
                        module_killed += 1
                except:
                    print("modules doesnt exist")
                    sleep(1)
    
    if module_killed == len(modules):
        print("all modules successfully kill")
        return True
    else:
        print("module killed: " + str(module_killed) + ", but modules request: " + str(len(modules)))
        return False

def check_module_online(module):
    ip_online = False
    module_server_online = False
    if module == "ur10":
        hostname = "192.168.10.101" #example
        response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

        if response == 0:
            ip_online = True
            motion_client = actionlib.SimpleActionClient('motion_server', RobotControlAction)
            if motion_client.wait_for_server(timeout = rospy.Duration(1.0)):
                module_server_online = motion_client
            else:
                print("motion action not found ")
        
    elif module == "plc":
        hostname = "192.168.10.103" #example
        response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

        if response == 0:
            ip_online = True
            plc_client = rospy.ServiceProxy('plc_server', PLCService)
            try:
                plc_client.wait_for_service(timeout = rospy.Duration(1.0))
                module_server_online = plc_client
            except:
                print("plc service not found ")

    return (ip_online, module_server_online)

def get_active_modules():
    feedback = os.popen("rosnode list").readlines()
    active_module_list = []
    for i in range(len(feedback)):
        active_module_list.append(feedback[i].strip())
    
    ur10_robot_status, ur10_program_status = "", ""
    ip_online, motion_client = check_module_online("ur10")
    if ip_online:
        if motion_client:
            robot_goal = RobotControlGoal()
            robot_goal.option = robot_goal.DASHBOARD_SERVER_QUERY
            motion_client.send_goal(robot_goal)
            motion_client.wait_for_result()
            robot_result = RobotControlResult()
            robot_result = motion_client.get_result()
            if robot_result.success:
                ur10_robot_status = robot_result.dashboard_robot_status
                ur10_program_status = robot_result.dashboard_program_status
                success = True
            else:
                ur10_robot_status = "UNKNOWN"
                ur10_program_status = "UNKNOWN"
        else:
            ur10_robot_status = "UNKNOWN"
            ur10_program_status = "UNKNOWN"
    else:
        ur10_robot_status = "POWER OFF"
        ur10_program_status = "STOPPED"

    return active_module_list, ur10_robot_status, ur10_program_status

def ping_check():
    ping_check_fail_modules = []
    for x in REQUIRED_MODULE_DICT:
        if 'ip' in REQUIRED_MODULE_DICT[x]:
            print("checking " + str(x) + "'s ip")
            response = os.system("timeout 0.2 ping -c 1 " + REQUIRED_MODULE_DICT[x]['ip'])
            if response != 0:
                ping_check_fail_modules.append(x)
        elif 'usb' in REQUIRED_MODULE_DICT[x]:
            print("checking " + str(x) + "'s usb")
            print(REQUIRED_MODULE_DICT[x]['usb'])       #to figure out how to check usb
    
    return ping_check_fail_modules

class WebUIServerHandler(object):
    def __init__(self):
        self.server = actionlib.SimpleActionServer('webui_handler', WebUIAction, self.execute, False)
        self.server.start()
    
    def execute(self, goal):
        global PTPD_LAUNCHED
        result = WebUIResult()
        
        if goal.task == goal.GET_ACTIVE_MODULE: #GET_ACTIVE_MODULE = 1
            print("get active module query")
            result.active_modules, result.ur10_robot_status, result.ur10_program_status = get_active_modules()
            print(result)

        elif goal.task == goal.CHECK_MODULE_STATUS: #CHECK_MODULE_STATUS = 2
            # print("checking specific module status")
            feedback = os.popen("rosnode list").readlines()
            print(feedback)
            has_found = False
            for i in range(len(feedback)):
                if feedback[i] == goal.module+'\n':
                    print("module found")
                    has_found = True
                    break
            if has_found:
                result.success = True
            else:
                print("module not found")
                result.success = False

        elif goal.task == goal.IP_CHECK: #IP_CHECK = 3
            '''pinging ip address and usb of hardware '''

            result.problematic_modules = ping_check()
            result.success = True

        elif goal.task == goal.LAUNCH:
            if goal.module == "camera":
                if not PTPD_LAUNCHED:
                    print("launching ptpd background task")
                    print("launching 1")
                    launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd1.sh']
                    subprocess.Popen(launch_str)
                    print("launching 2")
                    launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd2.sh']
                    subprocess.Popen(launch_str)
                    print("launching 3")
                    launch_str = ['sudo', '../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/ptpd3.sh']
                    subprocess.Popen(launch_str)

                    PTPD_LAUNCHED = True


                print("launching camera node and pcl")
                if DEBUG:
                    launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch ai_server roboception.launch;']
                    subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                else:
                    subprocess.Popen("roslaunch ai_server roboception.launch", shell= True)

                print("launching ai nodes")
                launch_str = ['gnome-terminal', '--', 'bash', '-c', 'source ~/anaconda3/etc/profile.d/conda.sh; conda activate tf; roslaunch ai_server ai.launch;']
                subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                # pipe = subprocess.Popen(launch_str, shell=True, stdout= subprocess.PIPE, stderr= subprocess.PIPE)
                # result.pid = pipe.pid
                # sleep(5)

                roboception_reconfigure = pexpect.spawn('../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/roboception_configure.sh')
                print(roboception_reconfigure.read())
                roboception_reconfigure.expect(pexpect.EOF)
                roboception_reconfigure.close()
                if not roboception_reconfigure.exitstatus:
                    print("cameras configured")

                #check if nodes are successfully activated then return True
                module_list = ['/wetbay/rc_visard_driver', '/drybay/rc_visard_driver', '/pt1/rc_visard_driver', '/pcl_server','/ai_server']
                if check_active_module(module_list):
                    result.success = True
                    result.message = "cameras software launched succesfully"
                else:
                    result.success = False
                    result.message = "cameras software launch is unsuccesfully"

            elif goal.module == "ur10":
                print("pinging ur10 ip to check if it is powered on")
                hostname = "192.168.10.101" #example
                response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

                #and then check the response...
                if response == 0:
                    print('ping success, launching software')
                    # robot_control = subprocess.Popen("roslaunch ur10_pap demo.launch", shell=True, stdout= subprocess.PIPE, stderr= subprocess.PIPE)
                    if 1:
                        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch ur10_pap ur10_pap_bringup.launch;']
                        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                    else:
                        subprocess.Popen('roslaunch ur10_pap ur10_pap_bringup.launch', shell=True)

                    #check if nodes are successfully activated then return True
                    sleep(10)
                    module_list = ['/motion_server']
                    if check_active_module(module_list):
                        result.success = True
                        result.message = "UR10 software launched successfully"
                    else:
                        result.success = False
                        result.message = "UR10 software launch is unsuccesfully"
                else:
                    rospy.logerr("UR10 Ping unsuccessful, is it powered on?")
                    result.success = False
                    result.message = "UR10 Ping unsuccessful, is it powered on?"

            elif goal.module == "plc":
                
                print("pinging plc ip to check if it is powered on")
                hostname = "192.168.10.103" #example
                response = os.system("timeout 0.2 ping -c 1 " + str(hostname))

                #and then check the response...
                if response == 0:
                    print("launching plc node")
                    if DEBUG:
                        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'roslaunch plc_server plc.launch']
                        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                    else:
                        subprocess.Popen('roslaunch plc_server plc.launch', shell=True)
                    sleep(4)
                    module_list = ['/plc_server']
                    if check_active_module(module_list):
                        result.success = True
                        result.message = "PLC software launched successfully"
                    else:
                        result.success = False
                        result.message = "PLC software launch is unsuccesful"
                else:
                    rospy.logerr("PLC Ping unsuccessful, is it powered on?")
                    result.success = False
                    result.message = "PLC Ping unsuccessful, is it powered on?"

            elif goal.module == "program_manager":
                pigeonhole_to_process = ""
                for x in goal.arg2:
                    pigeonhole_to_process += str(x) + " "
                print("launching program manager with pigeonhole: " + pigeonhole_to_process)
                if DEBUG:
                    launch_str = ['gnome-terminal', '--', 'bash', '-c', 'rosrun program_manager combined_controller.py '+  pigeonhole_to_process]
                    print(launch_str)
                    subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                else:
                    subprocess.Popen('rosrun program_manager combined_controller.py '+  pigeonhole_to_process, shell=True)
                sleep(3)
                module_list = ['/program_manager']
                if check_active_module(module_list):
                    result.success = True
                    result.message = "program manager launched succesfully, pigeonhole to process: " + pigeonhole_to_process
                else:
                    result.success = False
                    result.message = "error during program manager launching"

            elif goal.module == "ib1":
                if DEBUG:
                    launch_str = ['gnome-terminal', '--', 'bash', '-c', 'rosrun ib1_server ib1_hardware_test.py']
                    subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                else:
                    subprocess.Popen('rosrun ib1_server ib1_hardware_test.py', shell=True)
                sleep(4)
                module_list = ['/ib1_server']
                if check_active_module(module_list):
                    result.success = True
                    result.message = "Inspection Bay 1 software launched succesfully"
                else:
                    result.success = False
                    result.message = "Error during Inspection Bay 1 software launching"

            
            elif goal.module == "debug":
                print("hi")
                subprocess.Popen("source ../anaconda3/etc/profile.d/conda.sh", shell= True)
                # roboception_reconfigure = pexpect.spawn('../all_ws/ur_ws/src/goldfinger_vision/ai_server/scripts/bash_script/roboception_configure.sh')
                # print(roboception_reconfigure.read())
                # roboception_reconfigure.expect(pexpect.EOF)
                # roboception_reconfigure.close()
                # if not roboception_reconfigure.exitstatus:
                #     print("cameras configured")
            elif goal.module == "ur5":
                sleep(5)
                result.success = True
            print(result)
            
        elif goal.task == goal.BOOTUP_OR_FIX:
            '''a function to bootup or relaunch some dead nodes. First will check if ip is alive, then will try to boot'''
            global REQUIRED_MODULE_DICT
            active_module_list, ur10_robot_status, ur10_program_status = get_active_modules()
            launch_list, kill_list, not_powered_list, good_list = [], [], [], []
            
            #checking and populate data
            for x in REQUIRED_MODULE_DICT:
                if 'ip' in REQUIRED_MODULE_DICT[x]:
                    response = os.system("timeout 0.2 ping -c 1 " + REQUIRED_MODULE_DICT[x]['ip'])
                    if response != 0:
                        REQUIRED_MODULE_DICT[x]['ping_alive'] = False
                    else:
                        REQUIRED_MODULE_DICT[x]['ping_alive'] = True
                elif 'usb' in REQUIRED_MODULE_DICT[x]:
                    # to figure out how to ping check usb
                    REQUIRED_MODULE_DICT[x]['ping_alive'] = False
                if 'node' in REQUIRED_MODULE_DICT[x]:
                    if REQUIRED_MODULE_DICT[x]['node'] in active_module_list:
                        REQUIRED_MODULE_DICT[x]['node_alive'] = True
                    else:
                        REQUIRED_MODULE_DICT[x]['node_alive'] = False
                
            #creating solution
            for x in REQUIRED_MODULE_DICT:
                if 'ping_alive' in REQUIRED_MODULE_DICT[x] and 'node_alive' in REQUIRED_MODULE_DICT[x]:
                    if not REQUIRED_MODULE_DICT[x]['ping_alive'] and not REQUIRED_MODULE_DICT[x]['node_alive']:
                        not_powered_list.append(x)
                    elif not REQUIRED_MODULE_DICT[x]['ping_alive'] and REQUIRED_MODULE_DICT[x]['node_alive']:
                        not_powered_list.append(x)
                        kill_list.append(x)
                    elif REQUIRED_MODULE_DICT[x]['ping_alive'] and not REQUIRED_MODULE_DICT[x]['node_alive']:  
                        launch_list.append(x)
                    elif REQUIRED_MODULE_DICT[x]['ping_alive'] and REQUIRED_MODULE_DICT[x]['node_alive']:  
                        good_list.append(x)
                    else:
                        print('i shouldnt reach here, please debug me')
                elif 'ping_alive' in REQUIRED_MODULE_DICT[x]:
                    if not REQUIRED_MODULE_DICT[x]['ping_alive']:
                        not_powered_list.append(x)
                    else:
                        good_list.append(x)
                elif 'node_alive' in REQUIRED_MODULE_DICT[x]:
                    if not REQUIRED_MODULE_DICT[x]['node_alive']:
                        launch_list.append(x)
                    else:
                        good_list.append(x)
                
            print("good_list: ", good_list)
            print("launch_list: ", launch_list)
            print("kill_list: ", kill_list)
            print("not_powered_list: ", not_powered_list)

            if len(not_powered_list):
                #some modules aren't powered on, less likely if all are connected properly to main switch
                print("The following module(s) isn't powered, please check.")
                print(not_powered_list)
                result.problematic_modules = not_powered_list
                result.quick_fix_message = "not_powered"

            else:
                #all modules are powered, launching or killing software
                for x in launch_list:
                    print(x)
                    if x == "pt1_camera" or x == "wetbay_camera" or x == "drybay_camera" or x == "pt2_camera":
                        #launch camera
                        print("launching camera")
                        temp_goal = WebUIGoal()
                        temp_goal.task = temp_goal.DEBUG
                        temp_goal.module = "cameraa"
                        server.execute(temp_goal)
                    else:
                        #launch other modules
                        print("launching ", x)
                        temp_goal = WebUIGoal()
                        temp_goal.task = temp_goal.DEBUG
                        temp_goal.module = x
                        server.execute(temp_goal)
                print("all done")



        elif goal.task == goal.DEBUG:
            print("running debug cycle, module: ", goal.module)
            result.success = True
            return

        elif goal.task == goal.KILL:

            if goal.module == "plc":
                modules_to_kill = ['/plc_server']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "PLC software successfully powered off"
                else:
                    result.success = False
                    result.message = "PLC software not successfully powered off"

            elif goal.module == "camera":
                modules_to_kill = ['/wetbay/rc_visard_driver', '/drybay/rc_visard_driver', '/pt1/rc_visard_driver', '/pt2/rc_visard_driver', '/pcl_server', '/ai_server']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "Camera software successfully powered off"
                else:
                    result.success = False
                    result.message = "Camera software not successfully powered off"

            elif goal.module == "ur10":
                modules_to_kill = ['/motion_server']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "UR10 software successfully powered off"
                else:
                    result.success = False
                    result.message = "UR10 software not successfully powered off"

            elif goal.module == "program_manager":
                modules_to_kill = ['/program_manager']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "Program manager terminated successfully "
                else:
                    result.success = False
                    result.message = "Program manager terminated is not successful"


            if goal.module == "ib1":
                modules_to_kill = ['/ib1_server']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "Non Ring Inspection Bay software successfully powered off"
                else:
                    result.success = False
                    result.message = "Non Ring Inspection Bay software not successfully powered off"

            elif goal.module == "ur5":
                modules_to_kill = ['/ur5']
                if kill_list_of_modules(modules_to_kill):
                    result.success = True
                    result.message = "UR5 software successfully powered off"
                else:
                    result.success = False
                    result.message = "UR5 software not successfully powered off"
            print(result)

                # print("kill node debug")
                
                # kill_task = subprocess.Popen(["rosnode kill /webui_handler_26214_1637658448041"], shell=True, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
                
                # poll_obj = select.poll()
                # poll_obj.register(kill_task.stdout, select.POLLIN)
                # kill_success = False
                # timeout = False
                # start_time = time.time()
                # while(not kill_success and not timeout):
                #     if (time.time() - start_time) > 1:
                #         timeout = True
                #         print("timeout")
                #         result.success = False
                #     poll_result = poll_obj.poll(0)
                #     if poll_result:
                #         stdout = kill_task.stdout.readlines()
                #         try:
                #             if stdout[-1] == "killed\n":
                #                 print("successfully killed")
                #                 kill_success = True
                #                 result.success = True
                #         except:
                #             print("modules doesnt exsit")
                #             result.success = False

        elif goal.task == goal.ROUTER:
            if goal.module == "ur10":
                ip_online, motion_client = check_module_online("ur10")
                if ip_online:
                    if motion_client:
                        robot_goal = RobotControlGoal()
                        robot_goal.option = robot_goal.DASHBOARD_SERVER_ACTION
                        if goal.arg1 == "continue_program":
                            robot_goal.extra_argument = "play"
                        elif goal.arg1 == "pause_program":
                            robot_goal.extra_argument = "pause"
                        motion_client.send_goal(robot_goal)
                        motion_client.wait_for_result()
                        robot_result = RobotControlResult()
                        robot_result = motion_client.get_result()
                        if robot_result.success:
                            print("request to " + str(goal.arg1) + " is successful")
                            result.success = True
                        else:
                            result.success = False
                            result.message = "Motion server returned False"
                    else:
                        result.success = False
                        result.message = "UR10 motion server not found, please try relaunch"
                else:
                    result.success = False
                    result.message = "UR10 IP not found, is it powered on?"
                
            elif goal.module == "plc":
                ip_online, plc_client = check_module_online("plc")
                if ip_online:
                    if plc_client:
                        plc_request = PLCServiceRequest()
                        plc_request.task = plc_request.READ_BULK
                        plc_request.bulk_read_module = "wetbay_drybay_input"
                        plc_response = plc_client(plc_request)
                        if plc_response.success:
                            # print("request is successful")
                            # print(plc_response.bulk_read_return_value)
                            result.success = True
                            result.input_condition = plc_response.bulk_read_return_value
                        else:
                            result.success = False
                            result.message = "PLC service returned False"
                    else:
                        result.success = False
                        result.message = "PLC service not found, please try relaunch"
                else:
                    result.success = False
                    result.message = "PLC IP not found, is it powered on?"
                
            
            print(result)
        
        self.server.set_succeeded(result)

def kill_all():
    os.killpg(os.getpgid(react_process.pid), signal.SIGTERM)
    os.killpg(os.getpgid(node_process.pid), signal.SIGTERM)

if __name__ == "__main__":
    rospy.init_node('webui_handler', anonymous= True)
    server = WebUIServerHandler()
    launch_str = ['sudo systemctl restart mongod.service']
    subprocess.Popen(launch_str, shell= True)
    if DEBUG:
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'cd ../all_ws/ur_ws/src/goldfinger_ui/backend && npm start']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
        sleep(1)
        launch_str = ['gnome-terminal', '--', 'bash', '-c', 'cd ../all_ws/ur_ws/src/goldfinger_ui/frontend && npm start']
        subprocess.Popen(launch_str, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
    else:
        launch_str = ['cd ../all_ws/ur_ws/src/goldfinger_ui/backend && npm start']
        node_process = subprocess.Popen(launch_str, shell= True, preexec_fn=os.setsid)
        sleep(1)
        launch_str = ['cd ../all_ws/ur_ws/src/goldfinger_ui/frontend && npm start']
        react_process = subprocess.Popen(launch_str, shell= True, preexec_fn=os.setsid)
        rospy.on_shutdown(kill_all)
    