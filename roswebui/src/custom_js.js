function launchBtn(module) {
    var launch = new ROSLIB.Topic({
        ros: ros,
        name: "/launch",
        messageType: "std_msgs/String",
    });
    if (module === "camera") {
        console.log("launching camera");
        var launch_msg = new ROSLIB.Message({
        data: "ai_server roboception.launch",
        });
        launch.publish(launch_msg);
    }
}

function killBtn(module){
    var kill = new ROSLIB.Topic({
        ros: ros,
        name: "/kill",
        messageType: "std_msgs/String",
    });
    if (module === "camera") {
        console.log("killing camera");
        var msg = new ROSLIB.Message({
        data: "",
        });
        // kill.publish(msg);
    }
}

function cameraTestBtn(module){
    var action = new ROSLIB.ActionClient({
        ros: ros,
        serverName: "/pcl_server",
        actionName: "pcl_server/PCLServerAction",
    });
    
    var goal = new ROSLIB.Goal({
        actionClient : action,
        goalMessage : {
          module: module,
          task: 1
        }
      });

    goal.on('result', function(result) {
    console.log('Final Result: ' + result.success);
    });
    goal.send();
}

function DebugBtn(module){
    var action = new ROSLIB.ActionClient({
        ros: ros,
        serverName: "/pcl_server",
        actionName: "pcl_server/PCLServerAction",
    });
    
    var goal = new ROSLIB.Goal({
        actionClient : action,
        goalMessage : {
          module: 'pt1',
          task: 1
        }
      });

    goal.on('result', function(result) {
    console.log('Final Result: ' + result.success);
    });
    goal.send();
}

function checkCameraStatus() {
    var action = new ROSLIB.ActionClient({
        ros: ros,
        serverName: "/webui_handler",
        actionName: "roswebui/WebUIAction",
    });
    
    var goal = new ROSLIB.Goal({
        actionClient : action,
        goalMessage : {
          module: '/pt1/rc_visard_driver',
          task: 1
        }
      });

    goal.on('result', function(result) {
        if(result.success){
            $("#pt1_camera_status").removeClass("badge-danger");
            $("#pt1_camera_status").addClass("badge-success");
        }
        else{
            $("#pt1_camera_status").removeClass("badge-success");
            $("#pt1_camera_status").addClass("badge-danger");
        }
    });
    goal.send();

    var goal = new ROSLIB.Goal({
        actionClient : action,
        goalMessage : {
          module: '/wetbay/rc_visard_driver',
          task: 1
        }
      });

    goal.on('result', function(result) {
        if(result.success){
            $("#wetbay_camera_status").removeClass("badge-danger");
            $("#wetbay_camera_status").addClass("badge-success");
        }
        else{
            $("#wetbay_camera_status").removeClass("badge-success");
            $("#wetbay_camera_status").addClass("badge-danger");
        }
    });
    goal.send();

    var goal = new ROSLIB.Goal({
        actionClient : action,
        goalMessage : {
          module: '/drybay/rc_visard_driver',
          task: 1
        }
      });

    goal.on('result', function(result) {
        if(result.success){
            $("#drybay_camera_status").removeClass("badge-danger");
            $("#drybay_camera_status").addClass("badge-success");
        }
        else{
            $("#drybay_camera_status").removeClass("badge-success");
            $("#drybay_camera_status").addClass("badge-danger");
        }
    });
    goal.send();
    console.log("added");
    setTimeout(checkCameraStatus, 5000);
}

function updateUIStatus(){
    let statusUI = {
        'pt1_camera_status': false,
        'wetbay_camera_status': false,
        'drybay_camera_status': false,
    };

    var action = new ROSLIB.ActionClient({
        ros: ros,
        serverName: "/webui_handler",
        actionName: "roswebui/WebUIAction",
    });
    
    var goal = new ROSLIB.Goal({
        actionClient : action,
        goalMessage : {
          task: 1
        }
      });

    goal.on('result', function(result) {
        for(x of result.active_modules){
            if(x == "/pt1/rc_visard_driver"){
                statusUI.pt1_camera_status = true;
            }
            else if(x === "/wetbay/rc_visard_driver"){
                statusUI.wetbay_camera_status = true;
            }
            else if(x === "/drybay/rc_visard_driver"){
                statusUI.drybay_camera_status = true;
            }
        }
        if (statusUI.pt1_camera_status){
            $("#pt1_camera_status").removeClass("badge-danger");
            $("#pt1_camera_status").addClass("badge-success");
        }
        else{
            $("#pt1_camera_status").removeClass("badge-success");
            $("#pt1_camera_status").addClass("badge-danger");
        }

        if (statusUI.wetbay_camera_status){
            $("#wetbay_camera_status").removeClass("badge-danger");
            $("#wetbay_camera_status").addClass("badge-success");
        }
        else{
            $("#wetbay_camera_status").removeClass("badge-success");
            $("#wetbay_camera_status").addClass("badge-danger");
        }

        if (statusUI.drybay_camera_status){
            $("#drybay_camera_status").removeClass("badge-danger");
            $("#drybay_camera_status").addClass("badge-success");
        }
        else{
            $("#drybay_camera_status").removeClass("badge-success");
            $("#drybay_camera_status").addClass("badge-danger");
        }
    });
    goal.send();
    setTimeout(updateUIStatus, 5000);
}

function cameraBtn(){
    $("#cameraBtn").removeClass("btn-danger");
    $("#cameraBtn").removeClass("btn-success");
    $("#cameraBtn").prop('disabled', true);
    $("#cameraBtn").html("WAIT");
    let camera = $("#cameraBtn").val();
    if (camera == "on"){
        
        var action = new ROSLIB.ActionClient({
            ros: ros,
            serverName: "/webui_handler",
            actionName: "roswebui/WebUIAction",
        });
        
        var goal = new ROSLIB.Goal({
            actionClient : action,
            goalMessage : {
              task: 3,
              module: 'camera'
            }
          });
    
        goal.on('result', function(result) {
            if (result.success){
                console.log("succesfully launched")
            }
        });
        goal.send();
        setTimeout(function() {
            $("#cameraBtn").prop('disabled', false);
            $("#cameraBtn").addClass("btn-success");
            $("#cameraBtn").attr({'value': 'off'});
            $("#cameraBtn").html('ON')
          }, 1000);
    }
    else{
        
        var action = new ROSLIB.ActionClient({
            ros: ros,
            serverName: "/webui_handler",
            actionName: "roswebui/WebUIAction",
        });
        
        var goal = new ROSLIB.Goal({
            actionClient : action,
            goalMessage : {
              task: 4,
              module: 'camera'
            }
          });
    
        goal.on('result', function(result) {
            if (result.success){
                console.log("succesfully killed")
            }
        });
        goal.send();
        setTimeout(function() {
            $("#cameraBtn").prop('disabled', false);
            $("#cameraBtn").addClass("btn-danger");
            $("#cameraBtn").attr({'value': 'on'});
            $("#cameraBtn").html('OFF')
          }, 1000);
    }
    console.log(camera)
    // $("#cameraBtn").toggleText('OFF', 'ON');
    
}

$(document).ready(function(){
    updateUIStatus();
    
  });
