#include <ros/ros.h>
#include <std_msgs/String.h>
#include <sstream>
using namespace std;

void callback(std_msgs::String msg){
    stringstream ss;
    ss << "roslaunch " << msg.data;
    cout << ss.str().c_str()<< endl;
    cout << "pid: " << system(ss.str().c_str()) << endl;
}

int main(int argc, char** argv){
    ros::init(argc,argv,"roslaunch_launcher");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("/launch", 1, callback);
    ros::spin();
    return 0;
}