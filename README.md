# Goldfinger UI

This UI consists of 2 components: Frontend and Backend
Frontend - coded with React.js
Backend - coded with Node.js and MongoDB
This UI adopted one of the most popular MERN stack.

### Frontend Setup

- (at root) npx install create-react-app front
- npm install bootstrap react-bootstrap react-router-dom axios (check package.json for more detail, react-router-dom must be at 5.3, else will be linking error)
- (check if port is set to 300)npm start

### Backend Setup

- npm init
- npm install nodemon cors mongoose express
- (check if port is set to 5000) nodemon server1.js

### Database Setup

- Install MongoDB Community
- Install MongoDB Compass
- Install mongosh
- (first time, create database) use mern_test
- (first time, create collection) db.instrument_set.insertOne({"model":"braum"})
- (first time, create some data at database for testing) db.instrument_set.insertOne(//paste from sample_database.json)
