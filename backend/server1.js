//import express
const express = require("express");
const app = express();
const port = 5000;

//cors
const cors = require("cors");
const corsOptions = {
  origin: "*",
  credentials: true, //access-control-allow-credentials:true
  optionSuccessStatus: 200,
};

app.use(cors(corsOptions));

//import bodyparser for json handling
const bodyParser = require("body-parser");
app.use(bodyParser.json());

//connect to database
const mongoose = require("mongoose");
mongoose.connect("mongodb://127.0.0.1:27017/mern_test", () => {
  console.log("connected to database");
});

//middleware
const simplifiedUIRouter = require("./routes/simplified_ui");
app.use("/api/simplified_ui", simplifiedUIRouter);

const systemControlRouter = require("./routes/system_control");
app.use("/api/system_control", systemControlRouter);

const errorRectificationRouter = require("./routes/error_rectification");
app.use("/api/error_rectification", errorRectificationRouter);

const inputConditionRouter = require("./routes/input_condition");
app.use("/api/input_condition", inputConditionRouter);

const wetbayDrybayPneumaticRouter = require("./routes/wetbay_drybay_pneumatic");
app.use("/api/wetbay_drybay_pneumatic", wetbayDrybayPneumaticRouter);

const instrumentSetRouter = require("./routes/instrument_set");
app.use("/api/instrument_set", instrumentSetRouter);

const consumablesRouter = require("./routes/consumables");
app.use("/api/consumables", consumablesRouter);

const sparesRouter = require("./routes/spares");
app.use("/api/spares", sparesRouter);

//routes
app.get("/", (req, res) => {
  console.log("good afernoon");
  res.status(200).send("good afternnon to you");
});

app.listen(port, () => {
  console.log(`listening at ${port}`);
});
