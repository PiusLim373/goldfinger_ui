const router = require("express").Router();
const instrument_set_model = require("../models/instruments_set.models");

router.get("/", async (req, res) => {
  try {
    const postHandler = await instrument_set_model.find();
    res.status(200).json(postHandler);
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.post("/", async (req, res) => {
  const instrument_set_template = {
    box_id: 0,
    model: "",
    input_time: 0,
    output_time: 0,
    status: "",
    content: {
      non_ring_instrument: {
        BAP: [],
        TCJ: [],
        FOR1: [],
        FOR2: [],
        SPR: [],
        RUL: [],
        RTL1: [],
        RTL2: [],
      },
      ring_instrument: {
        FAA: [],
        FAC: [],
        FSR: [],
        FTL: [],
        FTB: [],
        FTS: [],
        HNC: [],
        HNM: [],
        SDM: [],
        SDN: [],
        SCM: [],
      },
      gallipot: false,
      kidney_dish: false,
      wrapped_package: 0,
      printed_list: false,
      sticker_tag: false,
      indicator: false,
    },
    log: [],
  };
  let post_obj = instrument_set_template;
  let id = (await instrument_set_model.find()).length + 1;
  post_obj.model = req.body.model;
  post_obj.box_id = id;
  post_obj.input_time = new Date();
  post_obj.status = "processing";
  const instrument_to_process = new instrument_set_model(post_obj);
  try {
    const postHandler = await instrument_to_process.save();
    res.json({ box_id: post_obj.box_id });
  } catch (err) {
    res.status(400).json(err);
  }
});

router.get("/:id", async (req, res) => {
  try {
    const postHandler = await instrument_set_model.findOne({
      box_id: parseInt(req.params.id),
    });
    res.status(200).json(postHandler);
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.delete("/:id", async (req, res) => {
  try {
    const postHandler = await instrument_set_model.remove({
      box_id: parseInt(req.params.id),
    });
    res.status(200).json(postHandler);
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.put("/:id", async (req, res) => {
  try {
    data = req.body;
    if (Object.keys(data)[0] === "content") {
      let content_type = Object.keys(data["content"])[0];
      if (
        content_type === "non_ring_instrument" ||
        content_type === "ring_instrument"
      ) {
        //check if content is not non-ring or ring instrument                         //sample: data = {'content':{'kidney_dish': true}}
        res
          .status(400)
          .send(
            "operation not permitted, please send request to /api/(id)/non_ring_instrument"
          );
        return;
      } else {
        //check if content is of type ring or non ring isntrument, use $push    //sample: data = {'content':{'non_ring_instrument': {'TCJ': {'status':'comepleted'}}}}
        console.log("normal content");
        let key_str = "content." + Object.keys(data["content"])[0];
        let target = {
          [key_str]: data["content"][Object.keys(data["content"])[0]],
        };
        postHandler = await instrument_set_model.updateOne(
          { box_id: parseInt(req.params.id) },
          { $set: target }
        );
      }
    } else if (Object.keys(data)[0] === "status") {
      //sample: data = {'status':'completed'}
      if (data.status === "completed") {
        let target = {
          status: "completed",
          output_time: new Date(),
        };
        postHandler = await instrument_set_model.updateOne(
          { box_id: parseInt(req.params.id) },
          { $set: target }
        );
      }
    } else {
      //sample: data = {'output_time':123456}
      postHandler = await instrument_set_model.updateOne(
        { box_id: parseInt(req.params.id) },
        { $set: data }
      );
    }

    res.status(200).json({ postHandler });
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

// router.put("/:id/non_ring_instrument", async (req, res) => {
//   console.log("special content of non ring isntrument");
//   data = req.body;
//   instrument_type = Object.keys(data)[0];
//   let key_list = Object.keys(data[instrument_type]);

//   // for new non ring instrument creation
//   if (!key_list.includes("item_id")) {
//     console.log("no item_id is provided, item is new");
//     let id =
//       instrument_type +
//       String(
//         (
//           await instrument_set_model.findOne({
//             box_id: parseInt(req.params.id),
//           })
//         )["content"]["non_ring_instrument"][instrument_type].length + 1
//       );
//     const instrument_template = {
//       item_id: id,
//       inspection_result: "processing", //processing, missing, defective, ok
//       ai_output: [], //010_1641182568133_TCJ1_top.png
//     };
//     let poseObj = instrument_template;
//     let key_str = "content.non_ring_instrument." + instrument_type;
//     let target = { [key_str]: poseObj };
//     console.log(target);
//     poseObj["which_side"] = data[instrument_type]["which_side"];
//     postHandler = await instrument_set_model.updateOne(
//       { box_id: parseInt(req.params.id) },
//       { $push: target } //sample: {$push:{'content.non_ring_instrument.TCJ':{'status':'comepleted'}}}
//     );
//     res.status(200).json({ item_id: id });
//     return;
//   }

//   // for existing non ring instrument update
//   else {
//     console.log("updating existing non ring instrument");
//     let filter_key_str =
//       "content.non_ring_instrument." + instrument_type + ".item_id";
//     let filter = {
//       box_id: parseInt(req.params.id),
//       [filter_key_str]: data[instrument_type]["item_id"],
//     };

//     delete data[instrument_type]["item_id"];
//     let update_key = Object.keys(data[instrument_type])[0];
//     let key_str =
//       "content.non_ring_instrument." + instrument_type + ".$." + update_key;
//     let target = { [key_str]: data[instrument_type][update_key] };

//     console.log(filter);
//     console.log(target);

//     //use the push method to update the ai proof directory array
//     if (update_key === "ai_output") {
//       postHandler = await instrument_set_model.updateOne(
//         filter,
//         { $push: target } //push: {$set:{'content.non_ring_instrument.TCJ.$.ai_update':'/home/ur10controller/1.jpg'}}
//       );
//       if (postHandler["acknowledged"])
//         res.status(200).json({ message: "update successful" });
//       else res.status(400).json({ message: "update failed" });
//     }

//     //use the set method to modify the object key
//     else {
//       postHandler = await instrument_set_model.updateOne(
//         filter,
//         { $set: target } //sample: {$set:{'content.non_ring_instrument.TCJ.$.status':'completed'}}
//       );
//       if (postHandler["acknowledged"])
//         res.status(200).json({ message: "update successful" });
//       else res.status(400).json({ message: "update failed" });
//     }
//   }
// });
const non_ring_instrument_name_lib = {
  TCJ: "Towel Clip Jones",
  FOR1: "Forceps Mcindoe",
  FOR2: "Forceps Gillies",
  BAP: "Bard Parkers",
  SPR: "Spear Redivac",
  RUL: "Ruler Multipurposes",
  RTL1: "Retractors Langenback - Adult",
  RTL2: "Retractors Langenback - Baby",
};
router.put("/:id/non_ring_instrument", async (req, res) => {
  console.log("special content of non ring isntrument");
  data = req.body;
  let instrument_type = data["instrument_type"];
  key_list = Object.keys(data);

  // for new non ring instrument creation
  if (!key_list.includes("item_id")) {
    console.log("no item_id is provided, item is new");
    let id =
      instrument_type +
      "_" +
      String(
        (
          await instrument_set_model.findOne({
            box_id: parseInt(req.params.id),
          })
        )["content"]["non_ring_instrument"][instrument_type].length + 1
      );
    const instrument_template = {
      item_id: id,
      name: non_ring_instrument_name_lib[data["instrument_type"]],
      inspection_result: "processing", //processing, missing, pass, fail
      ai_output: [], //010_1641182568133_TCJ1_top.png
    };
    let poseObj = instrument_template;
    let key_str = "content.non_ring_instrument." + instrument_type;
    let target = { [key_str]: poseObj };
    console.log(target);
    postHandler = await instrument_set_model.updateOne(
      { box_id: parseInt(req.params.id) },
      { $push: target } //sample: {$push:{'content.non_ring_instrument.TCJ':{'inspection_result':'completed'}}}
    );
    res.status(200).json({ item_id: id });
    return;
  }

  // for existing non ring instrument update
  else {
    console.log("updating existing non ring instrument");
    let filter_key_str =
      "content.non_ring_instrument." + instrument_type + ".item_id";
    let filter = {
      box_id: parseInt(req.params.id),
      [filter_key_str]: data["item_id"],
    };
    let log_filter = {
      box_id: parseInt(req.params.id),
    };

    let item_id = data.item_id;
    delete data["item_id"];
    delete data["instrument_type"];
    let update_key = Object.keys(data)[0];
    let key_str =
      "content.non_ring_instrument." + instrument_type + ".$." + update_key;
    let target = { [key_str]: data[update_key] };

    console.log(filter);
    console.log(target);

    //use the push method to update the ai proof directory array
    if (update_key === "ai_output") {
      postHandler = await instrument_set_model.updateOne(
        filter,
        { $push: target } //push: {$set:{'content.non_ring_instrument.TCJ.$.ai_update':'/home/ur10controller/1.jpg'}}
      );
      if (postHandler["acknowledged"]) res.status(200).json({ success: true });
      else res.status(400).json({ success: false });
    }

    //use the set method to modify the object key
    else if (update_key === "inspection_result") {
      if (
        data.inspection_result === "missing" ||
        data.inspection_result === "fail"
      ) {
        let log_target = { log: `${item_id} ${data.inspection_result}` };
        console.log(log_target);
        postHandler = await instrument_set_model.updateOne(
          log_filter,
          { $push: log_target } //sample: {$set:{'content.non_ring_instrument.TCJ.$.inspection_result':'completed'}}
        );
      }
      postHandler = await instrument_set_model.updateOne(
        filter,
        { $set: target } //sample: {$set:{'content.non_ring_instrument.TCJ.$.inspection_result':'completed'}}
      );
      if (postHandler["acknowledged"]) res.status(200).json({ success: true });
      else res.status(400).json({ success: false });
    }
  }
});

// router.put("/:id/ring_instrument", async (req, res) => {
//   data = req.body;
//   console.log("special content which is ring or non ring isntrument");
//   instrument_type = Object.keys(data["content"][content_type])[0];
//   instrument_data = data["content"][content_type][instrument_type];
//   let key_str = "content." + content_type + "." + instrument_type;
//   let target = { [key_str]: instrument_data };
//   postHandler = await instrument_set_model.updateOne(
//     { box_id: parseInt(req.params.id) },
//     { $push: target } //sample: {$push:{'content.non_ring_instrument.TCJ':{'status':'comepleted'}}}
//   );
// });

const ring_instrument_name_lib = {
  FAA: "Forcep Artery Adson, Curved, 180mm",
  FAC: "Forcep Artery Crile, Curved, 140mm",
  FSR: "Forcep Sponge-Holding Rampley, Straight, 250mm",
  FTL: "Forcep Tissue Littlewood, 191mm",
  FTB: "Forcep Tissue Babcock 6', 150mm",
  FTS: "Forcep Tissues Stilles",
  HNC: "Holder Needle Crilewood, Tungstun Carbide, 150mm",
  HNM: "Holder Needle Mayo Hegar, Tungstun Carbide, 150mm, Jaw 19x3mm",
  SDM: "Scissor Dissecting Metzenbaum, curved, 180mm",
  SDN: "Scissor Dressing Nurses, Blunt, Sharp, Straight, 165mm",
  SCM: "Scissor Mayo, rounded, straight, 170mm",
};

router.put("/:id/ring_instrument", async (req, res) => {
  console.log("special content of ring isntrument");
  data = req.body;
  let instrument_type = data["instrument_type"];
  key_list = Object.keys(data);

  // for new non ring instrument creation
  if (!key_list.includes("item_id")) {
    console.log("no item_id is provided, item is new");
    let id =
      instrument_type +
      "_" +
      String(
        (
          await instrument_set_model.findOne({
            box_id: parseInt(req.params.id),
          })
        )["content"]["ring_instrument"][instrument_type].length + 1
      );
    const instrument_template = {
      item_id: id,
      name: ring_instrument_name_lib[data["instrument_type"]],
      inspection_result: "processing", //processing, missing, pass, fail
      ai_output: [], //010_1641182568133_TCJ1_top.png
    };
    let poseObj = instrument_template;
    let key_str = "content.ring_instrument." + instrument_type;
    let target = { [key_str]: poseObj };
    console.log(target);
    postHandler = await instrument_set_model.updateOne(
      { box_id: parseInt(req.params.id) },
      { $push: target } //sample: {$push:{'content.ring_instrument.FAA':{'inspection_result':'completed'}}}
    );
    res.status(200).json({ item_id: id });
    return;
  }

  // for existing non ring instrument update
  else {
    console.log("updating existing ring instrument");
    let filter_key_str =
      "content.ring_instrument." + instrument_type + ".item_id";
    let filter = {
      box_id: parseInt(req.params.id),
      [filter_key_str]: data["item_id"],
    };
    let log_filter = {
      box_id: parseInt(req.params.id),
    };

    let item_id = data.item_id;
    delete data["item_id"];
    delete data["instrument_type"];
    let update_key = Object.keys(data)[0];
    let key_str =
      "content.ring_instrument." + instrument_type + ".$." + update_key;
    let target = { [key_str]: data[update_key] };

    console.log(filter);
    console.log(target);

    //use the push method to update the ai proof directory array
    if (update_key === "ai_output") {
      postHandler = await instrument_set_model.updateOne(
        filter,
        { $push: target } //push: {$set:{'content.ring_instrument.FAA.$.ai_update':'/home/ur10controller/1.jpg'}}
      );
      if (postHandler["acknowledged"]) res.status(200).json({ success: true });
      else res.status(400).json({ success: false });
    }

    //use the set method to modify the object key
    else if (update_key === "inspection_result") {
      if (
        data.inspection_result === "missing" ||
        data.inspection_result === "fail"
      ) {
        let log_target = { log: `${item_id} ${data.inspection_result}` };
        console.log(log_target);
        postHandler = await instrument_set_model.updateOne(
          log_filter,
          { $push: log_target } //sample: {$set:{'content.non_ring_instrument.TCJ.$.inspection_result':'completed'}}
        );
      }
      postHandler = await instrument_set_model.updateOne(
        filter,
        { $set: target } //sample: {$set:{'content.ring_instrument.FAA.$.inspection_result':'completed'}}
      );
      if (postHandler["acknowledged"]) res.status(200).json({ success: true });
      else res.status(400).json({ success: false });
    }
  }
});

//############################################## debug template #####################
router.post("/debug_template", async (req, res) => {
  const post_obj = {
    box_id: 999,
    model: "braum",
    input_time: 1635701159953,
    output_time: 1636089593066,
    status: "completed",
    content: {
      non_ring_instrument: {
        BAP: [
          {
            item_id: "BAP_1",
            name: "Bard Parker",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_BAP_1_top.png",
              "999_1641182568134_BAP_1_top.png",
              "999_1641182568135_BAP_1_side.png",
              "999_1641182568136_BAP_1_front.png",
            ],
          },
          {
            item_id: "BAP_2",
            name: "Bard Parker",
            inspection_result: "fail",
            ai_output: [
              "999_1641182568137_BAP_2_top.png",
              "999_1641182568138_BAP_2_top.png",
              "999_1641182568139_BAP_2_side.png",
            ],
          },
        ],
        TCJ: [
          {
            item_id: "TCJ_1",
            name: "Towel Clip Jones",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_TCJ_1_top.png",
              "999_1641182568133_TCJ_1_top.png",
              "999_1641182568133_TCJ_1_front2.png",
            ],
          },
          {
            item_id: "TCJ_2",
            name: "Towel Clip Jones",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_TCJ_2_top.png",
              "999_1641182568133_TCJ_2_top.png",
              "999_1641182568133_TCJ_2_front.png",
            ],
          },
          {
            item_id: "TCJ_3",
            name: "Towel Clip Jones",
            inspection_result: "missing",
            ai_output: [],
          },
          {
            item_id: "TCJ_4",
            name: "Towel Clip Jones",
            inspection_result: "fail",
            ai_output: [
              ".png",
              "999_1641182568133_TCJ_4_top.png",
              "999_1641182568133_TCJ_4_front.png",
            ],
          },
          {
            item_id: "TCJ_5",
            name: "Towel Clip Jones",
            inspection_result: "fail",
            ai_output: ["999_1641182568133_TCJ_5_top.png"],
          },
          {
            item_id: "TCJ_6",
            name: "Towel Clip Jones",
            inspection_result: "missing",
            ai_output: [],
          },
        ],
        FOR1: [
          {
            item_id: "FOR1_1",
            name: "Forcep Mcindoe",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_FOR1_1_top.png",
              "999_1641182568133_FOR1_1_front.png",
            ],
          },
        ],
        FOR2: [
          {
            item_id: "FOR2_1",
            name: "Forcep Gillies",
            inspection_result: "fail",
            ai_output: ["999_1641182568133_FOR_2_top.png"],
          },
        ],
        SPR: [
          {
            item_id: "SPR_1",
            name: "Spear Redivac",
            inspection_result: "pass",
            ai_output: ["999_1641182568133_SPR_1_top.png"],
          },
        ],
        RUL: [
          {
            item_id: "RUL_1",
            name: "Ruler Multipurpose",
            inspection_result: "processing",
            ai_output: ["999_1641182568133_RUL_1_top.png"],
          },
        ],
        RTL1: [
          {
            item_id: "RTL1_1",
            name: "Retractor Langenback - Adult",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_RTL1_1_top.png",
              "999_1641182568133_RTL1_1_front.png",
            ],
          },
          {
            item_id: "RTL1_2",
            name: "Retractor Langenback - Adult",
            inspection_result: "missing",
            ai_output: [],
          },
        ],
        RTL2: [
          {
            item_id: "RTL2_1",
            name: "Retractor Langenback - Baby",
            inspection_result: "fail",
            ai_output: ["999_1641182568133_RTL2_1_top.png"],
          },
          {
            item_id: "RTL2_2",
            name: "Retractor Langenback - Baby",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_RTL2_2_top.png",
              "999_1641182568133_RTL2_2_front.png",
            ],
          },
        ],
      },
      ring_instrument: {
        FAA: [
          {
            item_id: "FAA_1",
            name: "Forcep Artery Adson, Curved, 180mm",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_FAA_1_top.png",
              "999_1641182568134_FAA_1_top.png",
              "999_1641182568135_FAA_1_side.png",
              "999_1641182568136_FAA_1_front.png",
            ],
          },
          {
            item_id: "FAA_2",
            name: "Forcep Artery Adson, Curved, 180mm",
            inspection_result: "fail",
            ai_output: [
              "999_1641182568137_FAA_2_top.png",
              "999_1641182568138_FAA_2_top.png",
              "999_1641182568139_FAA_2_side.png",
            ],
          },
          {
            item_id: "FAA_3",
            name: "Forcep Artery Adson, Curved, 180mm",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568137_FAA_3_top.png",
              "999_1641182568138_FAA_3_top.png",
              "999_1641182568139_FAA_3_side.png",
            ],
          },
        ],
        FAC: [
          {
            item_id: "FAC_1",
            name: "Forcep Artery Crile, Curved, 140mm",
            inspection_result: "pass",
            ai_output: [
              "999_1641182568133_FAC_1_top.png",
              "999_1641182568134_FAC_1_top.png",
              "999_1641182568135_FAC_1_side.png",
              "999_1641182568136_FAC_1_front.png",
            ],
          },
        ],
      },
      gallipot: true,
      kidney_dish: true,
      wrapped_package: 2,
      printed_list: true,
      sticker_tag: true,
      indicator: true,
    },
    log: [
      "BAP_2 defective",
      "TCJ_3 missing",
      "TCJ_4 defective",
      "TCJ_5 defective",
      "TCJ_6 missing",
      "FOR2_1 defective",
      "RTL1_2 defective",
      "RTL2_1 missing",
    ],
  };
  const instrument_to_process = new instrument_set_model(post_obj);
  try {
    const postHandler = await instrument_to_process.save();
    res.json(postHandler);
  } catch (err) {
    res.status(400).json(err);
  }
});

module.exports = router;
