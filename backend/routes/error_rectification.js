const router = require("express").Router();

var error = false;
var error_message = "";
var error_solved = false;

router.get("/", async (req, res) => {
  res.status(200).json({
    error: error,
    error_message: error_message,
    error_solved: error_solved,
  });
});

router.post("/", async (req, res) => {
  error = req.body.error;
  error_message = req.body.error_message;
  error_solved = false;
  res.status(200).json({
    message: "updated",
  });
});

router.get("/error_solved", async (req, res) => {
  error_solved = true;
  error = false;
  error_message = "";
  res.status(200).json({
    message: "updated",
  });
});

module.exports = router;
