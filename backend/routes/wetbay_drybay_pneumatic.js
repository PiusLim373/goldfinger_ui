const router = require("express").Router();

const rosnodejs = require("rosnodejs");

rosnodejs.initNode("/rosnodejs");
const nh = rosnodejs.nh;
const webui_action_client = new rosnodejs.SimpleActionClient({
  nh,
  type: "roswebui/WebUI",
  actionServer: "/webui_handler",
});

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

router.get("/", async (req, res) => {
  //query plc to check for input condition
  try {
    var timeOut = true;
    webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
      timeOut = false;
      const goal = {
        task: 5,
        module: "plc",
        arg1: "wetbay_drybay_pneumatic",
        arg2: [],
      };
      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          var ac_result = webui_action_client.getResult();
          if (ac_result.success === true) {
            console.log(ac_result.input_condition);
            res.status(200).json({
              success: true,
              message: "",
              wetbay_pneumatic: ac_result.input_condition.splice(0, 6),
              drybay_pneumatic: ac_result.input_condition.splice(0, 6),
            });
          } else {
            res.status(200).json({
              success: false,
              message: "Error during input condition acquicision",
            });
          }
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  } catch (err) {
    console.log("debug return for input condition");
    var wetbay = [0, 0, 0, 0, 0, 0];
    var drybay = [0, 0, 0, 0, 0, 0];

    res
      .status(200)
      .json({ wetbay_pneumatic: wetbay, drybay_pneumatic: drybay });
  }
});

router.post("/", async (req, res) => {
  try {
    var timeOut = true;
    webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
      timeOut = false;
      const goal = {
        task: 5,
        module: "plc",
        arg1: "pneumatic_toggle_" + req.body.data,
        arg2: [],
      };
      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          var ac_result = webui_action_client.getResult();
          if (ac_result.success === true) {
            res.status(200).json({
              success: true,
              message: "",
            });
          } else {
            res.status(200).json({
              success: false,
              message: ac_result.message,
            });
          }
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  } catch (err) {
    res.status(400).json({ message: "err" });
  }
});
// router.get("/reset_led", async (req, res) => {
//   //query plc to check for input condition
//   var timeOut = true;
//   webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
//     timeOut = false;
//     const goal = { task: 5, module: "plc", arg1: "reset_led", arg2: [] };
//     webui_action_client
//       .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
//       .then((finished) => {
//         var ac_result = webui_action_client.getResult();
//         if (ac_result.success === true) {
//           console.log(ac_result.input_condition);
//           res.status(200).json({
//             success: true,
//           });
//         } else {
//           res.status(200).json({
//             success: false,
//             message: "Error during input condition acquicision",
//           });
//         }
//       });
//   });
//   await sleep(1000);
//   if (timeOut) res.status(400).json({ message: "timeout" });
// });

// router.get("/debug", async (req, res) => {
//   //query plc to check for input condition

//   console.log("debug return for input condition");
//   var wetbay = [0, 0, 0, 0, 1, 1];
//   var drybay = [11, 10, 11, 11, 10, 11];

//   res.status(200).json({
//     success: true,
//     message: "",
//     wetbay_input: wetbay,
//     drybay_input: drybay,
//   });
// });

module.exports = router;
