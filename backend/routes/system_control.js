const router = require("express").Router();
const rosnodejs = require("rosnodejs");
const { shutdown } = require("rosnodejs/dist/lib/ThisNode");

rosnodejs.initNode("/rosnodejs");
const nh = rosnodejs.nh;
const webui_action_client = new rosnodejs.SimpleActionClient({
  nh,
  type: "roswebui/WebUI",
  actionServer: "/webui_handler",
});

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

var main_program_status = false;
var main_program_message = "";
var main_program_error_message = false;

var boot_or_fix_in_progress = false;
var shutdown_in_progress = false;

router.post("/main_program_message", async (req, res) => {
  main_program_message = req.body.main_program_message;
  main_program_error_message = req.body.main_program_error_message;
  res.status(200).json({ message: "message_received" });
});

router.get("/main_program_message", async (req, res) => {
  res.status(200).json({
    main_program_status: main_program_status,
    main_program_error_message: main_program_error_message,
    main_program_message: main_program_message,
  });
});

router.get("/main_program_status", async (req, res) => {
  try {
    var timeOut = true;
    webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
      timeOut = false;
      const goal = { task: 2, module: "/program_manager", arg1: "", arg2: [] };
      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          var ac_result = webui_action_client.getResult();
          if (ac_result.success === true) {
            main_program_status = true;
            res.status(200).json({ main_program_status: main_program_status });
          } else {
            main_program_status = false;
            res.status(200).json({ main_program_status: false });
          }
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  } catch (err) {
    // res.status(500).json({ message: err });

    res.status(200).json({ main_program_status: main_program_status });
  }
});

router.get("/main_operation_page_quick_check", async (req, res) => {
  const goal = { task: 6, module: "", arg1: "", arg2: [] }; //task 6 == MAIN_OPERATION_PAGE_QUICK_CHECK
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 3, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
        if (webui_action_client.getResult().success) {
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
            problematic_module:
              webui_action_client.getResult().problematic_modules,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.get("/bootup_shutdown_checker", async (req, res) => {
  res.status(200).json({
    bootup_in_progress: boot_or_fix_in_progress,
    shutdown_in_progress: shutdown_in_progress,
  });
});

router.get("/bootup_or_fix", async (req, res) => {
  if (boot_or_fix_in_progress) {
    res.status(200).json({
      success: false,
      message: "previous_progress_not_completed",
    });
  } else {
    boot_or_fix_in_progress = true;
    const goal = { task: 31, module: "", arg1: "", arg2: [] }; //task 31 == BOOTUP_OR_FIX
    console.log(goal);

    var timeOut = true;
    webui_action_client.waitForServer({ secs: 3, nsecs: 0 }).then(() => {
      timeOut = false;

      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          console.log(webui_action_client.getResult());
          if (webui_action_client.getResult().success) {
            res.status(200).json({
              success: true,
              message: webui_action_client.getResult().message,
            });
          } else {
            res.status(200).json({
              success: false,
              message: webui_action_client.getResult().message,
              problematic_module:
                webui_action_client.getResult().problematic_modules,
            });
          }
          boot_or_fix_in_progress = false;
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  }
});

router.get("/shutdown", async (req, res) => {
  shutdown_in_progress = true;
  res.status(200).json({ message: "shutdown" });

  const goal = { task: 41, module: "", arg1: "", arg2: [] }; //task 41 == SHUTDOWN
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.get("/kill/:node", async (req, res) => {
  //for node initializing//;
  console.log(`killing ${req.params.node}`);
  const goal = { task: 4, module: req.params.node, arg1: "", arg2: [] }; //task 4 == KILL
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        if (webui_action_client.getResult().success) {
          if (req.params.node === "program_manager") {
            main_program_status = false;
            main_program_message = "";
            main_program_error_message = false;
          }
          console.log(`signalled to kill ${req.params.node}`);
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.post("/program_manager", async (req, res) => {
  var pigeonhole_to_process = req.body.pigeonhole_to_process;

  const goal = {
    task: 3,
    module: "program_manager",
    arg1: "",
    arg2: pigeonhole_to_process,
  };
  console.log(goal);
  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
        if (webui_action_client.getResult().success) {
          main_program_status = true;
          console.log(`successfully launched program manager`);
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

module.exports = router;
