const router = require("express").Router();
const non_ring_instrument_model = require("../models/non_ring.models");

const consumables_module_hashmap = {};

var non_ring_ib_ui_state = {
  ui_state: "loading",
  tool_selected: "",
};

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

// const rosnodejs = require("rosnodejs");

// rosnodejs.initNode("/rosnodejs");
// const nh = rosnodejs.nh;
// const webui_action_client = new rosnodejs.SimpleActionClient({
//   nh,
//   type: "roswebui/WebUI",
//   actionServer: "/webui_handler",
// });

router.get("/", async (req, res) => {
  res.status(200).json(non_ring_ib_ui_state);
});

router.get("/belt_width/:which_tool", async (req, res) => {
  //ROS action call to ib server to change belt width
  console.log(`belt width adjusted for ${req.params.which_tool}`)
  try {
    res.status(200).json({ success: true, message: `belt width adjusted for ${req.params.which_tool}` });
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.get("/start_inspection", async (req, res) => {
  //ROS action call to start inspection

  console.log("starting inspection");
  non_ring_ib_ui_state.ui_state = "inspecting";
  await sleep(3000);
  non_ring_ib_ui_state.ui_state = "finished";
  res.status(200).json({ success: true });
});

// router.get("/database", async (req, res) => {
//   try {
//     const postHandler = await consumables_model.find();
//     var hashmap = {};
//     for (const con of postHandler) {
//       hashmap[con.codename] = con.name + " @ " + con.location;
//     }
//     res.status(200).json({
//       consumables: postHandler,
//       consumables_ui_state: consumables_ui_state,
//       consumable_hashmap: hashmap,
//     });
//   } catch (err) {
//     res.status(400).json({ message: err });
//   }
// });
/////////////////////////////////////////////////////////////// debug template
router.post("/debug_template", async (req, res) => {
  const debug_template = [
    {
      name: "Wrapping Paper",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Printer Bay",
      codename: "wppb",
    },
    
  ];

  try {
    const postHandler = await consumables_model.insertMany(debug_template);
    res.json(postHandler);
  } catch (err) {
    res.status(400).json(err);
  }
});
module.exports = router;
