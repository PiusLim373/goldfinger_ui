const router = require("express").Router();
const rosnodejs = require("rosnodejs");

rosnodejs.initNode("/rosnodejs");
const nh = rosnodejs.nh;
const webui_action_client = new rosnodejs.SimpleActionClient({
  nh,
  type: "roswebui/WebUI",
  actionServer: "/webui_handler",
});

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

router.get("/ip_check", async (req, res) => {
  //for checking active rosnode//;
  try {
    var timeOut = true;
    webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
      timeOut = false;
      const goal = { task: 6, module: "", arg1: "", arg2: [] };
      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          var ac_result = webui_action_client.getResult();
          console.log(ac_result);
          res.status(200).json({ problematic_modules: ac_result.problematic_modules });
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  } catch (err) {
    res.status(500).json({ message: err });
  }
});


router.get("/", async (req, res) => {
  //for checking active rosnode//;
  try {
    var timeOut = true;
    webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
      timeOut = false;
      const goal = { task: 1, module: "", arg1: "", arg2: [] };
      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          var ac_result = webui_action_client.getResult();
          console.log(ac_result);
          var module_status = {
            plc_status: false,
            wetbay_camera_status: false,
            drybay_camera_status: false,
            pt1_camera_status: false,
            pt2_camera_status: false,
            ai_status: false,
            pcl_status: false,
            ur10_status: false,
            ur5_status: false,
            nrib_status: false,
            ur10_robot_status: ac_result.ur10_robot_status,
            ur10_program_status: ac_result.ur10_program_status,
            main_program_status: false,
          };
          for (var active_module of ac_result.active_modules) {
            if (active_module === "/plc_server")
              module_status.plc_status = true;
            else if (active_module === "/wetbay/rc_visard_driver")
              module_status.wetbay_camera_status = true;
            else if (active_module === "/drybay/rc_visard_driver")
              module_status.drybay_camera_status = true;
            else if (active_module === "/pt1/rc_visard_driver")
              module_status.pt1_camera_status = true;
            else if (active_module === "/pt2/rc_visard_driver")
              module_status.pt2_camera_status = true;
            else if (active_module === "/ai_server")
              module_status.ai_status = true;
            else if (active_module === "/pcl_server")
              module_status.pcl_status = true;
            else if (active_module === "/motion_server")
              module_status.ur10_status = true;
            else if (active_module === "/program_manager") {
              module_status.main_program_status = true;
              main_program_status = true;
            } else if (active_module === "/ur5_program_manager")
              module_status.ur5_status = true;
            else if (active_module === "/ib1_server")
              module_status.nrib_status = true;
          }

          res.status(200).json(module_status);
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  } catch (err) {
    // res.status(500).json({ message: err });

    console.log("debug return");
    var module_status = {
      plc_status: true,
      wetbay_camera_status: true,
      drybay_camera_status: true,
      pt1_camera_status: true,
      pt2_camera_status: true,
      ai_status: true,
      pcl_status: true,
      ur10_status: true,
      ur5_status: true,
      ur10_robot_status: "RUNNING",
      ur10_program_status: "PLAYING",
      main_program_status: main_program_status,
    };
    res.status(200).json(module_status);
  }
});


module.exports = router;
