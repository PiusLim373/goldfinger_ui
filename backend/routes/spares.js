const router = require("express").Router();
const non_ring_spares_model = require("../models/non_ring_spares.models");
const ring_spares_model = require("../models/ring_spares.models");

var spares_ui_state = {
  refill_state: "prompting",
  which_spares: "",
};

var non_ring_spare_bay_instrument = [
  "tcj",
  "rla",
  "rlb",
  "rul",
  "spr",
  "fdm",
  "fdg",
  "bap",
];
var ring_spare_bay_instrument = [
  "faa",
  "fac",
  "fsr",
  "ftl",
  "ftb",
  "fts",
  "hnc",
  "hnm",
  "sdm",
  "sdn",
  "scm",
];

// const rosnodejs = require("rosnodejs");

// rosnodejs.initNode("/rosnodejs");
// const nh = rosnodejs.nh;
// const webui_action_client = new rosnodejs.SimpleActionClient({
//   nh,
//   type: "roswebui/WebUI",
//   actionServer: "/webui_handler",
// });

router.get("/", async (req, res) => {
  try {
    const non_ring_postHandler = await non_ring_spares_model.find();
    const ring_postHandler = await ring_spares_model.find();
    var hashmap = {};
    for (const nri of non_ring_postHandler) {
      hashmap[nri.codename] = nri.name + " @ " + nri.location + " Spare Bay";
    }
    for (const ri of ring_postHandler) {
      hashmap[ri.codename] = ri.name + " @ " + ri.location + " Spare Bay";
    }

    res.status(200).json({
      non_ring_spares: non_ring_postHandler,
      ring_spares: ring_postHandler,
      spares_ui_state: spares_ui_state,
      spares_hashmap: hashmap,
    });
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.get("/consume/:ring_or_nonring/:which_spares", async (req, res) => {
  try {
    if (req.params.ring_or_nonring === "non_ring") {
      postHandler = await non_ring_spares_model.updateOne(
        { codename: req.params.which_spares },
        { $inc: { remaining: -1 } }
      );
    } else if (req.params.ring_or_nonring === "ring") {
      postHandler = await ring_spares_model.updateOne(
        { codename: req.params.which_spares },
        { $inc: { remaining: -1 } }
      );
    } else {
      throw "invalid option";
    }
    res.status(200).json({
      message: `${req.params.which_spares} updated successfully`,
    });
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.get("/unlock_door/:which_spares", async (req, res) => {
  //ROS action call to unlock module door code here
  if (non_ring_spare_bay_instrument.includes(req.params.which_spares))
    which_spare_bay = "non_ring";
  else if (ring_spare_bay_instrument.includes(req.params.which_spares))
    which_spare_bay = "ring";
  console.log(
    `pinging ${which_spare_bay} Spare Bay to check to unlock door for refilling`
  );
  spares_ui_state.refill_state = "refilling";
  spares_ui_state.which_spares = req.params.which_spares;
  res.status(200).json({ success: true });
});

router.get("/check_door/:which_spares", async (req, res) => {
  //ROS action call to check if module door is locked properly code here
  if (non_ring_spare_bay_instrument.includes(req.params.which_spares))
    which_spare_bay = "non_ring";
  else if (ring_spare_bay_instrument.includes(req.params.which_spares))
    which_spare_bay = "ring";
  console.log(
    `pinging ${which_spare_bay} Spare Bay to check if door is locked properly`
  );
  res.status(200).json({ success: true });
});

router.get("/refill/:which_spares", async (req, res) => {
  try {
    if (non_ring_spare_bay_instrument.includes(req.params.which_spares))
      which_spare_bay = "non_ring";
    else if (ring_spare_bay_instrument.includes(req.params.which_spares))
      which_spare_bay = "ring";
    if (which_spare_bay === "non_ring") {
      found = await non_ring_spares_model.findOne({
        codename: req.params.which_spares,
      });
      capacity = found.capacity;
      postHandler = await non_ring_spares_model.updateOne(
        { codename: req.params.which_spares },
        { $set: { remaining: capacity, time_last_refill: new Date() } }
      );
      spares_ui_state.refill_state = "prompting";
      spares_ui_state.which_spares = "";
    } else if (which_spare_bay === "ring") {
      found = await ring_spares_model.findOne({
        codename: req.params.which_spares,
      });
      capacity = found.capacity;
      postHandler = await ring_spares_model.updateOne(
        { codename: req.params.which_spares },
        { $set: { remaining: capacity, time_last_refill: new Date() } }
      );
      spares_ui_state.refill_state = "prompting";
      spares_ui_state.which_spares = "";
    } else {
      throw "invalid option";
    }
    res.status(200).json({
      message: `${req.params.which_spares} updated successfully`,
    });
  } catch (err) {
    res.status(400).json({ message: "error" });
  }
});

/////////////////////////////////////////////////////////////// debug template
router.post("/non_ring/debug_template", async (req, res) => {
  const debug_template = [
    {
      name: "Towel Clip Jones",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "tcj",
    },
    {
      name: "Retractor Langenback - Adult",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "rla",
    },
    {
      name: "Retractor Langenback - Baby",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "rlb",
    },
    {
      name: "Ruler Multipurpose",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "rul",
    },
    {
      name: "Spear Redivac",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "spr",
    },
    {
      name: "Forcep Dissecting  Mcindoe",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "fdm",
    },
    {
      name: "Forcep Dissecting  Gillies",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "fdg",
    },
    {
      name: "Bard Parker",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "bap",
    },
  ];

  try {
    const postHandler = await non_ring_spares_model.insertMany(debug_template);
    res.json(postHandler);
  } catch (err) {
    res.status(400).json(err);
  }
});

router.post("/ring/debug_template", async (req, res) => {
  const debug_template = [
    {
      name: "Forcep Artery Adson, Curved, 180mm",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 12,
      location: "Non-Ring",
      codename: "faa",
    },
    {
      name: "Forcep Artery Crile, Curved, 140mm",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "fac",
    },
    {
      name: "Forcep Sponge-Holding Rampley, Straight, 250mm",
      time_last_refill: 1635701159953,
      remaining: 1,
      capacity: 6,
      location: "Non-Ring",
      codename: "fsr",
    },
    {
      name: "Forcep Tissue Littlewood, 191mm",
      time_last_refill: 1635701159953,
      remaining: 1,
      capacity: 6,
      location: "Non-Ring",
      codename: "ftl",
    },
    {
      name: "Forcep Tissue Babcock 6', 150mm",
      time_last_refill: 1635701159953,
      remaining: 3,
      capacity: 6,
      location: "Non-Ring",
      codename: "ftb",
    },
    {
      name: "Forcep Tissues Stilles",
      time_last_refill: 1635701159953,
      remaining: 1,
      capacity: 6,
      location: "Non-Ring",
      codename: "fts",
    },
    {
      name: "Holder Needle Crilewood, Tungstun Carbide, 150mm",
      time_last_refill: 1635701159953,
      remaining: 4,
      capacity: 6,
      location: "Non-Ring",
      codename: "hnc",
    },
    {
      name: "Holder Needle Mayo Hegar, Tungstun Carbide, 150mm, Jaw 19x3mm",
      time_last_refill: 1635701159953,
      remaining: 6,
      capacity: 6,
      location: "Non-Ring",
      codename: "hnm",
    },
    {
      name: "Scissor Dissecting Metzenbaum, curved, 180mm",
      time_last_refill: 1635701159953,
      remaining: 6,
      capacity: 6,
      location: "Non-Ring",
      codename: "sdm",
    },
    {
      name: "Scissor Dressing Nurses, Blunt, Sharp, Straight, 165mm",
      time_last_refill: 1635701159953,
      remaining: 6,
      capacity: 6,
      location: "Non-Ring",
      codename: "sdn",
    },
    {
      name: "Scissor Mayo, rounded, straight, 170mm",
      time_last_refill: 1635701159953,
      remaining: 5,
      capacity: 6,
      location: "Non-Ring",
      codename: "scm",
    },
  ];

  try {
    const postHandler = await ring_spares_model.insertMany(debug_template);
    res.json(postHandler);
  } catch (err) {
    res.status(400).json(err);
  }
});
module.exports = router;
