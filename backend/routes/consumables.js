const router = require("express").Router();
const consumables_model = require("../models/consumables.models");

const consumables_module_hashmap = {
  wppb: "printer_bay_server",
  a4pb: "printer_bay_server",
  wpwb: "wrapping_bay_server",
  stwb: "wrapping_bay_server",
  cmib: "ring_ib_server",
  luib: "ring_ib_server",
  inid: "indicator_server",
  tptb: "tray_paper_server",
};

var consumables_ui_state = {
  refill_state: "prompting",
  which_consumables: "",
};

// const rosnodejs = require("rosnodejs");

// rosnodejs.initNode("/rosnodejs");
// const nh = rosnodejs.nh;
// const webui_action_client = new rosnodejs.SimpleActionClient({
//   nh,
//   type: "roswebui/WebUI",
//   actionServer: "/webui_handler",
// });

router.get("/", async (req, res) => {
  try {
    const postHandler = await consumables_model.find();
    var hashmap = {};
    for (const con of postHandler) {
      hashmap[con.codename] = con.name + " @ " + con.location;
    }
    res.status(200).json({
      consumables: postHandler,
      consumables_ui_state: consumables_ui_state,
      consumable_hashmap: hashmap,
    });
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.get("/consume/:which_consumables", async (req, res) => {
  try {
    found = await consumables_model.findOne({
      codename: req.params.which_consumables,
    });
    rate_of_consumption = found.rate_of_consumption;
    postHandler = await consumables_model.updateOne(
      { codename: req.params.which_consumables },
      { $inc: { remaining: rate_of_consumption * -1 } }
    );
    res.status(200).json({
      message: `${req.params.which_consumables} updated successfully`,
    });
  } catch (err) {
    res.status(400).json({ message: err });
  }
});

router.get("/unlock_door/:which_consumables", async (req, res) => {
  //ROS action call to unlock module door code here
  console.log(
    `pinging ${
      consumables_module_hashmap[req.params.which_consumables]
    } to unlock door for consumables refilling`
  );
  consumables_ui_state.refill_state = "refilling";
  consumables_ui_state.which_consumables = req.params.which_consumables;
  res.status(200).json({ success: true });
});

router.get("/check_door/:which_consumables", async (req, res) => {
  //ROS action call to check if module door is locked properly code here
  console.log(
    `pinging ${
      consumables_module_hashmap[req.params.which_consumables]
    } to check if door is locked properly`
  );
  res.status(200).json({ success: true });
});

router.get("/refill/:which_consumables", async (req, res) => {
  try {
    postHandler = await consumables_model.updateOne(
      { codename: req.params.which_consumables },
      { $set: { remaining: 100, time_last_refill: new Date() } }
    );
    consumables_ui_state.refill_state = "prompting";
    consumables_ui_state.which_consumables = "";
    res.status(200).json({
      message: `${req.params.which_consumables} updated successfully`,
    });
  } catch (err) {
    res.status(400).json({ message: "error" });
  }
});

/////////////////////////////////////////////////////////////// debug template
router.post("/debug_template", async (req, res) => {
  const debug_template = [
    {
      name: "Wrapping Paper",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Printer Bay",
      codename: "wppb",
    },
    {
      name: "A4 Paper",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Printer Bay",
      codename: "a4pb",
    },
    {
      name: "Wrapping Paper",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Wrapping Bay",
      codename: "wpwb",
    },
    {
      name: "3M Steam Tape",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Wrapping Bay",
      codename: "stwb",
    },
    {
      name: "Cutting Test Material",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Ring Inspection Bay",
      codename: "cmib",
    },
    {
      name: "Ring Instrument Lubricant",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Ring Inspection Bay",
      codename: "luib",
    },
    {
      name: "Steam Indicator",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Indicator Dispenser",
      codename: "inid",
    },
    {
      name: "Tray Paper",
      time_last_refill: 1635701159953,
      rate_of_consumption: 2,
      remaining: 57,
      location: "Tray Paper Bay",
      codename: "tptb",
    },
  ];

  try {
    const postHandler = await consumables_model.insertMany(debug_template);
    res.json(postHandler);
  } catch (err) {
    res.status(400).json(err);
  }
});
module.exports = router;
