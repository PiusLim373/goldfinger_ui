const router = require("express").Router();
const rosnodejs = require("rosnodejs");
const { shutdown } = require("rosnodejs/dist/lib/ThisNode");

rosnodejs.initNode("/rosnodejs");
const nh = rosnodejs.nh;
const webui_action_client = new rosnodejs.SimpleActionClient({
  nh,
  type: "roswebui/WebUI",
  actionServer: "/webui_handler",
});

function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

var main_program_status = false;
var main_program_message = "";
var main_program_error_message = false;

var boot_or_fix_in_progress = false;
var shutdown_in_progress = false;

router.get("/", async (req, res) => {
  //for checking active rosnode//;
  try {
    var timeOut = true;
    webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
      timeOut = false;
      const goal = { task: 1, module: "", arg1: "", arg2: [] };
      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          var ac_result = webui_action_client.getResult();
          console.log(ac_result);
          var module_status = {
            plc_status: false,
            wetbay_camera_status: false,
            drybay_camera_status: false,
            pt1_camera_status: false,
            pt2_camera_status: false,
            ai_status: false,
            pcl_status: false,
            ur10_status: false,
            ur5_status: false,
            nrib_status: false,
            ur10_robot_status: ac_result.ur10_robot_status,
            ur10_program_status: ac_result.ur10_program_status,
            main_program_status: false,
          };
          for (var active_module of ac_result.active_modules) {
            if (active_module === "/plc_server")
              module_status.plc_status = true;
            else if (active_module === "/wetbay/rc_visard_driver")
              module_status.wetbay_camera_status = true;
            else if (active_module === "/drybay/rc_visard_driver")
              module_status.drybay_camera_status = true;
            else if (active_module === "/pt1/rc_visard_driver")
              module_status.pt1_camera_status = true;
            else if (active_module === "/pt2/rc_visard_driver")
              module_status.pt2_camera_status = true;
            else if (active_module === "/ai_server")
              module_status.ai_status = true;
            else if (active_module === "/pcl_server")
              module_status.pcl_status = true;
            else if (active_module === "/motion_server")
              module_status.ur10_status = true;
            else if (active_module === "/program_manager") {
              module_status.main_program_status = true;
              main_program_status = true;
            } else if (active_module === "/ur5_program_manager")
              module_status.ur5_status = true;
            else if (active_module === "/ib1_server")
              module_status.nrib_status = true;
          }

          res.status(200).json(module_status);
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  } catch (err) {
    // res.status(500).json({ message: err });

    console.log("debug return");
    var module_status = {
      plc_status: true,
      wetbay_camera_status: true,
      drybay_camera_status: true,
      pt1_camera_status: true,
      pt2_camera_status: true,
      ai_status: true,
      pcl_status: true,
      ur10_status: true,
      ur5_status: true,
      ur10_robot_status: "RUNNING",
      ur10_program_status: "PLAYING",
      main_program_status: main_program_status,
    };
    res.status(200).json(module_status);
  }
});

router.post("/main_program_message", async (req, res) => {
  main_program_message = req.body.main_program_message;
  main_program_error_message = req.body.main_program_error_message;
  res.status(200).json({ message: "message_received" });
});

router.get("/main_program_message", async (req, res) => {
  res.status(200).json({
    main_program_status: main_program_status,
    main_program_error_message: main_program_error_message,
    main_program_message: main_program_message,
  });
});

router.get("/main_program_status", async (req, res) => {
  try {
    var timeOut = true;
    webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
      timeOut = false;
      const goal = { task: 2, module: "/program_manager", arg1: "", arg2: [] };
      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          var ac_result = webui_action_client.getResult();
          if (ac_result.success === true) {
            main_program_status = true;
            res.status(200).json({ main_program_status: main_program_status });
          } else {
            main_program_status = false;
            res.status(200).json({ main_program_status: false });
          }
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  } catch (err) {
    // res.status(500).json({ message: err });

    res.status(200).json({ main_program_status: main_program_status });
  }
});

router.get("/main_operation_page_quick_check", async (req, res) => {
  const goal = { task: 6, module: "", arg1: "", arg2: [] }; //task 6 == MAIN_OPERATION_PAGE_QUICK_CHECK
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 3, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
        if (webui_action_client.getResult().success) {
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
            problematic_module : webui_action_client.getResult().problematic_modules,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.get("/bootup_or_fix_checker", async (req, res) => {
  res.status(200).json({bootup_in_progress: boot_or_fix_in_progress});
});

router.get("/bootup_or_fix", async (req, res) => {
  if(boot_or_fix_in_progress){
    res.status(200).json({
      success: false,
      message: "previous_progress_not_completed",
    });
  }
  else{
    boot_or_fix_in_progress= true;
    const goal = { task: 31, module: "", arg1: "", arg2: [] }; //task 31 == BOOTUP_OR_FIX
    console.log(goal);

    var timeOut = true;
    webui_action_client.waitForServer({ secs: 3, nsecs: 0 }).then(() => {
      timeOut = false;

      webui_action_client
        .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
        .then((finished) => {
          console.log(webui_action_client.getResult());
          if (webui_action_client.getResult().success) {
            res.status(200).json({
              success: true,
              message: webui_action_client.getResult().message,
            });
          } else {
            res.status(200).json({
              success: false,
              message: webui_action_client.getResult().message,
              problematic_module : webui_action_client.getResult().problematic_modules,
            });
          }
          boot_or_fix_in_progress = false;
        });
    });
    await sleep(1000);
    if (timeOut) res.status(400).json({ message: "timeout" });
  }
  
});

router.get("/shutdown_checker", async (req, res) => {
  res.status(200).json({shutdown_in_progress: shutdown_in_progress});
});

router.get("/shutdown", async (req, res) => {
  shutdown_in_progress = true;
  res.status(200).json({message: "shutdown"});
  const goal = { task: 41, module: req.params.node, arg1: "", arg2: [] }; //task 3 == SHUTDOWN
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
        if (webui_action_client.getResult().success) {
          console.log(`signalled to boot ${req.params.node}`);
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.get("/boot/:node", async (req, res) => {
  //for node initializing//;
  console.log(`booting ${req.params.node}`);
  const goal = { task: 3, module: req.params.node, arg1: "", arg2: [] }; //task 3 == LAUNCH
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
        if (webui_action_client.getResult().success) {
          console.log(`signalled to boot ${req.params.node}`);
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.get("/kill/:node", async (req, res) => {
  //for node initializing//;
  console.log(`killing ${req.params.node}`);
  const goal = { task: 4, module: req.params.node, arg1: "", arg2: [] }; //task 4 == KILL
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        if (webui_action_client.getResult().success) {
          if (req.params.node === "program_manager")
            {main_program_status = false;
              main_program_message = "";
              main_program_error_message = false;
            }
          console.log(`signalled to kill ${req.params.node}`);
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.get("/:change_program_status", async (req, res) => {
  console.log(`request to ${req.params.change_program_status} program`);
  const goal = {
    task: 5,
    module: "ur10",
    arg1: req.params.change_program_status, //pause_program or continue_program
    arg2: [],
  }; //task 5 == ROUTER
  console.log(goal);

  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
        if (webui_action_client.getResult().success) {
          console.log(
            `successfully ${req.params.change_program_status} program`
          );
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

router.post("/program_manager", async (req, res) => {
  var pigeonhole_to_process = req.body.pigeonhole_to_process;

  const goal = {
    task: 3,
    module: "program_manager",
    arg1: "",
    arg2: pigeonhole_to_process,
  };
  console.log(goal);
  var timeOut = true;
  webui_action_client.waitForServer({ secs: 1, nsecs: 0 }).then(() => {
    timeOut = false;

    webui_action_client
      .sendGoalAndWait(goal, { secs: 0, nsecs: 0 }, { secs: 0, nsecs: 0 })
      .then((finished) => {
        console.log(webui_action_client.getResult());
        if (webui_action_client.getResult().success) {
          main_program_status = true;
          console.log(`successfully launched program manager`);
          res.status(200).json({
            success: true,
            message: webui_action_client.getResult().message,
          });
        } else {
          res.status(200).json({
            success: false,
            message: webui_action_client.getResult().message,
          });
        }
      });
  });
  await sleep(1000);
  if (timeOut) res.status(400).json({ message: "timeout" });
});

module.exports = router;
