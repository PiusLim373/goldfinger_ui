const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ring_sparesScheme = new Schema(
  {
    name: {
      type: String,
    },
    time_last_refill: {
      type: Number,
    },
    remaining: {
      type: Number,
    },
    capacity: {
      type: Number,
    },
    location: {
      type: String,
    },
    codename: {
      type: String,
    },
  },
  { strict: false }
);

// const noSchema = new Schema({}, {strict: false});

const ring_spares = mongoose.model("ring_spares", ring_sparesScheme);
module.exports = ring_spares;
