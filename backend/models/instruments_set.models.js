const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const instrumentSetScheme = new Schema(
  {
    id: {
      type: Number,
    },
    model: {
      type: String,
    },
    input_time: {
      type: Number,
    },
    output_time: {
      type: Number,
    },
    content: {
      non_ring_instrument: {
        BAP: {
          type: Array,
        },
        TCJ: {
          type: Array,
        },
        FOR1: {
          type: Array,
        },
        FOR2: {
          type: Array,
        },
        SPR: {
          type: Array,
        },
        RUL: {
          type: Array,
        },
        RTL1: {
          type: Array,
        },
        RTL2: {
          type: Array,
        },
      },
      ring_instrument: {
        FAA: {
          type: Array,
        },
        FAC: {
          type: Array,
        },
        FSR: {
          type: Array,
        },
        FTL: {
          type: Array,
        },
        FTB: {
          type: Array,
        },
        FTS: {
          type: Array,
        },
        HNC: {
          type: Array,
        },
        HNM: {
          type: Array,
        },
        SDM: {
          type: Array,
        },
        SDN: {
          type: Array,
        },
        SCM: {
          type: Array,
        },
      },
      printed_list: {
        type: String,
      },
      sticker_tag: {
        type: String,
      },
    },
  },
  { strict: false }
);

// const noSchema = new Schema({}, {strict: false});

const instrument_set = mongoose.model("instrument_sets", instrumentSetScheme);
module.exports = instrument_set;
