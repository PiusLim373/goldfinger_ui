const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const non_ring_instrumentScheme = new Schema(
  {
    name: {
      type: String,
    },
    input_time: {
      type: Number,
    },
    output_time: {
      type: Number,
    },
    inspection_result: {
      type: Boolean,
    },
  },
  { strict: false }
);

// const noSchema = new Schema({}, {strict: false});

const non_ring_instrument = mongoose.model(
  "non_ring_instrument",
  non_ring_instrumentScheme
);
module.exports = non_ring_instrument;
