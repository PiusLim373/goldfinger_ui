const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const non_ring_sparesScheme = new Schema(
  {
    name: {
      type: String,
    },
    time_last_refill: {
      type: Number,
    },
    remaining: {
      type: Number,
    },
    capacity: {
      type: Number,
    },
    location: {
      type: String,
    },
    codename: {
      type: String,
    },
  },
  { strict: false }
);

// const noSchema = new Schema({}, {strict: false});

const non_ring_spares = mongoose.model(
  "non_ring_spares",
  non_ring_sparesScheme
);
module.exports = non_ring_spares;
