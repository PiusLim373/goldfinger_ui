const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const consumablesScheme = new Schema(
  {
    name: {
      type: String,
    },
    time_last_refill: {
      type: Number,
    },
    rate_of_consumption: {
      type: Number,
    },
    remaining: {
      type: Number,
    },
    location: {
      type: String,
    },
  },
  { strict: false }
);

// const noSchema = new Schema({}, {strict: false});

const consumables = mongoose.model("consumables", consumablesScheme);
module.exports = consumables;
